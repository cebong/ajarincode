<?php 
	class Scanner{
		public $file_list = array();
		public $stack = [];
		public $dirfile = '';
		public $status = [];

		public function scan_directory($dirpath = null){
			if($dirpath == null) return false;
			$this->file_list = array();
			$this->stack[] = $dirpath;

			while($this->stack){
				$base_dir = array_pop($this->stack);

				if($dh = opendir($base_dir)){
					while(($file = readdir($dh)) !== false){
						if($file !== '.' AND $file !== '..'){
							$current_file = $base_dir.'\\'.$file;

							if(is_file($current_file)){
								continue;
							}
							elseif(is_dir($current_file)){
								$this->stack[] = $current_file;
								$this->file_list[] = $base_dir.'\\'.$file;
								$this->status[] = $this->copy_file($base_dir.'\\'.$file);
							}
						}
					}
				}
			}
			//return $this->file_list;
			return $this->status;
		}

		private function copy_file($current = null){
			if($current == null) return false;
			$this->dirfile = dirname(__FILE__).'\\assets\\';

			return copy($this->dirfile.'\\index.html',$current.'\\index.html');
		}
	}

	$scan = new Scanner();
	$bd = dirname(__FILE__).'\\assets';
	$directory = $scan->scan_directory($bd);

	print_r($directory);
?>