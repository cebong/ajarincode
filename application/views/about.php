<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_2.png);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>About Us</h1>
							<h2>AjarinCode adalah sebuah web yang dibuat untuk membantu seseorang dalam memahami dan mempelajari seputar bahasa pemrograman web(HTML, CSS, Javascript, PHP) dan DBMS(MySQL). Dalam pembelajarannya sendiri AjarinCode akan memandu secata step by step sehingga bagi seorang pemula tidak perlu khawatir jika ingin belajar mengenai bahasa pemrograman ini</a></h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-blog">
		<div class="container">
			<div class="row">
				<h1 class="text-center">TEAM</h1>
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>assets/home/images/team/reza.jpg" alt=""></a>
						<div class="blog-text">
							<h3>Reza Akbar Hidayat</h3>
							<span class="posted_on">Founder &amp; Web Developer</span>
							<p>
								<a href="https://www.facebook.com/profile.php?id=100009285696783" target="_blank"><i class="icon-facebook"></i></a>
								<a href="https://twitter.com/reza_ak23" target="_blank"><i class="icon-twitter"></i></a>
								<a href="https://www.instagram.com/reza_ak23/" target="_blank"><i class="icon-instagram"></i></a>
								<a href="https://www.linkedin.com/in/reza-hidayat-b01501106/" target="_blank"><i class="icon-linkedin"></i></a>
							</p>
						</div> 
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>assets/home/images/team/dayu.jpg" alt=""></a>
						<div class="blog-text">
							<h3>Ida Ayu Putu Sribawa</h3>
							<span class="posted_on">Web Developer</span>
							<p>
								<a href="https://www.facebook.com/idaayuputu.sribawa" target="_blank"><i class="icon-facebook"></i></a>
							</p>
						</div> 
					</div>
				</div>
				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>assets/home/images/team/nanang.jpg" alt=""></a>
						<div class="blog-text">
							<h3><a href="">Nanang Adistia</a></h3>
							<span class="posted_on">Web Designer</span>
							<p>
								<a href="https://www.facebook.com/nanang.adistia" target="_blank"><i class="icon-facebook"></i></a>
								<a href="https://www.instagram.com/nanang_adistia/" target="_blank"><i class="icon-instagram"></i></a>
							</p>
						</div> 
					</div>
				</div>

				<div class="col-lg-4 col-md-4">
					<div class="fh5co-blog animate-box">
						<a href="#"><img class="img-responsive" src="<?php echo base_url() ?>assets/home/images/team/yokego.jpg" alt=""></a>
						<div class="blog-text">
							<h3><a href="">I Wayan Yokego</a></h3>
							<span class="posted_on">Wev Designer</span>
							<p>
								<a href="https://www.facebook.com/wayanyokego" target="_blank"><i class="icon-facebook"></i></a>
								<a href="https://twitter.com/Yan_Kego" target="_blank"><i class="icon-twitter"></i></a>
								<a href="https://www.instagram.com/yan.kego/" target="_blank"><i class="icon-instagram"></i></a>
							</p>
						</div> 
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-started" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_3.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Lets Get Started</h2>
					<p>Ayo Mulai jadi Developer dari sekarang</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center">
					<p><a href="<?php echo site_url('login') ?>" class="btn btn-default btn-lg">Start Course</a></p>
				</div>
			</div>
		</div>
	</div>