<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_2.png);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<h1>Harga yang Kami Tawarkan</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-pricing">
		<div class="container">
			<div class="row">
				<div class="pricing">
					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Fundamental</h2>
							<div class="price"><sup class="currency">Rp</sup>0</div>
							<ul class="classes">
								<li><i class="icon-check2"></i>&nbsp;Akses Materi Fundamental</li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Standart</h2>
							<div class="price"><sup class="currency">Rp</sup>100K</div>
							<ul class="classes">
								<li><i class="icon-check2"></i>&nbsp; Akses Materi Fundamental &amp; Standart</li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Medium</h2>
							<div class="price"><sup class="currency">Rp</sup>200K</div>
							<ul class="classes">
								<li><i class="icon-check2"></i>&nbsp;Akses Materi Fundamental, Standart &amp; Medium</li>
								<li><i class="icon-check2"></i>&nbsp;Akses Source Code</li>
							</ul>
						</div>
					</div>

					<div class="col-md-3 animate-box">
						<div class="price-box">
							<h2 class="pricing-plan">Advanced</h2>
							<div class="price"><sup class="currency">Rp</sup>350K</div>
							<ul class="classes">
								<li><i class="icon-check2"></i>&nbsp;Free Hosting</li>
								<li><i class="icon-check2"></i>&nbsp;Akses Source Code</li>
								<li><i class="icon-check2"></i>&nbsp;Akses Semua Materi</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div id="fh5co-started" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_3.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Lets Get Started</h2>
					<p>Ayo Mulai jadi Developer dari sekarang</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center">
					<p><a href="<?php echo site_url('login') ?>" class="btn btn-default btn-lg">Start Course</a></p>
				</div>
			</div>
		</div>
	</div>