<header id="fh5co-header" class="fh5co-cover fh5co-cover-sm" role="banner" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_2.png);" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="display-t">
                        <div class="display-tc animate-box" data-animate-effect="fadeIn">
                            <h1>Contact Us</h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </header>

    <div id="fh5co-contact">
        <div class="container">
            <div class="row">
                <?php if($this->session->userdata('status')=="Berhasil"){ ?>
                    <p class="text-center" style="color: #08F94C"><?php echo $this->session->flashdata('message') ?></p>
                <?php } ?>
                <div class="col-md-5 col-md-push-1 animate-box">
                    
                    <div class="fh5co-contact-info">
                        <h3>Contact Information</h3>
                        <ul>
                            <li class="phone">&nbsp;+6285792071316</li>
                            <li class="email"><a href="mailto:support@ajarincode.com">support@ajarincode.com</a></li>
                            <li class="url"><a href="https://ajarincode.com">www.ajarincode.com</a></li>
                        </ul>
                    </div>

                </div>
                <div class="col-md-6 animate-box">
                    <h3>Get In Touch</h3>
                    <form action="<?php echo site_url('home/kirim_email') ?>" method="post">
                        <div class="row form-group">
                            <div class="col-md-12">
                                <!-- <label for="fname">First Name</label> -->
                                <input type="text" id="fname" name="nama" class="form-control" placeholder="Fullname">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <!-- <label for="email">Email</label> -->
                                <input type="email" id="email" name="email" class="form-control" placeholder="Your email address">
                            </div>
                        </div>

                        <div class="row form-group">
                            <div class="col-md-12">
                                <!-- <label for="message">Message</label> -->
                                <textarea name="message" id="message" name="Message" cols="30" rows="10" class="form-control" placeholder="Say something about us"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Send Message" class="btn btn-primary">
                        </div>

                    </form>     
                </div>
            </div>
            
        </div>
    </div>
   
    <div id="fh5co-started" style="background-image:url(<?php echo base_url() ?>assets/home/images/background_3.jpg);">
        <div class="overlay"></div>
        <div class="container">
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
                    <h2>Lets Get Started</h2>
                    <p>Ayo Mulai jadi Developer dari sekarang</p>
                </div>
            </div>
            <div class="row animate-box">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <p><a href="<?php echo site_url('login') ?>" class="btn btn-default btn-lg">Start Course</a></p>
                </div>
            </div>
        </div>
    </div>