<!DOCTYPE html>
<html lang="en">
<head>
	<title>AjarinCode</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->	
	<link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/icon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/css/util.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/maintenance/css/main.css">
<!--===============================================================================================-->
</head>
<body>
	
	
	<div class="bg-g1 size1 flex-w flex-col-c-sb p-l-15 p-r-15 p-b-30">
		<div class="flex-w flex-c cd100 wsize1 bor1">
			<div class="flex-col-c-m size4 bg0 bor2">
				<span class="l1-txt3">Maintenance</span>
			</div>
		</div>


		<div class="flex-col-c w-full p-t-50 p-b-80">
			<center>
				<img src="<?php echo base_url('assets/images/logo.png') ?>" width="100px" height="100px">
			</center>
			<h3 class="l1-txt1 txt-center p-b-10">
				Working !!!
			</h3>

			<p class="txt-center l1-txt2 p-b-43 wsize2">
				Kami sedang melakukan pembaruan dan perbaikan sistem, mohon maaf atas ketidaknyamanannya
			</p>			
		</div>

		<span class="s1-txt2 txt-center">
			@ 2018 AjarinCode
		</span>

	</div>



	

<!--===============================================================================================-->	
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/jquery/jquery-3.2.1.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/bootstrap/js/popper.js"></script>
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/countdowntime/moment.min.js"></script>
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/countdowntime/moment-timezone.min.js"></script>
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/countdowntime/moment-timezone-with-data.min.js"></script>
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/countdowntime/countdowntime.js"></script>
	<script>
		$('.cd100').countdown100({
			// Set Endtime here
			// Endtime must be > current time
			endtimeYear: 0,
			endtimeMonth: 0,
			endtimeDate: 35,
			endtimeHours: 18,
			endtimeMinutes: 0,
			endtimeSeconds: 0,
			timeZone: "" 
			// ex:  timeZone: "America/New_York", can be empty
			// go to " http://momentjs.com/timezone/ " to get timezone
		});
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>assets/maintenance/vendor/tilt/tilt.jquery.min.js"></script>
	<script >
		$('.js-tilt').tilt({
			scale: 1.1
		})
	</script>
<!--===============================================================================================-->
	<script src="<?php echo base_url() ?>assets/maintenance/js/main.js"></script>

</body>
</html>