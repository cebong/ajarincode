<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Menu Tes
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li class="active"><a href="<?php echo site_url('dashboard/tes') ?>"></a>Tes</li>
      </ol>
	</section>
	<section class="content">
		<div class="row">
			<?php if($this->session->flashdata('status') == "gagal"){ ?>
				<div class="col-md-2"></div>
				<div class="col-md-8">
					<div class="callout callout-danger">
						<p align="center"><?php echo $this->session->userdata('message') ?></p>
					</div>
				</div>
				<div class="col-md-2"></div>
			<?php } ?>
		</div>
		<div class="row">
			<div class="col-md-2">
				<div class="box">
					<div class="box-body bg-orange">
						<a href="<?php echo site_url('soal/tes/'.$id=1) ?>" class="btn">
							<img src="<?php echo base_url('assets/images/kategori/html.png') ?>" width="50px" height="50px"><br>
							<!-- <h5 style="color: #FFFFFF">Fudamental</h5> -->
						</a>
						<strong>
							<?php 
								$this->db->select('nilai');
								// $this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('hasil_tes.id_level',$this->session->userdata('id_level'));
								$this->db->where('id_kategori',1);
								$query = $this->db->get('hasil_tes')->result();

								foreach($query as $q){
									if($q->nilai < 70){
										echo "<strong style='color: #FD0505'>".$q->nilai."</strong>";
									}
									else{
										echo $q->nilai;
									}
								}
							?>/100
						</strong>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="box">
					<div class="box-body bg-aqua">
						<a href="<?php echo site_url('soal/tes/'.$id=2) ?>" class="btn">
							<img src="<?php echo base_url('assets/images/kategori/css.png') ?>" width="50px" height="50px"><br>
						</a>
						<strong>
							<?php 
								$this->db->select('nilai');
								// $this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('hasil_tes.id_level',$this->session->userdata('id_level'));
								$this->db->where('id_kategori',2);
								$query = $this->db->get('hasil_tes')->result();

								foreach($query as $q){
									if($q->nilai < 70){
										echo "<strong style='color: #FD0505'>".$q->nilai."</strong>";
									}
									else{
										echo $q->nilai;
									}
								}
							?>
							/100
						</strong>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="box">
					<div class="box-body bg-green">
						<a href="#" class="btn">
							<img src="<?php echo base_url('assets/images/kategori/js.png') ?>" width="50px" height="50px"><br>
						</a>
						<strong>
							<?php 
								$this->db->select('nilai');
								// $this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('hasil_tes.id_level',$this->session->userdata('id_level'));
								$this->db->where('id_kategori',3);
								$query = $this->db->get('hasil_tes')->result();

								foreach($query as $q){
									if($q->nilai < 70){
										echo "<strong style='color: #FD0505'>".$q->nilai."</strong>";
									}
									else{
										echo $q->nilai;
									}
								}
							?>
							/100
						</strong>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="box">
					<div class="box-body bg-yellow">
						<a href="#" class="btn">
							<img src="<?php echo base_url('assets/images/kategori/php.png') ?>" width="50px" height="50px"><br>
						</a>
						<strong>
							<?php 
								$this->db->select('nilai');
								// $this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('hasil_tes.id_level',$this->session->userdata('id_level'));
								$this->db->where('id_kategori',4);
								$query = $this->db->get('hasil_tes')->result();

								foreach($query as $q){
									if($q->nilai < 70){
										echo "<strong style='color: #FD0505'>".$q->nilai."</strong>";
									}
									else{
										echo $q->nilai;
									}
								}
							?>
							/100
						</strong>
					</div>
				</div>
			</div>
			<div class="col-md-2">
				<div class="box">
					<div class="box-body bg-purple">
						<a href="#" class="btn">
							<img src="<?php echo base_url('assets/images/kategori/sql.png') ?>" width="50px" height="50px"><br>
						</a>
						<strong>
							<?php 
								$this->db->select('nilai');
								// $this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('hasil_tes.id_level',$this->session->userdata('id_level'));
								$this->db->where('id_kategori',5);
								$query = $this->db->get('hasil_tes')->result();

								foreach($query as $q){
									if($q->nilai < 70){
										echo "<strong style='color: #FD0505'>".$q->nilai."</strong>";
									}
									else{
										echo $q->nilai;
									}
								}
							?>
							/100
						</strong>
					</div>
				</div>
			</div>
			<!-- <div class="col-md-2">
				<div class="box">
					<div class="box-body bg-black">
						<a href="#" class="btn">
							<img src="<?php echo base_url('assets/images/nilai.png') ?>" width="50px" height="50px"><br>
						</a>
						<strong>
							<?php 
								$this->db->select_sum('nilai');
								$this->db->join('level','level.id_level=hasil_tes.id_level');
								$this->db->where('id_user',$this->session->userdata('id'));
								$this->db->where('nama_level',$this->session->userdata('level'));
								$query = $this->db->get('hasil_tes')->result();

								// $nilai_akhir = $query->nilai / 5;
								
								foreach($query as $q){
									$nilai_akhir = $q->nilai / 5;
									echo $nilai_akhir;
								}
							?>
						</strong>
					</div>
				</div>
			</div> -->
		</div>
		<div class="row">
			<div class="col-md-2"></div>
			<div class="col-md-2"></div>

			<div class="col-md-2">
				<a href="<?php echo site_url('hasil/upgrade_level/'.$this->session->userdata('id')) ?>" class="btn btn-primary" style="width: 100%"><i class="fa fa-level-up"></i>&nbsp;Upgrade Level</a>
			</div>
			
		</div>
	</section>
</div>