<div class="content-wrapper">
	<section class="content-header">
		<h1>
			List Materi
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboarsx"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('dashboard/materi') ?>">Materi</a></li>
			<li class="active"><a href="<?php echo site_url('materi/tampil_materi') ?>">List Materi</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box box-solid">
					<div class="box-header with-border">
						<h3 class="box-title">Daftar Materi</h3>
					</div>
					<div class="box-body">
						<div class="box-group" id="accordian">
							<?php foreach($kategori as $k){ ?>
								<div class="panel box box-primary">
									<div class="box-header with-border">
										<h4 class="box-title">
											<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $k->id_kategori ?>">
												<?php echo $k->nama_kategori ?>
											</a>
										</h4>
									</div>
									<div id="<?php echo $k->id_kategori ?>" class="panel-collapse collapse">
					                    <div class="box-body">
					                    	<?php foreach($materi as $m){ ?>
					                    		<?php if($m->id_kategori == $k->id_kategori){ ?>
					                    			<li><a href="<?php echo site_url('materi/baca_materi/'.$m->id_materi.'/'.$m->slug_materi) ?>"><?php echo $m->judul_materi ?></a></li>
					                    		<?php } ?>
					                    	<?php } ?>
					                    </div>
					                </div>
								</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>