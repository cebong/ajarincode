<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Form Edit Data Level
    </h1>
    <ol class="breadcrumb">
      <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
      <li><a href="#">Materi</a></li>
      <li><a href="#">Edit</a></li>
    </ol>
  </section>

  <section class="content">
    <div class="row">
      <div class="col-xs-12">
        <div class="box">
          <div class="box-header"></div>
            <div class="box-body">
              <div class="callout callout-info">
                <h4><strong>Perhatian !</strong></h4>
                <p>
                  Sebelum membuat materi perhatikan beberapa hal berikut :
                  <li>Pilih Level dan Kategori sesuai dengan bobot materi yang diberikan</li>
                  <li>Deskripsi berisikan penjelasan secara singkat mengenai materi yang diberikan</li>
                  <li>Untuk link video bersifat opsional yang artinya jika dalam materi tersebut ingin diberikan cuplikan video silahkan cantumkan link video jika tida bisa dikosongkan</li>
                </p>
              </div>
              <?php foreach($materi as $m){ ?>
              <form action="<?php echo site_url('materi/update') ?>" method="post">
                <div class="form-group">
                  <label>Judul :</label>
                  <input type="hidden" name="id" class="form-control" value="<?php echo $m->id_materi ?>">
                  <input type="text" name="judul" class="form-control" value="<?php echo $m->judul_materi ?>" required>
                </div>
                <div class="form-group">
                  <label>Level :</label>
                  <select class="form-control select2" name="level" required>
                    <option>-- Pilih Level --</option>
                    <?php foreach($level as $l){ ?>
                    <option value="<?php echo $l->id_level ?>" <?php if($l->id_level==$m->id_level){echo "selected";} ?>><?php echo $l->nama_level ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Kategori :</label>
                  <select class="form-control select2" name="kategori" required>
                    <option>-- Pilih Kategori --</option>
                    <?php foreach($kategori as $k){ ?>
                    <option value="<?php echo $k->id_kategori ?>" <?php if($k->id_kategori==$m->id_kategori){echo "selected";} ?>><?php echo $k->nama_kategori ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Deskripsi :</label>
                  <input type="text" name="deskripsi" class="form-control" value="<?php echo $m->deskripsi_materi ?>" required>
                </div>
                <div class="form-group">
                  <label>URL Video (Optional)</label>
                  <input type="text" name="video" class="form-control" value="<?php echo $m->link_video ?>">
                </div>
                <div class="form-group">
                  <label>Isi Materi :</label>
                  <textarea name="isi" id="summernote"><?php echo $m->isi_materi ?></textarea>
                </div>
                <div class="form-group">
                  <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Save</button>
                </div>
              </form> 
              <?php } ?>
            </div>
          </div>
        </div>
      </div>
  </section>
</div>