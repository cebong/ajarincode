 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Materi
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/materi') ?>">Materi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  $this->db->like('id_level',1);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Materi Fundamental</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  $this->db->like('id_level',2);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Materi Standart</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  $this->db->like('id_level',3);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Materi Medium</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  $this->db->like('id_level',4);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Materi Advanced</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#html" data-toggle="tab">HTMl</a></li>
              <li><a href="#css" data-toggle="tab">CSS</a></li>
              <li><a href="#javascript" data-toggle="tab">Javascript</a></li>
              <li><a href="#php" data-toggle="tab">PHP</a></li>
              <li><a href="#sql" data-toggle="tab">SQL</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="html">
               <!-- BOX DEFAULT VERSION -->
              <div class="box-default">
            <div class="box-header">
              <a href="<?php echo site_url(('materi/create')) ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
            </div>
                  <div class="box-body">
                    <table id="html1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Level</th>
                          <th class="text-center">Judul</th>
                          <th class="text-center">Link Video</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                          <?php $no=$this->uri->segment(3)+1; foreach($materi as $m){ ?>
                        <?php if($m->nama_kategori=="HTML"){ ?>
                          <tr>
                            <td class="text-center"><?php echo $m->nama_level ?></td>
                            <td><?php echo $m->judul_materi ?></td>
                            <td class="text-center"><?php if($m->link_video==null){echo "Tidak Ada";}else{echo "Ada";} ?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('materi/edit/'.$m->id_materi) ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                              <a href="#" data-url="<?php echo site_url('materi/hapus/'.$m->id_materi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                            </td>
                          </tr>
                            <?php } ?>
                      <?php } ?>
                        </tbody>
                    </table>
                  <!-- PAGINATION DATATABLE -->  
                  <!-- <?php echo $this->pagination->create_links() ?> -->
                  </div>
              </div>
              </div>
              <div class="tab-pane" id="css">
                <!-- BOX WITH BLUE LINE -->
                <div class="box">
                  <div class="box-header">
                    <a href="<?php echo site_url(('materi/create')) ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
                    </div>
                  <div class="box-body">
                    <table id="css1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Level</th>
                          <th class="text-center">Kategori</th>
                          <th class="text-center">Judul</th>
                          <th class="text-center">Link Video</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                        <tbody>
                      <?php $no=$this->uri->segment(3)+1; foreach($materi as $m){ ?>
                        <?php if($m->nama_kategori=="CSS"){ ?>
                          <tr>
                            <td class="text-center"><?php echo $m->nama_level ?></td>
                            <td class="text-center"><?php echo $m->nama_kategori ?></td>
                            <td><?php echo $m->judul_materi ?></td>
                            <td class="text-center"><?php if($m->link_video==null){echo "Tidak Ada";}else{echo "Ada";} ?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('materi/edit/'.$m->id_materi) ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                              <a href="#" data-url="<?php echo site_url('materi/hapus/'.$m->id_materi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                            </td>
                          </tr>
                             <?php } ?>
                      <?php } ?>
                        </tbody>
                    </table>
                    <!-- MANUAL PAGINATION -->
                    <?php echo $this->pagination->create_links() ?>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="javascript">
                <div class="box">
                  <div class="box-body">
                    <div class="box-header">
                    <a href="<?php echo site_url(('materi/create')) ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
                  </div>
                    <table id="javascript1" class="table table-bordered table-striped">
                      <thead>
                        <tr> 
                          <th class="text-center">Level</th>
                          <th class="text-center">Kategori</th>
                          <th class="text-center">Judul</th>
                          <th class="text-center">Link Video</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                        <tbody>
                      <?php $no=$this->uri->segment(3)+1; foreach($materi as $m){ ?>
                        <?php if($m->nama_kategori=="Javascript"){ ?>
                          <tr>
                            <td class="text-center"><?php echo $m->nama_level ?></td>
                            <td class="text-center"><?php echo $m->nama_kategori ?></td>
                            <td><?php echo $m->judul_materi ?></td>
                            <td class="text-center"><?php if($m->link_video==null){echo "Tidak Ada";}else{echo "Ada";} ?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('materi/edit/'.$m->id_materi) ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                              <a href="#" data-url="<?php echo site_url('materi/hapus/'.$m->id_materi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                            </td>
                          </tr>
                        <?php } ?>
                      <?php } ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links() ?>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="php">
                <div class="box">
                  <div class="box-body">
                    <div class="box-header">
                    <a href="<?php echo site_url(('materi/create')) ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
                  </div>
                    <table id="php1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Level</th>
                          <th class="text-center">Kategori</th>
                          <th class="text-center">Judul</th>
                          <th class="text-center">Link Video</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                        <tbody>
                      <?php $no=$this->uri->segment(3)+1; foreach($materi as $m){ ?>
                        <?php if($m->nama_kategori=="PHP"){ ?>
                          <tr>
                            <td class="text-center"><?php echo $m->nama_level ?></td>
                            <td class="text-center"><?php echo $m->nama_kategori ?></td>
                            <td><?php echo $m->judul_materi ?></td>
                            <td class="text-center"><?php if($m->link_video==null){echo "Tidak Ada";}else{echo "Ada";} ?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('materi/edit/'.$m->id_materi) ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                              <a href="#" data-url="<?php echo site_url('materi/hapus/'.$m->id_materi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                            </td>
                          </tr>
                        <?php } ?>
                      <?php } ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links() ?>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="sql">
                <div class="box">
                  <div class="box-body">
                    <div class="box-header">
                    <a href="<?php echo site_url(('materi/create')) ?>" class="btn btn-info btn-xs"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
                  </div>
                    <table id="sql1" class="table table-bordered table-striped">
                      <thead>
                        <tr>
                          <th class="text-center">Level</th>
                          <th class="text-center">Kategori</th>
                          <th class="text-center">Judul</th>
                          <th class="text-center">Link Video</th>
                          <th class="text-center">Action</th>
                        </tr>
                      </thead>
                      <?php $no=$this->uri->segment(3)+1; foreach($materi as $m){ ?>
                        <?php if($m->nama_kategori=="SQL"){ ?>
                        <tbody>
                          <tr>
                            <td class="text-center"><?php echo $m->nama_level ?></td>
                            <td class="text-center"><?php echo $m->nama_kategori ?></td>
                            <td><?php echo $m->judul_materi ?></td>
                            <td class="text-center"><?php if($m->link_video==null){echo "Tidak Ada";}else{echo "Ada";} ?></td>
                            <td class="text-center">
                              <a href="<?php echo site_url('materi/edit/'.$m->id_materi) ?>" class="btn btn-success btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                              <a href="#" data-url="<?php echo site_url('materi/hapus/'.$m->id_materi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                            </td>
                          </tr>
                        <?php } ?>
                      <?php } ?>
                        </tbody>
                    </table>
                    <?php echo $this->pagination->create_links() ?>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    // Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Materi",
          text: "Yakin ingin menghapus materi ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FD0404",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
  </script>