<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Baca Materi
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i></a> Dashboard</li>
			<li><a href="<?php echo site_url('dashboard/materi') ?>">Materi</a></li>
			<li><a href="#">List Materi</a></li>
			<li class="active"><a href="#">Baca Materi</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<?php foreach($materi as $m){ ?>
						<?php if($m->link_video == null){ ?>
							<div class="box-body">
								<h1><?php echo $m->judul_materi ?></h1>
								<p><small><?php echo $m->deskripsi_materi ?></small></p>
								<p><?php echo $m->isi_materi ?></p>
							</div>
						<?php } ?>
						<?php if($m->link_video != null){ ?>
							<div class="box-body">
								<h1><?php echo $m->judul_materi ?></h1>
								<p><small><?php echo $m->deskripsi_materi ?></small></p>
								<p><?php echo $m->isi_materi ?></p>
								<table>
									<td><iframe width="560" height="315" src="<?php echo $m->link_video ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe></td>
								</table>
							</div>
						<?php } ?>
					<?php } ?>
				</div>
			</div>
		</div>
	</section>
</div>