<div class="content-wrapper">
    <section class="content-header">
        <h1>
            Form Edit Data User
        </h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url('admin/peserta') ?>">User</a></li>
            <li><a href="#">Edit</a></li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header"></div>
                    <div class="box-body">
                        <?php foreach($user as $u){ ?>
                        <form action="<?php echo site_url('user/update_peserta') ?>" method="post">
                            <div class="form-group">
                                <input type="hidden" name="id" class="form-control" value="<?php echo $u->id_user ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Nama Lengkap :</label>
                                <input type="text" name="nama" class="form-control" value="<?php echo $u->nama_user ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Jenis Kelamin :</label><br>
                                <input type="radio" name="jk" class="flat-red" value="L" <?php if($u->jk=="L"){echo "checked";} ?> readdonly>&nbsp;<i class="fa fa-male"></i>&nbsp;Laki-laki
                                <input type="radio" name="jk" class="flat-red" value="P" <?php if($u->jk=="P"){echo "checked";} ?>>&nbsp;<i class="fa fa-female"></i>&nbsp;Perempuan
                            </div>
                            <div class="form-group">
                                <label class="control-label">Tanggal Lahir :</label>
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                      <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" class="form-control pull-right" name="tanggal" id="datepicker" value="<?php echo $u->tgl_lahir ?>" readonly>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Username :</label>
                                <input type="text" name="username" class="form-control" value="<?php echo $u->username ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Em-Mail :</label>
                                <input type="email" name="email" class="form-control" value="<?php echo $u->email ?>" readonly>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Level :</label>
                                <select class="form-control select2" name="level">
                                    <option>-- Pilih Level --</option>
                                    <?php foreach($level as $l){ ?>
                                    <option value="<?php echo $l->id_level ?>" <?php if($l->id_level==$u->id_level){echo "selected";} ?>><?php echo $l->nama_level ?></option>
                                    <?php } ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label class="control-label">Hak Akses :</label>
                                <input type="radio" name="hak" class="flat-red" value="Peserta" <?php if($u->hak_akses=="Peserta"){echo "checked";} ?>>Peserta
                                <input type="radio" name="hak" class="flat-red" value="Mentor" <?php if($u->hak_akses=="Mentor"){echo "checked";} ?>>Mentor
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i>&nbsp;Update</button>
                                <a href="<?php echo site_url('admin/peserta') ?>" class="btn btn-danger"><i class="fa fa-times"></i>&nbsp;Batal</a>
                            </div>
                        </form>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>