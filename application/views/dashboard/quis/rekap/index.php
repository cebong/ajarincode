<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Hasil Tes
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/rekap_tes') ?>">Rekap Tes</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#html" data-toggle="tab">HTML</a></li>
						<li><a href="#css" data-toggle="tab">CSS</a></li>
						<li><a href="#javascript" data-toggle="tab">Javascript</a></li>
						<li><a href="#php" data-toggle="tab">PHP</a></li>
						<li><a href="#sql" data-toggle="tab">SQL</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="html">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#fundamental" data-toggle="tab">Fundamental</a></li>
									<li><a href="#standart" data-toggle="tab">Standart</a></li>
									<li><a href="#medium" data-toggle="tab">Medium</a></li>
									<li><a href="#advanced" data-toggle="tab">Advanced</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="fundamental">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="HTML"){ ?>
															<?php if($r->nama_level=="Fundamental"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="standart">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="HTML"){ ?>
															<?php if($r->nama_level=="Standart"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="HTML"){ ?>
															<?php if($r->nama_level=="Medium"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="advanced">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="HTML"){ ?>
															<?php if($r->nama_level=="Advanced"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="css">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#fundamental" data-toggle="tab">Fundamental</a></li>
									<li><a href="#standart" data-toggle="tab">Standart</a></li>
									<li><a href="#medium" data-toggle="tab">Medium</a></li>
									<li><a href="#advanced" data-toggle="tab">Advanced</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="fundamental">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="CSS"){ ?>
															<?php if($r->nama_level=="Fundamental"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="standart">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="CSS"){ ?>
															<?php if($r->nama_level=="Standart"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="CSS"){ ?>
															<?php if($r->nama_level=="Medium"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="advanced">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="CSS"){ ?>
															<?php if($r->nama_level=="Advanced"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="javascript">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#fundamental" data-toggle="tab">Fundamental</a></li>
									<li><a href="#standart" data-toggle="tab">Standart</a></li>
									<li><a href="#medium" data-toggle="tab">Medium</a></li>
									<li><a href="#advanced" data-toggle="tab">Advanced</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="fundamental">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="Javascript"){ ?>
															<?php if($r->nama_level=="Fundamental"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="standart">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="Javascript"){ ?>
															<?php if($r->nama_level=="Standart"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="Javascript"){ ?>
															<?php if($r->nama_level=="Medium"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="advanced">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="Javascript"){ ?>
															<?php if($r->nama_level=="Advanced"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="php">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#fundamental" data-toggle="tab">Fundamental</a></li>
									<li><a href="#standart" data-toggle="tab">Standart</a></li>
									<li><a href="#medium" data-toggle="tab">Medium</a></li>
									<li><a href="#advanced" data-toggle="tab">Advanced</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="fundamental">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="PHP"){ ?>
															<?php if($r->nama_level=="Fundamental"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="standart">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="PHP"){ ?>
															<?php if($r->nama_level=="Standart"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="PHP"){ ?>
															<?php if($r->nama_level=="Medium"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="advanced">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="PHP"){ ?>
															<?php if($r->nama_level=="Advanced"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="sql">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#fundamental" data-toggle="tab">Fundamental</a></li>
									<li><a href="#standart" data-toggle="tab">Standart</a></li>
									<li><a href="#medium" data-toggle="tab">Medium</a></li>
									<li><a href="#advanced" data-toggle="tab">Advanced</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="fundamental">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="SQL"){ ?>
															<?php if($r->nama_level=="Fundamental"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="standart">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="SQL"){ ?>
															<?php if($r->nama_level=="Standart"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="SQL"){ ?>
															<?php if($r->nama_level=="Medium"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
									<div class="tab-pane" id="advanced">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Username</th>
															<th class="text-center">Kategori</th>
															<th class="text-center">Nilai</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no=$this->uri->segment(3)+1; foreach($rekap as $r){ ?>
														<?php if($r->nama_kategori=="SQL"){ ?>
															<?php if($r->nama_level=="Advanced"){ ?>
															<tbody>
																<tr>
																	<td class="text-center"><?php echo $no++ ?></td>
																	<td><?php echo $r->username ?></td>
																	<td><?php echo $r->nama_kategori ?></td>
																	<td class="text-center"><?php echo $r->nilai ?></td>
																	<td class="text-center">
																		<a href="#" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
																		<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
																	</td>
																</tr>
															</tbody>
															<?php } ?>
														<?php } ?>
													<?php  }?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>