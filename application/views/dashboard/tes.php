<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Tes Upgrade Level
			<small>AjarinCode</small>
		</h1>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<form action="<?php echo site_url('soal/kirim_jawaban') ?>" method="post">
						<?php $no=1; foreach($soal as $s){ ?>
						<div class="box-body">
							<input type="hidden" name="kategori" class="form-control" value="<?php echo $s->id_kategori ?>">
							<input type="hidden" name="level" class="form-control" value="<?php echo $s->id_level ?>">
							<input type="hidden" name="id_soal[]" class="form-control" value="<?php echo $s->id_soal ?>">
							<?php echo $no++; ?>. <?php cetak($s->pertanyaan) ?>
							<div class="row">
								<div class="form-group">
									<div class="col-md-6">
										<input type="radio" class="flat-red" name="jawaban[<?php echo $s->id_soal ?>]" value="A">&nbsp;<?php cetak($s->opsiA) ?>
										<br>
										<input type="radio" class="flat-red" name="jawaban[<?php echo $s->id_soal ?>]" value="B">&nbsp;<?php cetak($s->opsiB) ?>
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-6">
										<input type="radio" class="flat-red" name="jawaban[<?php echo $s->id_soal ?>]" value="C">&nbsp;<?php cetak($s->opsiC) ?>
										<br>
										<input type="radio" class="flat-red" name="jawaban[<?php echo $s->id_soal ?>]" value="D">&nbsp;<?php cetak($s->opsiD) ?>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
						<div class="form-group">
							<button type="submit" class="btn btn-warning">Kirim Jawaban</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
</div>