<div class="content-wrapper">
	<section class="content-header">
		<h1>Daftar Rekening AjarinCode</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/rekening') ?>">Rekening</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-3">

			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-header">
						<a href="#" class="btn btn-info" data-toggle="modal" data-target="#tambah"><i class="fa fa-plus"></i>&nbsp;Tambah</a>
					</div>
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">No. Rek</th>
									<th class="text-center">Bank</th>
									<th class="text-center">Pemilik</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($rekening as $r){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php echo $no++ ?></td>
									<td><?php echo $r->no_rek ?></td>
									<td class="text-center"><?php echo $r->inisial_rek ?></td>
									<td><?php echo $r->pemilik_rek ?></td>
									<td class="text-center">
										<a href="#" class="btn btn-success btn-xs"><i class="fa fa-edit"></i></a>
										<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal Tambah -->
		<div class="modal fade" id="tambah">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <h4 class="modal-title">Tambah Rekening Pembayaran AjarinCode</h4>
              </div>
              <?php echo form_open_multipart('rekening/tambah') ?>
	              <div class="modal-body">
	              	<div class="form-group">
	              		<label class="control-label">No. Rekening</label>
	              		<input type="text" name="nomer" class="form-control" placeholder="Ex : 77654321">
	              	</div>
	              	<div class="form-group">
	              		<label class="control-label">Nama Bank</label>
	              		<input type="text" name="nama" class="form-control" placeholder="Ex : PT. Bank Central Asia Tbk.">
	              	</div>
	              	<div class="form-group">
	              		<label class="control-label">Inisial Bank</label>
	              		<input type="text" name="inisial" class="form-control" placeholder="Ex : BCA">
	              	</div>
	              	<div class="form-group">
	              		<label class="control-label">Nama Pemilik Rekening</label>
	              		<input type="text" name="pemilik" class="form-control" placeholder="Ex : AjarinCode">
	              	</div>
	              	<div class="form-group">
	              		<div class="row">
	              			<div class="col-md-6">
	              				<input type="file" name="path" class="form-control" accept="image/*" id="preview_gambar">
	              			</div>
	              			<div class="col-md-6">
	              				<table class="table table-bordered">
	              					<tbody>
	              						<td><img src="<?php echo base_url('assets/images/default_image.png') ?>" width="100px" heigth="50px" id="gambar_nodin"></td>
	              					</tbody>
	              				</table>
	              			</div>
	              		</div>
	              	</div>
	              </div>
	              <div class="modal-footer">
	                <button type="button" class="btn btn-default pull-left" data-dismiss="modal"><i class="fa fa-times"></i> Close</button>
	                <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
	              </div>
              </form>
            </div>
          </div>
        </div>
        <!-- Modal Tambah -->
	</section>
</div>