<div class="content-wrapper">
	<section class="content-header">
		<h1>
			View Detail Pembayaran
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashbaord') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/rekap_event') ?>">Pembayaran Event</a></li>
			<li class="active"><a href="#">View Pembayaran</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php foreach($pembayaran as $p){ ?>
						<div class="col-md-9">
							<div class="form-group">
								<label class="control-label">Order ID :</label>
								<p><?php echo $p->id_bayar_event ?></p>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Event yang diikuti :</label>
										<p><?php echo $p->nama_event ?></p>
									</div>
									<div class="form-group">
										<label class="control-label">Deskripsi Event :</label>
										<p><?php echo $p->deskripsi_event ?></p>
									</div>	
									<div class="form-group">
										<label class="control-label">Level Partisipasi :</label>
										<p><?php echo $p->id_level ?></p>
									</div>
									<div class="form-group">
										<label class="control-label">Harga Event :</label>
										<p>Rp <?php echo number_format($p->harga_event,2,',','.') ?></p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">Username :</label>
										<p><?php echo $p->username ?></p>
									</div>
									<div class="form-group">
										<label class="control-label">Total Bayar :</label>
										<p>Rp <?php echo number_format($p->total_bayar_event,2,',','.') ?></p>
									</div>
									<!-- <div class="form-group">
										<a href="<?php echo site_url('partisipasi/update_status/'.$p->id_participant) ?>" class="btn btn-success btn-xs" onclick="self.close()">Konfirmasi Pembayaran</a>
									</div> -->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Bukti Bayar :</label><br>
								<p class="easyzoom easyzoom--overlay">
									<?php if($p->bukti_bayar_event==null){echo "Belum melakukan pembayaran";}else{ ?>
									<a href="<?php echo base_url('assets/event/bukti_bayar/'.$p->bukti_bayar_event) ?>">
										<img src="<?php echo base_url('assets/event/bukti_bayar/'.$p->bukti_bayar_event) ?>" width="250px" height="300px">
									</a>
									<?php } ?>
								</p>
							</div>	
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>