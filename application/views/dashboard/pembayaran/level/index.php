<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Daftar Pembayaran Event
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><i class="fa fa-dashboard"></i><a href="<?php echo site_url('dashboard') ?>">&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/rekap_tes') ?>"></a>Pembayaran Event</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#belum_lunas" data-toggle="tab">Belum Lunas</a></li>
						<li><a href="#lunas" data-toggle="tab">Lunas</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="belum_lunas">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Order ID</th>
												<th class="text-center">Level</th>
												<th class="text-center">Username</th>
												<th class="text-center">Bayar</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($pembayaran_level as $pe){ ?>
											<?php if($pe->status_pembayaran=="Belum Lunas"){ ?>
												<tbody>
													<td class="text-center"><?php cetak($no++) ?></td>
													<td class="text-center"><?php cetak($pe->id_bayar_level) ?></td>
													<td><?php cetak($pe->nama_level) ?></td>
													<td><?php cetak($pe->username) ?></td>
													<td class="text-center">Rp <?php cetak(number_format($pe->total_bayar_level,2,',','.')) ?></td>
													<td><?php cetak($pe->status_pembayaran) ?></td>
													<td>
														<a href="<?php echo site_url('level/view_pembayaran/'.$pe->id_bayar_level) ?>" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('level/update_status/'.$pe->id_bayar_level) ?>" class="btn btn-success btn-xs <?php if($pe->status_pembayaran=="Lunas"){echo "disabled";} ?>"><i class="fa fa-check"></i></a>
														<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
													</td>
												</tbody>
											<?php } ?>
										<?php } ?>
									</table>
									<?php echo $this->pagination->create_links(); ?>
								</div>
							</div>		
						</div>
						<div class="tab-pane" id="lunas">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Order ID</th>
												<th class="text-center">Level</th>
												<th class="text-center">Username</th>
												<th class="text-center">Bayar</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($pembayaran_level as $pe){ ?>
											<?php if($pe->status_pembayaran=="Lunas"){ ?>
												<tbody>
													<td class="text-center"><?php cetak($no++) ?></td>
													<td class="text-center"><?php cetak($pe->id_bayar_level) ?></td>
													<td><?php cetak($pe->nama_level) ?></td>
													<td><?php cetak($pe->username) ?></td>
													<td class="text-center">Rp <?php cetak(number_format($pe->total_bayar_level,2,',','.')) ?></td>
													<td><?php cetak($pe->status_pembayaran) ?></td>
													<td>
														<a href="<?php echo site_url('level/view_pembayaran/'.$pe->id_bayar_level) ?>" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('level/update_status/'.$pe->id_bayar_level) ?>" class="btn btn-success btn-xs <?php if($pe->status_pembayaran=="Lunas"){echo "disabled";} ?>"><i class="fa fa-check"></i></a>
														<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
													</td>
												</tbody>
											<?php } ?>
										<?php } ?>
									</table>
									<?php echo $this->pagination->create_links(); ?>
								</div>
							</div>		
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>