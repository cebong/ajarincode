<div class="content-wrapper">
	<section class="content-header">
		<h1>
			View Detail Pembayaran
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashbaord') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/rekap_level') ?>">Pembayaran Level</a></li>
			<li class="active"><a href="#">View Pembayaran</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php foreach($pembayaran as $p){ ?>
						<div class="col-md-9">
							<div class="form-group">
								<label class="control-label">Order ID :</label>
								<p><?php echo $p->id_bayar_level ?></p>
							</div>
							<div class="row">
								<div class="col-md-6">
									<div class="form-group">
										<label class="control-label">Level :</label>
										<p><?php echo $p->nama_level ?></p>
									</div>
									
									<div class="form-group">
										<label class="control-label">Harga Upgrade Level :</label>
										<p>Rp <?php echo number_format($p->harga_level,2,',','.') ?></p>
									</div>
								</div>
								<div class="col-md-3">
									<div class="form-group">
										<label class="control-label">Username :</label>
										<p><?php echo $p->username ?></p>
									</div>
									<div class="form-group">
										<label class="control-label">Total Bayar :</label>
										<p>Rp <?php echo number_format($p->total_bayar_level,2,',','.') ?></p>
									</div>
									<!-- <div class="form-group">
										<a href="<?php echo site_url('partisipasi/update_status/'.$p->id_participant) ?>" class="btn btn-success btn-xs" onclick="self.close()">Konfirmasi Pembayaran</a>
									</div> -->
								</div>
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								<label class="control-label">Bukti Bayar :</label><br>
								<p class="easyzoom easyzoom--overlay">
									<?php if($p->bukti_bayar_level==null){echo "Belum melakukan pembayaran";}else{ ?>
									<a href="<?php echo base_url('assets/bukti_level/'.$p->bukti_bayar_level) ?>">
										<img src="<?php echo base_url('assets/bukti_level/'.$p->bukti_bayar_level) ?>" width="250px" height="300px">
									</a>
									<?php } ?>
								</p>
							</div>	
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>