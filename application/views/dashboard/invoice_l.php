<!-- Main content -->
    <section class="invoice" id="konten">
      <!-- title row -->
      <div class="row">
        <div class="col-xs-12">
          <h2 class="page-header">
            <img src="<?php echo base_url('assets/images/logo.png') ?>" width="50px" height="50px"> AjarinCode
          <?php foreach( $invoice as $i){ ?>
            <small class="pull-right">Date: <?php echo date("d/F/Y", strtotime($i['tgl_bayar_level'])) ?></small>
          </h2>
        </div>
        <!-- /.col -->
      </div>
      <!-- info row -->
      <div class="row invoice-info">
        <div class="col-sm-4 invoice-col">
          From
          <address>
            <strong>AjarinCode</strong><br>
            Jalan Pakusari No. 10A<br>
            Sesetan, Denpasar 80223<br>
            Phone: +6285792071316<br>
            Email: support@ajarincode.com
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          To
          <address>
            <strong><?php echo $this->session->userdata('username') ?></strong><br>
            Phone: <?php echo $this->session->userdata('no_user') ?><br>
            Email: <?php echo $this->session->userdata('email') ?>
          </address>
        </div>
        <!-- /.col -->
        <div class="col-sm-4 invoice-col">
          <br>
          <b>Order ID:</b> <?php echo $i['id_bayar_level'] ?><br>
          <b>Account:</b> <?php echo $this->session->userdata('id') ?>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- Table row -->
      <div class="row">
        <div class="col-xs-12 table-responsive">
          <table class="table table-striped">
            <thead>
            <tr>
              <th>No</th>
              <th>Jenis Pembayaran</th>
              <th>Harga</th>
              <th>Subtotal</th>
            </tr>
            </thead>
            <tbody>
            <tr>
              <td><?php $no=1; echo $no++ ?></td>
              <td><?php echo $i['nama_level'] ?></td>
              <td><?php echo $i['harga_level'] ?></td>
              <td><?php echo $i['harga_level'] ?></td>
            </tr>
            </tbody>
          </table>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <div class="row">
        <!-- accepted payments column -->
        <div class="col-xs-6">
          <p class="lead">Metode Pembayaran:</p>
          <img src="<?php echo base_url('assets/images/pembayaran/BCA.png') ?>" alt="BCA">

          <div class="alert alert-info" style="margin-top: 10px;">
           <b>Note! :</b>
           Harap melakukan pembayaran tepat sampai 3 digit terakhir untuk mempercepat proses transaksi<br>
           Silahkan melakukan transaksi ke BCA : <?php echo "7725297106 A/N Reza Akbar Hidayat" ?>
          </div>
        </div>
        <!-- /.col -->
        <div class="col-xs-6">

          <div class="table-responsive">
            <table class="table">
              <tr>
                <th style="width:50%">Subtotal:</th>
                <td><?php echo $i['harga_level'] ?></td>
              </tr>
              <tr>
                <th>Tax</th>
                <td><?php echo $i['kode_unik_level'] ?></td>
              </tr>
              <tr>
                <th>Total:</th>
                <td><?php echo $i['total_bayar_level'] ?></td>
              </tr>
            </table>
          </div>
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->

      <!-- this row will not appear when printing -->
      <div class="row no-print">
        <div class="col-xs-12">
          <a href="#" onclick="window.print()" target="_blank" class="btn btn-default"><i class="fa fa-print"></i> Print</a>
          <button type="button" class="btn btn-success pull-right" data-toggle="modal" data-target="#bayar" onclick="edit(<?php echo $i['id_bayar_level'] ?>)"><i class="fa fa-credit-card"></i> Submit Payment
          </button>
          <button type="button" class="btn btn-primary pull-right" style="margin-right: 5px;" id="konvert">
            <i class="fa fa-download"></i> Generate PDF
          </button>
        </div>
      </div>
      <?php } ?>
      <!-- Modal -->
          <div class="modal fade" id="bayar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Upload Bukti Bayar</h4>
                </div>
                <div class="modal-body">
                  <?php echo form_open_multipart('tagihan/update_tagihan_level') ?>
                    <div class="row">
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Order ID</label>
                          <input type="text" name="id" class="form-control" id="id_bayar_level" readonly>
                        </div>
                        <div class="form-group">
                          <label class="control-laebl">Nama Level</label>
                          <input type="text" name="event" class="form-control" id="nama_level" disabled>
                        </div>
                      </div>
                      <div class="col-md-6">
                        <div class="form-group">
                          <label class="control-label">Total Bayar (Rp)</label>
                          <input type="text" name="harga" class="form-control" id="total_bayar_level" disabled>
                        </div>
                        <div class="form-group">
                          <label class="control-label">Upload Bukti Pembayaran</label>
                          <input type="file" name="gambar" class="form-control" accept="image/*">
                        </div>
                      </div>
                    </div>
                     <div class="row">
                      <div class="col-md-6">
                        <button class="btn btn-primary" type="submit"><i class="fa fa-upload"></i>&nbsp;Bayar</button>
                      </div>
                    </div>
                  </form>
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div>
          <!-- /.modal -->
    </section>
    <!-- /.content -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/0.9.0rc1/jspdf.min.js"></script>
    <script type="text/javascript">
        var doc = new jsPDF();
            var specialElementHandlers = {
                '#editor': function (element, renderer) {
                return true;
            }
        };

        $('#konvert').click(function () {   
            doc.fromHTML($('#konten').html(), 15, 15, {
                'width': 170,
                    'elementHandlers': specialElementHandlers
            });
            doc.save('Tagihan.pdf');
        });

        function edit(idbayartagihan){
        $.ajax({
            url:"<?php echo site_url('tagihan/upload_bukti_level');?>",
            type:"post",
            dataType: 'json',
            data:{id:idbayartagihan},
            cache:false,
            success:function(result){
              $('#id_bayar_level').val(result['id_bayar_level']);
              $('#nama_level').val(result['nama_level']);
              $('#total_bayar_level').val(result['total_bayar_level']);
            }
        });
      }
    </script>