<div class="content-wrapper">
  <section class="content-header">
    <h1>
      Data Participant Event
      <small>AjarinCode</small>
    </h1>
    <ol class="breadcrumb">
      <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
      <li class="active"><a href="<?php echo site_url('admin/partisipasi') ?>">Participant</a></li>
    </ol>
  </section>
  <section class="content">
    <div class="row">
      <div class="col-md-12">
        <div class="nav-tabs-custom">
          <ul class="nav nav-tabs">
            <li class="active"><a href="#belum_lunas" data-toggle="tab">Belum Lunas</a></li>
            <li><a href="#sudah_lunas" data-toggle="tab">Sudah Lunas</a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="belum_lunas">
              <div class="box">
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Username</th>
                        <th class="text-center">Event</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <?php $no=$this->uri->segment(3)+1; foreach($partisipasi as $p){ ?>
                      <?php if($p->status_participant=="Belum Lunas"){ ?>
                        <tbody>
                          <tr>
                            <td class="text-center"><?php echo $no++ ?></td>
                            <td><?php echo $p->username ?></td>
                            <td><?php echo $p->nama_event ?></td>
                            <td><?php echo $p->status_participant ?></td>
                          </tr>
                        </tbody>
                      <?php } ?>
                    <?php } ?>
                  </table>
                  <?php echo $this->pagination->create_links(); ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="sudah_lunas">
              <div class="box">
                <div class="box-body">
                  <table class="table table-bordered">
                    <thead>
                      <tr>
                        <th class="text-center">#</th>
                        <th class="text-center">Username</th>
                        <th class="text-center">Event</th>
                        <th class="text-center">Status</th>
                      </tr>
                    </thead>
                    <?php $no=$this->uri->segment(3)+1; foreach($partisipasi as $p){ ?>
                      <?php if($p->status_participant=="Lunas"){ ?>
                        <tbody>
                          <tr>
                            <td class="text-center"><?php echo $no++ ?></td>
                            <td><?php echo $p->username ?></td>
                            <td><?php echo $p->nama_event ?></td>
                            <td><?php echo $p->status_participant ?></td>
                          </tr>
                        </tbody>
                      <?php } ?>
                    <?php } ?>
                  </table>
                  <?php echo $this->pagination->create_links(); ?>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<script type="text/javascript">
  // Hapus
  $(document).ready(function(){
    $('.confirm_delete').on('click', function(){
      
      var delete_url = $(this).attr('data-url');

      swal({
        title: "Hapus Partisipan",
        text: "Yakin ingin menghapus partisipan ini ?",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#FD0404",
        confirmButtonText: "Hapus !",
        cancelButtonText: "Batalkan",
        closeOnConfirm: false     
      }, function(){
      window.location.href = delete_url;
      });

    return false;
    });
  });
</script>