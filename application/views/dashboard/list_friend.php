<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Mulai hubungkan bersama
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('dashboard/friend') ?>">List Friend</a></li>
		</ol>
	</section>
	<section class="content">
		<?php foreach($friend as $f){ ?>
		 <div class="col-md-4">
          <div class="box box-widget widget-user">
            <div class="widget-user-header <?php if($f->nama_level=="Fundamental"){echo "bg-aqua";}elseif($f->nama_level=="Standart"){echo "bg-green";}elseif($f->nama_level=="Medium"){echo "bg-yellow";}elseif($f->nama_level=="Advanced"){echo "bg-red";} ?>">
              <h3 class="widget-user-username"><?php echo $f->nama_user ?></h3>
              <h5 class="widget-user-desc"><?php echo $f->username ?></h5>
            </div>
            <div class="widget-user-image">
              <img class="img-circle" src="<?php echo base_url('assets/images/dp/'.$f->path) ?>" alt="User Avatar">
            </div>
            <div class="box-footer">
              <div class="row">
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <h5 class="description-header">Asal</h5>
                    <span class="description-text"><?php echo $f->nama_kabupaten ?></span>
                  </div>
                </div> 
                <div class="col-sm-4 border-right">
                  <div class="description-block">
                    <button type="button" class="btn btn-primary"><i class="fa fa-user-plus"></i>&nbsp;Follow</button>
                  </div>
                </div>
                <div class="col-sm-4">
                  <div class="description-block">
                    <h5 class="description-header">Level</h5>
                    <span class="description-text"><?php echo $f->nama_level ?></span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
    	<?php } ?>
	</section>
</div>