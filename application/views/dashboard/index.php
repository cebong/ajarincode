<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
  <?php if($this->session->userdata('hak_akses') == "Admin"){ ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Dashboard
        <small>Control panel</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->not_like('hak_akses','Admin');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Total User</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('event');
                  $this->db->like('status_event','Open Participant');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Active Event</p>
            </div>
            <div class="icon">
              <i class="fa fa-500px"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->like('hak_akses','Mentor');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Total Mentor</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-secret"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-3 col-xs-6">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Total Materi</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <!-- ./col -->
      </div>
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-white">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->not_like('hak_akses','Admin');
                  $this->db->like('confirm',1);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>User Verify</p>
            </div>
            <div class="icon">
              <i class="fa fa-user"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-navy">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->not_like('hak_akses','Admin');
                  $this->db->like('confirm',0);
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>User Not Verify</p>
            </div>
            <div class="icon">
              <i class="fa fa-user-times"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-olive">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('ask_code');
                  $this->db->like('status_ask','Terjawab');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Pertanyaan Terjawab</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-maroon">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('ask_code');
                  $this->db->like('status_ask','Pending');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Pertanyaan Belum Direspon</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"><small><i class="fa fa-times"></i></small></i>
            </div>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row">
        <!-- Left col -->
        <section class="col-lg-4 connectedSortable">
          <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Pengguna AjarinCode</h3>
            </div>
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url('assets/images/image.png') ?>" alt="First slide" width="400px" heigth="50px">

                    <!-- <div class="carousel-caption">
                      Pengguna AjarinCode
                    </div> -->
                  </div>
                  <?php foreach($user as $u){ ?>
                  <div class="item">
                    <img src="<?php echo base_url('assets/images/project-7.jpg') ?>" width="400px" height="50px" alt="Third slide">

                    <div class="carousel-caption">
                      <img src="<?php echo base_url('assets/images/dp/'.$u->path) ?>" width="100px" height="100px"><br>
                      <p style="color: #0628F9"><strong><?php echo $u->username ?></strong></p>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
          </div>
        </section>
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
        <section class="col-lg-8 connectedSortable">
           <!-- quick email widget -->
          <div class="box box-info">
            <div class="box-header">
              <i class="fa fa-envelope"></i>

              <h3 class="box-title">Quick Email</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                <button type="button" class="btn btn-info btn-sm" data-widget="remove" data-toggle="tooltip"
                        title="Remove">
                  <i class="fa fa-times"></i></button>
              </div>
              <!-- /. tools -->
            </div>
            <div class="box-body">
              <form action="#" method="post">
                <div class="form-group">
                  <input type="email" class="form-control" name="emailto" placeholder="Email to:">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control" name="subject" placeholder="Subject">
                </div>
                <div>
                  <!-- <textarea class="textarea" placeholder="Message"
                            style="width: 100%; height: 125px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea> -->
                  <textarea id="summernote" name="isi"></textarea>
                </div>
              </form>
            </div>
            <div class="box-footer clearfix">
              <button type="button" class="pull-right btn btn-default" id="sendEmail">Send
                <i class="fa fa-arrow-circle-right"></i></button>
            </div>
          </div>
        </section>
        <!-- right col -->
      </div>
      <!-- /.row (main row) -->

    </section>
    <!-- /.content -->
  <?php } ?>
  <?php if($this->session->userdata('hak_akses') == "Mentor"){ ?>
    <section class="content-header">
      <h1>
        Dashboard
        <small>AjarinCode</small>
      </h1>
      <ol class="breadcrumb">
        <li class="active"><a href="<?php echo site_url('dashboard') ?>"></a></li>
      </ol>
    </section>
    <section class="content">
      <div class="row">
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('user');
                  $this->db->like('hak_akses','Peserta');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Active User</p>
            </div>
            <div class="icon">
              <i class="fa fa-users"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-red">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('ask_code');
                  echo $this->db->count_all_results();
                ?>
              </h3>
              <p>Tanya Code</p>
            </div>
            <div class="icon">
              <i class="fa fa-question"></i>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  echo $this->db->count_all_results(); 
                ?>
              </h3>
              <p>Materi</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div>
        <!-- <div class="col-lg-3 col-xs-6">
          <div class="small-box bg-green">
            <div class="inner">
              <h3>
                <?php 
                  $this->db->from('materi');
                  echo $this->db->count_all_results(); 
                ?>
              </h3>
              <p>Materi</p>
            </div>
            <div class="icon">
              <i class="fa fa-book"></i>
            </div>
          </div>
        </div> -->
      </div>
      <div class="row">
        <div class="col-md-3">
            <div class="box">
              <div class="box-body bg-blue">
                <div id="weather-one" class="weather-one"></div>
              </div>
            </div>
          </div>
      </div>
    </section>
  <?php } ?>
  <?php if($this->session->userdata('hak_akses') == "Peserta"){ ?>
    <div class="container">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
        <ol class="breadcrumb">
          <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        </ol>
      </section>

      <!-- Main content -->
      <section class="content">
        <!-- <div class="callout callout-success">
          <div id="weather-one" class="weather-one"></div>
        </div> -->
        <div class="callout callout-info">
          <marquee><p>Welcome <?php echo $this->session->userdata('username') ?> Selamat Belajar <i class="fa fa-smile-o"></i></p></marquee>
        </div>
        <div class="row">
          <div class="col-md-3">
            <div class="box">
              <div class="box-body">
                <?php if($this->session->userdata('id_level') == "1"){ ?>
                  <img src="<?php echo base_url() ?>assets/images/label/fundamental.png" width="100px" height="100px">
                  <label class="text-center">Level Fundamental</label>
                <?php }?>
                <?php if($this->session->userdata('id_level') == "2"){ ?>
                  <img src="<?php echo base_url() ?>assets/images/label/standart.png" width="100px" height="100px">
                  <label class="text-center">Level Standart</label>
                <?php }?>
                <?php if($this->session->userdata('id_level') == "3"){ ?>
                  <img src="<?php echo base_url() ?>assets/images/label/medium.png" width="100px" height="100px">
                  <label class="text-center">Level Medium</label>
                <?php }?>
                <?php if($this->session->userdata('id_level') == "4"){ ?>
                  <img src="<?php echo base_url() ?>assets/images/label/advanced.png" width="100px" height="100px">
                  <label class="text-center">Level Advanced</label>
                <?php }?>
              </div>
            </div>
          </div>
          <div class="col-md-3">
            <div class="box">
              <div class="box-body">
                <img src="<?php echo base_url() ?>assets/images/event.png" width="100px" height="100px">
                <label class="control-label text-center">
                <?php 
                  $this->db->from('participant');
                  $this->db->like('id_user',$this->session->userdata('id'));
                  echo $this->db->count_all_results();
                ?>
                Event Diikuti
                </label>
              </div>
            </div>
          </div>
          <div class="col-md-3"></div>
          <div class="col-md-3">
            <div class="box">
              <div class="box-body bg-blue">
                <div id="weather-one" class="weather-one"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4">
            <div class="box box-solid">
            <div class="box-header with-border">
              <h3 class="box-title">Pengguna AjarinCode</h3>
            </div>
            <div class="box-body">
              <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <div class="carousel-inner">
                  <div class="item active">
                    <img src="<?php echo base_url('assets/images/image.png') ?>" alt="First slide" width="400px" heigth="50px">

                    <!-- <div class="carousel-caption">
                      Pengguna AjarinCode
                    </div> -->
                  </div>
                  <?php foreach($user as $u){ ?>
                  <div class="item">
                    <img src="<?php echo base_url('assets/images/project-7.jpg') ?>" width="400px" height="50px" alt="Third slide">

                    <div class="carousel-caption">
                      <img src="<?php echo base_url('assets/images/dp/'.$u->path) ?>" width="100px" height="100px"><br>
                      <p style="color: #0628F9"><strong><?php echo $u->username ?></strong></p>
                    </div>
                  </div>
                  <?php } ?>
                </div>
                <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                  <span class="fa fa-angle-left"></span>
                </a>
                <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                  <span class="fa fa-angle-right"></span>
                </a>
              </div>
            </div>
          </div>
          </div>
        </div>
        <!-- /.box -->
      </section>
      <!-- /.content -->
    </div>
    <!-- /.container -->
  <?php } ?>
  </div>
<!-- ChartJS -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/chart.js/Chart.min.js"></script>
  <script>
 
           var lineChartData = {
                labels : <?php echo json_encode($merk);?>,
                datasets : [
                     
                    {
                        fillColor: "rgba(60,141,188,0.9)",
                        strokeColor: "rgba(60,141,188,0.8)",
                        pointColor: "#3b8bba",
                        pointStrokeColor: "#fff",
                        pointHighlightFill: "#fff",
                        pointHighlightStroke: "rgba(152,235,239,1)",
                        data : <?php echo json_encode($stok);?>
                    }
 
                ]
                 
            }
 
        var myLine = new Chart(document.getElementById("canvas").getContext("2d")).Line(lineChartData);
         
    </script>