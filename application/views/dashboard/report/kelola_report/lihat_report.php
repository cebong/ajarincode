<div class="content-wrapper">
	<section class="content-header">
		<h1>
			View Report
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/report') ?>">Data Report</a></li>
			<li><a href="#">View Report</a></li>
		</ol>
	</section>
	<section class="content">
		 <div class="row">
		 	<div class="col-md-12">
		 		<div class="box">
		 			<div class="box-body">
		 				<?php foreach($report as $r){ ?>
		 					<?php if($r->path_report!=null){ ?>
				 				<div class="form-group">
				 					<label class="control-label">Kode Report :</label>
				 					<input type="text" name="id" class="form-control" value="<?php echo $r->id_report ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Pelapor :</label>
				 					<input type="text" name="user" class="form-control" value="<?php echo $r->username ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Jenis Report :</label>
				 					<input type="text" name="jenis" class="form-control" value="<?php echo $r->jenis_report ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Isi Report</label>
				 					<textarea name="isi" class="form-control" style="height: 250px;" readonly><?php echo $r->isi_report ?></textarea>
				 				</div>

				 				<div class="form-group">
				 					<label class="control-label">Screenshoot :</label><br>
				 					<p class="easyzoom easyzoom--overlay">
									<a href="<?php echo base_url('report/'.$r->path_report) ?>">
										<img src="<?php echo base_url('report/'.$r->path_report) ?>" width="450px" height="250px">
									</a>
								</p>
				 				</div>
			 				<?php } ?>
			 				<?php if($r->path_report==null){ ?>
			 					<div class="form-group">
				 					<label class="control-label">Kode Report :</label>
				 					<input type="text" name="id" class="form-control" value="<?php echo $r->id_report ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Pelapor :</label>
				 					<input type="text" name="user" class="form-control" value="<?php echo $r->username ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Jenis Report :</label>
				 					<input type="text" name="jenis" class="form-control" value="<?php echo $r->jenis_report ?>" readonly>
				 				</div>
				 				<div class="form-group">
				 					<label class="control-label">Isi Report</label>
				 					<textarea name="isi" class="form-control" style="height: 250px;" readonly><?php echo $r->isi_report ?></textarea>
				 				</div>
			 				<?php } ?>
		 				<?php } ?>
		 			</div>
		 		</div>
		 	</div>
		 </div>
	</section>
</div>