<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Data Report Bug
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/report') ?>">Data Report</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#menunggu" data-toggle="tab">Report Menunggu</a></li>
						<li><a href="#diproses" data-toggle="tab">Report Diproses</a></li>
						<li><a href="#cleared" data-toggle="tab">Report Cleared</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="menunggu">
							<div class="nav-tabs-custom">
								<ul class="nav nav-tabs">
									<li class="active"><a href="#low" data-toggle="tab">Prioritas Low</a></li>
									<li><a href="#medium" data-toggle="tab">Prioritas Medium</a></li>
									<li><a href="#high" data-toggle="tab">Prioritas High</a></li>
								</ul>
								<div class="tab-content">
									<div class="tab-pane active" id="low">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Kode Report</th>
															<th class="text-center">Pelapor</th>
															<th class="text-center">Jenis Report</th>
															<th class="text-center">Status</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no = $this->uri->segment(3) + 1; foreach($report as $r){ ?>
														<?php if($r->status_report=="Menunggu"){ ?>
															<?php if($r->jenis_report=="Low"){ ?>
														<tbody>
															<tr>
																<td class="text-center"><?php echo $no++ ?></td>
																<td class="text-center"><?php echo $r->id_report ?></td>
																<td><?php echo $r->username ?></td>
																<td class="text-center"><?php echo $r->jenis_report ?></td>
																<td class="text-center <?php if($r->status_report=="Menunggu"){echo "bg-red";}elseif($r->status_report=="Diproses"){echo "bg-green";}else{echo "bg-aqua";} ?>"><?php echo $r->status_report ?></td>
																<td class="text-center">
																	<a href="<?php echo site_url('report/view_report/'.$r->id_report) ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>
																	<a href="<?php echo site_url('report/proses_report/'.$r->id_report) ?>" class="btn btn-warning btn-xs" <?php if($r->status_report=="Diproses" || $r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-refresh"></i></a>
																	<a href="<?php echo site_url('report/finish_report/'.$r->id_report) ?>" class="btn btn-success btn-xs" <?php if($r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
																	 <a href="#" data-url="<?php echo site_url('report/hapus_report/'.$r->id_report) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
																</td>
															</tr>
														</tbody>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>	
									</div>
									<div class="tab-pane" id="medium">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Kode Report</th>
															<th class="text-center">Pelapor</th>
															<th class="text-center">Jenis Report</th>
															<th class="text-center">Status</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no = $this->uri->segment(3) + 1; foreach($report as $r){ ?>
														<?php if($r->status_report=="Menunggu"){ ?>
															<?php if($r->jenis_report=="Medium"){ ?>
														<tbody>
															<tr>
																<td class="text-center"><?php echo $no++ ?></td>
																<td class="text-center"><?php echo $r->id_report ?></td>
																<td><?php echo $r->username ?></td>
																<td class="text-center"><?php echo $r->jenis_report ?></td>
																<td class="text-center <?php if($r->status_report=="Menunggu"){echo "bg-red";}elseif($r->status_report=="Diproses"){echo "bg-green";}else{echo "bg-aqua";} ?>"><?php echo $r->status_report ?></td>
																<td class="text-center">
																	<a href="<?php echo site_url('report/view_report/'.$r->id_report) ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>
																	<a href="<?php echo site_url('report/proses_report/'.$r->id_report) ?>" class="btn btn-warning btn-xs" <?php if($r->status_report=="Diproses" || $r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-refresh"></i></a>
																	<a href="<?php echo site_url('report/finish_report/'.$r->id_report) ?>" class="btn btn-success btn-xs" <?php if($r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
																	 <a href="#" data-url="<?php echo site_url('report/hapus_report/'.$r->id_report) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
																</td>
															</tr>
														</tbody>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>	
									</div>
									<div class="tab-pane" id="high">
										<div class="box">
											<div class="box-body">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th class="text-center">#</th>
															<th class="text-center">Kode Report</th>
															<th class="text-center">Pelapor</th>
															<th class="text-center">Jenis Report</th>
															<th class="text-center">Status</th>
															<th class="text-center">Action</th>
														</tr>
													</thead>
													<?php $no = $this->uri->segment(3) + 1; foreach($report as $r){ ?>
														<?php if($r->status_report=="Menunggu"){ ?>
															<?php if($r->jenis_report=="High"){ ?>
														<tbody>
															<tr>
																<td class="text-center"><?php echo $no++ ?></td>
																<td class="text-center"><?php echo $r->id_report ?></td>
																<td><?php echo $r->username ?></td>
																<td class="text-center"><?php echo $r->jenis_report ?></td>
																<td class="text-center <?php if($r->status_report=="Menunggu"){echo "bg-red";}elseif($r->status_report=="Diproses"){echo "bg-green";}else{echo "bg-aqua";} ?>"><?php echo $r->status_report ?></td>
																<td class="text-center">
																	<a href="<?php echo site_url('report/view_report/'.$r->id_report) ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>
																	<a href="<?php echo site_url('report/proses_report/'.$r->id_report) ?>" class="btn btn-warning btn-xs" <?php if($r->status_report=="Diproses" || $r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-refresh"></i></a>
																	<a href="<?php echo site_url('report/finish_report/'.$r->id_report) ?>" class="btn btn-success btn-xs" <?php if($r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
																	 <a href="#" data-url="<?php echo site_url('report/hapus_report/'.$r->id_report) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
																</td>
															</tr>
														</tbody>
															<?php } ?>
														<?php } ?>
													<?php } ?>
												</table>
												<?php echo $this->pagination->create_links() ?>
											</div>
										</div>	
									</div>
								</div>
							</div>	
						</div>
						<div class="tab-pane" id="diproses">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">#</th>
												<th class="text-center">Kode Report</th>
												<th class="text-center">Pelapor</th>
												<th class="text-center">Jenis Report</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no = $this->uri->segment(3) + 1; foreach($report as $r){ ?>
											<?php if($r->status_report=="Diproses"){ ?>
											<tbody>
												<tr>
													<td class="text-center"><?php echo $no++ ?></td>
													<td class="text-center"><?php echo $r->id_report ?></td>
													<td><?php echo $r->username ?></td>
													<td class="text-center"><?php echo $r->jenis_report ?></td>
													<td class="text-center <?php if($r->status_report=="Menunggu"){echo "bg-red";}elseif($r->status_report=="Diproses"){echo "bg-green";}else{echo "bg-aqua";} ?>"><?php echo $r->status_report ?></td>
													<td class="text-center">
														<a href="<?php echo site_url('report/view_report/'.$r->id_report) ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('report/proses_report/'.$r->id_report) ?>" class="btn btn-warning btn-xs" <?php if($r->status_report=="Diproses" || $r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-refresh"></i></a>
														<a href="<?php echo site_url('report/finish_report/'.$r->id_report) ?>" class="btn btn-success btn-xs" <?php if($r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
														 <a href="#" data-url="<?php echo site_url('report/hapus_report/'.$r->id_report) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
													</td>
												</tr>
											</tbody>
											<?php } ?>
										<?php } ?>
									</table>
									<?php echo $this->pagination->create_links() ?>
								</div>
							</div>	
						</div>
						<div class="tab-pane" id="cleared">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">#</th>
												<th class="text-center">Kode Report</th>
												<th class="text-center">Pelapor</th>
												<th class="text-center">Jenis Report</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no = $this->uri->segment(3) + 1; foreach($report as $r){ ?>
											<?php if($r->status_report=="Clear"){ ?>
											<tbody>
												<tr>
													<td class="text-center"><?php echo $no++ ?></td>
													<td class="text-center"><?php echo $r->id_report ?></td>
													<td><?php echo $r->username ?></td>
													<td class="text-center"><?php echo $r->jenis_report ?></td>
													<td class="text-center <?php if($r->status_report=="Menunggu"){echo "bg-red";}elseif($r->status_report=="Diproses"){echo "bg-green";}else{echo "bg-aqua";} ?>"><?php echo $r->status_report ?></td>
													<td class="text-center">
														<a href="<?php echo site_url('report/view_report/'.$r->id_report) ?>" class="btn btn-info btn-xs" target="_blank"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('report/proses_report/'.$r->id_report) ?>" class="btn btn-warning btn-xs" <?php if($r->status_report=="Diproses" || $r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-refresh"></i></a>
														<a href="<?php echo site_url('report/finish_report/'.$r->id_report) ?>" class="btn btn-success btn-xs" <?php if($r->status_report=="Clear"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
														 <a href="#" data-url="<?php echo site_url('report/hapus_report/'.$r->id_report) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i></a>
													</td>
												</tr>
											</tbody>
											<?php } ?>
										<?php } ?>
									</table>
									<?php echo $this->pagination->create_links() ?>
								</div>
							</div>	
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	// Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Report",
          text: "Yakin ingin menghapus report ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FD0404",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
</script>