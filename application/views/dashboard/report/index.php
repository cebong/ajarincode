<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Report
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('dashboard/bug') ?>">Report</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php echo form_open_multipart('report/create_report') ?>
							<div class="form-group">
								<label class="control-label">Kode Report :</label>
								<input type="text" name="id" class="form-control" value="<?php echo $kodeunik ?>" readonly>
							</div>
							<div class="form-group">
								<input type="hidden" name="user" class="form-control" value="<?php echo $this->session->userdata('id') ?>">
							</div>
							<div class="form-group">
								<label class="control-label">Jenis Report :</label>
								<select class="form-control" name="jenis">
									<option>-- Pilih Jenis Report --</option>
									<option value="Low">Low</option>
									<option value="Medium">Medium</option>
									<option value="High">High</option>
								</select>
							</div>
							<div class="form-group">
								<label class="control-label">Tulis Report :</label>
								<textarea class="form-control" style="height: 250px" name="isi"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">Screenshot <small>(Optional Max. Size 2MB)</small> :</label>
								<input type="file" name="img" class="form-control" accept="image/*">
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-primary"><i class="fa fa-exclamation"></i>&nbsp;Lapor</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>