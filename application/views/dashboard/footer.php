<!-- /.content-wrapper -->
<footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2018 All rights reserved.
  </footer>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/jquery/dist/jquery.min.js"></script>
<script src="http://cdn.rawgit.com/ashleighy/emoji.js/master/emoji.js.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
<!-- Kontrol Kabupaten -->
<script type="text/javascript">
          $(document).ready(function(){
              $('#provinsi').change(function(){
                  var id=$(this).val();
                  $.ajax({
                      url : "<?php echo base_url();?>home/get_kabupaten",
                      method : "POST",
                      data : {id: id},
                      async : false,
                      dataType : 'json',
                      success: function(data){
                          var html = '';
                          var i;
                          for(i=0; i<data.length; i++){
                              //html += '<option>'+data[i].nama_kabupaten+'</option>';
                              html += '<option value='+data[i].id_kabupaten+'>'+data[i].nama_kabupaten+'</option>';
                          }
                          $('.kabupaten').html(html);

                      }
                  });
              });
          });
      </script>
<!-- Show/Hide Komen -->
<script>
  $(document).ready(function() {
  
    $("#tombol").click(function() {
      $("#komentar").toggle("slow");
    })
  
  });
</script>
<!--Kontrol Menu-->
<script type="text/javascript">
  $(function() {
    $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
  });
</script>
<!-- jQuery UI 1.11.4 -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Baca File Gambar -->
<script type="text/javascript">
  function bacaGambar(input) {
   if (input.files && input.files[0]) {
      var reader = new FileReader();
 
      reader.onload = function (e) {
          $('#gambar_nodin').attr('src', e.target.result);
      }
 
      reader.readAsDataURL(input.files[0]);
   }
  }
  $("#preview_gambar").change(function(){
     bacaGambar(this);
  });
</script>
<!-- Penghitung Jumlah Karakter -->
<script type="text/javascript">
  $(document).ready(function(){
      var left = 500
      $('#text_counter').text('Characters left: ' + left);
     
        $('#textarea').keyup(function () {
     
        left = 500 - $(this).val().length;
     
        if(left < 0){
          $('#text_counter').addClass("overlimit");
           $('#posting').attr("disabled", true);
        }
        else{
          $('#text_counter').removeClass("overlimit");
          $('posting').attr("disabled", false);
        }
     
        $('#text_counter').text('Characters left: ' + left);
      });
    });     
</script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button);
</script>
<!-- jvectormap -->
<script src="<?php echo base_url() ?>assets/dashboard/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
<script src="<?php echo base_url() ?>assets/dashboard/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
<!-- datepicker -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() ?>assets/dashboard/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
<!-- Slimscroll -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>assets/dashboard/dist/js/adminlte.min.js"></script>
<!-- iCheck 1.0.1 -->
<script src="<?php echo base_url() ?>assets/dashboard/plugins/iCheck/icheck.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- Sweet Alert -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/sweet-alert/sweet-alert.min.js"></script>
<!-- EasyZoom -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/easyzoom/js/easyzoom.js"></script>
<!-- Export To Excel -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/exporttoexcel/jquery.table2excel.min.js"></script>
<!-- Weather -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/weather/jquery.simpleWeather.min.js"></script>
<!-- Mask Money -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/maskMoney/jquery.mask.min.js"></script>
<!-- summernote -->
<script src="<?php echo base_url() ?>assets/dashboard/bower_components/summernote/dist/summernote.min.js"></script>
<!-- Emoji -->
<script type="text/javascript" src="<?php echo base_url() ?>assets/dashboard/bower_components/jqueryemoji/js/jQueryEmoji.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/dashboard/bower_components/emojionearea/dist/emojionearea.min.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>assets/dashboard/bower_components/emojionearea/dist/emojione.min.js"></script>
<script>
  
  //initializeS Summernote Editor
  $(document).ready(function() {
    $('#summernote').summernote({
      height: 300,
    });
  });
  //Initialize Select2 Elements
    $('.select2').select2()
  //Date picker
     $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
    $('#datepicker2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
    })
    
  //Flat red color scheme for iCheck
    $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
      checkboxClass: 'icheckbox_flat-green',
      radioClass   : 'iradio_flat-green'
    })
    // EasyZoom
    // Instantiate EasyZoom instances
    var $easyzoom = $('.easyzoom').easyZoom();

    // Setup thumbnails 
    var api1 = $easyzoom.filter('.easyzoom--with-thumbnails').data('easyZoom');

    $('.thumbnails').on('click', 'a', function(e) {
      var $this = $(this);

      e.preventDefault();

      // Use EasyZoom's `swap` method
      api1.swap($this.data('standard'), $this.attr('href'));
    });

    // Setup toggles
    var api2 = $easyzoom.filter('.easyzoom--with-toggle').data('easyZoom');

    $('.toggle').on('click', function() {
      var $this = $(this);

      if ($this.data("active") === true) {
        $this.text("Switch on").data("active", false);
        api2.teardown();
      } else {
        $this.text("Switch off").data("active", true);
        api2._init();
      }
    });

    // Wheater
    (function ($) {
    //    "use strict";

        function loadWeather(location, woeid) {
            $.simpleWeather({
                location: location,
                woeid: woeid,
                unit: 'f',
                success: function (weather) {

                    html = '<i class="wi wi-yahoo-' + weather.code + '"></i><h2> ' + weather.temp + '&deg;' + weather.units.temp + '</h2>';
                    html += '<div class="city">' + weather.city + ', ' + weather.region + '</div>';
                    html += '<div class="currently">' + weather.currently + '</div>';
                    html += '<div class="celcious">' + weather.alt.temp + '&deg;C</div>';

                    $("#weather-one").html(html);
                },
                error: function (error) {
                    $("#weather-one").html('<p>' + error + '</p>');
                }
            });
        }


        // init
        loadWeather("<?php if($this->session->userdata('asal')=="Badung"){echo "Kuta";}else{echo $this->session->userdata('asal');} ?>", '');

    })(jQuery);
</script>
<!-- Select2 -->
<script>
  $(function () {
    // Materi dataTables
    $('#example2').DataTable() 
    $('#html1').DataTable() 
    $('#css1').DataTable()
    $('#javascript1').DataTable()
    $('#php1').DataTable()
    $('#sql1').DataTable()
  })
</script>
</body>
</html>