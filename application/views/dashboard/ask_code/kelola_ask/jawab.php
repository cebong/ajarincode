<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Jawab Ask Code
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('admin/show_ask') ?>">List Pertanyaan</a></li>
			<li class="active"><a href="#">Jawab</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<?php foreach($ask as $a){ ?>
							<?php if($a->code != null){ ?>
							<form action="<?php echo site_url('ask/kirim_jawaban') ?>" method="post">
								<div class="form-group">
									<input type="hidden" name="responder" class="form-control" value="<?php echo $this->session->userdata('id') ?>">
									<input type="text" name="id" class="form-control" value="<?php echo $a->id_ask ?>" readonly>
								</div>
								<div class="form-group">
									<label class="control-label">Penanya :</label>
									<input type="text" name="username" class="form-control" value="<?php echo $a->username ?>" readonly>
								</div>
								<div class="form-group">
									<label class="control-label">Pertanyaan :</label>
									<textarea class="textarea" name="tanya" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;" readonly><?php echo $a->asking ?></textarea>
								</div>
								<div class="form-group">
									<label class="control-label">Code</label>
									<textarea id="code" name="code" readonly><?php cetak($a->code) ?></textarea>
								</div>
								<div class="form-group">
									<label class="control-label">Berikan Jawaban :</label>
									<textarea class="form-control" name="jawaban" placeholder="Tulis Jawaban Anda"></textarea>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp;Kirim jawaban</button>
								</div>
							</form>
							<?php } ?>
							<?php if($a->code == null){ ?>
								<form action="<?php echo site_url('ask/kirim_jawaban') ?>" method="post">
								<div class="form-group">
									<input type="hidden" name="responder" class="form-control" value="<?php echo $this->session->userdata('id') ?>">
									<input type="hidden" name="id" class="form-control" value="<?php echo $a->id_ask ?>">
								</div>
								<div class="form-group">
									<label class="control-label">Penanya :</label>
									<input type="text" name="username" class="form-control" value="<?php echo $a->username ?>" readonly>
								</div>
								<div class="form-group">
									<label class="control-label">Pertanyaan :</label>
									<input type="text" name="pertanyaan" class="form-control" value="<?php echo $a->asking ?>" readonly>
								</div>
								<div class="form-group">
									<label class="control-label">Berikan Jawaban :</label>
									<textarea class="form-control" name="jawaban" placeholder="Tulis Jawaban Anda"></textarea>
								</div>
								<div class="form-group">
									<button type="submit" class="btn btn-primary"><i class="fa fa-upload"></i>&nbsp;Kirim jawaban</button>
								</div>
							</form>
							<?php } ?>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>
<script>
	var editor = CodeMirror.fromTextArea(document.getElementById('code'), {
    matcBrackets: true,
    lineNumbers: true,
    mode: 'text/html',
    tabMode: 'indent',
    matchTags: {bothTags: true},
    extraKeys: {'Ctrl-J': 'toMatchingTag'}, 
  highlightSelectionMatches: {showToken: /\w/},
  lineNumbers: true
});
</script>