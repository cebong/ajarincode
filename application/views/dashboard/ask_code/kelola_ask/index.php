<div class="content-wrapper">
	<section class="content-header">
		<h1>
			List Ask
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/show_ask') ?>"></a>&nbsp;List Ask</li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">#</th>
									<th class="text-center">Username</th>
									<th class="text-center">Pertanyaan</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($ask as $a){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php cetak($a->id_ask) ?></td>
									<td class="text-center"><?php cetak($a->username) ?></td>
									<td><?php echo $a->asking ?></td>
									<td class="text-center"><?php cetak($a->status_ask) ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('ask/jawab/'.$a->id_ask) ?>" class="btn btn-info btn-xs"><i class="fa fa-eye"></i>&nbsp;Jawab</a>
										<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
						<?php echo $this->pagination->create_links() ?>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>