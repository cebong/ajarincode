<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Request Pertanyaan
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li><a href="<?php echo site_url('dashboard/ask_code') ?>">Tanya Kode</a></li>
			<li class="active"><a href="#">Request</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="box">
					<div class="box-body">
						<form action="<?php echo site_url('ask/tambah') ?>" method="post">
							<div class="form-group">
								<input type="hidden" name="id" class="form-control" value="<?php echo $this->session->userdata('id') ?>">
							</div>
							<div class="form-group">
								<input type="text" name="kupon" class="form-control" value="<?php echo $kodeunik ?>" readonly>
							</div>
							<div class="form-group">
								<label class="control-label">Pertanyaan</label>
								<textarea class="textarea" name="tanya" placeholder="Place some text here" style="width: 100%; height: 200px; font-size: 14px; line-height: 18px; border: 1px solid #dddddd; padding: 10px;"></textarea>
							</div>
							<div class="form-group">
								<label class="control-label">Code</label>
								<textarea id="code" name="code"></textarea>
							</div>
							<div class="form-group">
								<button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp;Tanya</button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>

<script>
	var editor = CodeMirror.fromTextArea(document.getElementById('code'), {
    matcBrackets: true,
    lineNumbers: true,
    mode: 'text/html',
    tabMode: 'indent',
    matchTags: {bothTags: true},
    extraKeys: {'Ctrl-J': 'toMatchingTag'}, 
  highlightSelectionMatches: {showToken: /\w/},
  lineNumbers: true
});
</script>