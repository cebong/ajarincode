<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Ask Code
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('dashboard/ask_code') ?>"> Ask Code</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<a href="<?php echo site_url('ask') ?>" class="btn btn-primary"><i class="fa fa-question"></i>&nbsp; Tanya Code</a>
				<div class="box">
					<div class="box-body">
						<table class="table table-bordered">
							<thead>
								<tr>
									<th class="text-center">Ticket</th>
									<th class="text-center">Pertanyaan</th>
									<th class="text-center">Status</th>
									<th class="text-center">Action</th>
								</tr>
							</thead>
							<?php $no=1; foreach($ask as $a){ ?>
							<tbody>
								<tr>
									<td class="text-center"><?php cetak($a->id_ask) ?></td>
									<td><?php echo $a->asking ?></td>
									<td class="text-center <?php if($a->status_ask=="Pending"){echo "bg-red";}else{echo "bg-green";} ?>"><?php cetak($a->status_ask) ?></td>
									<td class="text-center">
										<a href="<?php echo site_url('ask/edit/'.$a->id_ask) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i></a>
										<a href="<?php echo site_url('ask/ceklist/'.$a->id_ask) ?>" class="btn btn-success btn-xs" <?php if($a->status_ask=="Terjawab"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
										<a href="#" class="btn btn-danger btn-xs"><i class="fa fa-trash"></i></a>
									</td>
								</tr>
							</tbody>
							<?php } ?>
						</table>
					</div>
				</div>
			</div>
		</div>
	</section>
</div>