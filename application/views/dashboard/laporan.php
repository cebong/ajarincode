 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kabupaten
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/laporan') ?>">Laporan</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-body">
              <div class="col-md-6">
                <div class="form-group" align="center">
                    <i class="fa fa-user fa-4x"></i>
                    <small><i class="fa fa-file-pdf-o"></i></small><br>
                    <p>Cetak Laporan Peserta</p>
                  <a href="#" data-toggle="modal" data-target="#peserta" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Cetak</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" align="center">
                    <i class="fa fa-user fa-4x"></i>
                    <small><i class="fa fa-file-pdf-o"></i></small><br>
                    <p>Cetak Laporan Mentor</p>
                  <a href="<?php echo site_url('laporan/cetak_mentor') ?>" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Cetak</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" align="center">
                    <i class="fa fa-dollar fa-4x"></i>
                    <small><i class="fa fa-file-pdf-o"></i></small><br>
                    <p>Cetak Laporan Keuangan</p>
                  <a href="#" target="_blank" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Cetak</a>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group" align="center">
                    <i class="fa fa-500px fa-4x"></i>
                    <small><i class="fa fa-file-pdf-o"></i></small><br>
                    <p>Cetak Laporan Event</p>
                  <a href="#" data-toggle="modal" data-target="#event" class="btn btn-primary"><i class="fa fa-print"></i>&nbsp;Cetak</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>  
    <section>
    <!-- /.content -->
    <div class="modal fade" id="peserta">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#level" data-toggle="tab">Level</a></li>
                <li><a href="#kabupaten" data-toggle="tab">Asal</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="level">
                  <form action="<?php echo site_url('laporan/cetak_per_level') ?>" method="post" target="_blank">
                    <div class="form-group">
                      <select class="form-control select2" name="level" style="width: 100%">
                        <option>-- Pilih Level --</option>
                        <?php foreach($level as $l){ ?>
                        <option value="<?php echo $l->id_level ?>"><?php echo $l->nama_level ?></option>
                        <?php  }?>
                      </select>
                    </div>
                    <div class="form-group text-center">
                      <button type="submit" class="btn btn-info"><i class="fa fa-print"></i>&nbsp;Cetak</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" id="kabupaten">
                  <form action="<?php echo site_url('laporan/cetak_per_kabupaten') ?>" method="post" target="_blank">
                    <div class="form-group">
                      <select class="form-control select2" name="kabupaten" style="width: 100%">
                        <option>-- Pilih Kabupaten --</option>
                        <?php foreach($kabupaten as $k){ ?>
                        <option value="<?php echo $k->id_kabupaten ?>"><?php echo $k->nama_kabupaten ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group text-center">
                      <button type="submit" class="btn btn-info"><i class="fa fa-print"></i>&nbsp;Cetak</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
    <!-- Event -->
    <div class="modal fade" id="event">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
          </div>
          <div class="modal-body">
            <div class="nav-tabs-custom">
              <ul class="nav nav-tabs">
                <li class="active"><a href="#level" data-toggle="tab">Jenis Event</a></li>
                <li><a href="#kabupaten" data-toggle="tab">Asal</a></li>
              </ul>
              <div class="tab-content">
                <div class="tab-pane active" id="level">
                  <form action="<?php echo site_url('laporan/cetak_event_jenis') ?>" method="post" target="_blank">
                    <div class="form-group">
                      <select class="form-control select2" name="jenis" style="width: 100%">
                        <option>-- Pilih Jenis Event --</option>
                        <?php foreach($jenis as $j){ ?>
                        <option value="<?php echo $j->id_jenis ?>"><?php echo $j->nama_jenis ?></option>
                        <?php  }?>
                      </select>
                    </div>
                    <div class="form-group text-center">
                      <button type="submit" class="btn btn-info"><i class="fa fa-print"></i>&nbsp;Cetak</button>
                    </div>
                  </form>
                </div>
                <div class="tab-pane" id="kabupaten">
                  <form action="<?php echo site_url('laporan/cetak_per_kabupaten') ?>" method="post" target="_blank">
                    <div class="form-group">
                      <select class="form-control select2" name="kabupaten">
                        <option>-- Pilih Kabupaten --</option>
                        <?php foreach($kabupaten as $k){ ?>
                        <option value="<?php echo $k->id_kabupaten ?>"><?php echo $k->nama_kabupaten ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <button type="submit" class="btn btn-info"><i class="fa fa-print"></i>&nbsp;Cetak</button>
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
          </div>
        </div>
        <!-- /.modal-content -->
      </div>
      <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->
  </div>
  <!-- /.content-wrapper -->
<!-- <script>
  $(document).ready(function(){
    $("#form-input").css("display","none"); //Menghilangkan form-input ketika pertama kali dijalankan
    $(".detail").click(function(){ //Memberikan even ketika class detail di klik (class detail ialah class radio button)
      if ($("input[name='level']:checked").val() == "level" ) { //Jika radio button "berbeda" dipilih maka tampilkan form-inputan
      $("#form-input").slideDown("fast"); //Efek Slide Down (Menampilkan Form Input)
      } else {
      $("#form-input").slideUp("fast"); //Efek Slide Up (Menghilangkan Form Input)
      }
    });
  });
</script> -->