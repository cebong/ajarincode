<div class="content-wrapper">
	<section class="content-header">
		<h1>Event AjarinCode</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Event</a></li>
        </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<?php foreach($event as $e){ ?>
				<div class="box">
					<div class="box-body">
						<form action="<?php echo site_url('event/partisipasi') ?>" method="post">
							<div class="col-md-3">
								<img src="<?php echo base_url('assets/event/image/'.$e->path_event) ?>" width="250px" height="250px">
							</div>
							<div class="col-md-4">
								<label class="control-label">Nama Event :</label>
								<p><?php echo $e->nama_event ?></p>
								<input type="hidden" name="id" value="<?php echo $e->id_event ?>">
								<label class="control-label">Deskripsi Event :</label>
								<p><?php echo $e->deskripsi_event ?></p>
								<label class="control-label">Tanggal Pelaksanaan :</label>
								<p><?php echo date("d F Y", strtotime($e->tgl_mulai))." s/d ".date("d F Y", strtotime($e->tgl_akhir)) ?></p>
								<?php if($e->status_event == "Open Participant"){ ?>
									<button type="submit" class="btn btn-info"><i class="fa fa-registered"></i>&nbsp;Partisipasi</a>
								<?php } ?>
							</div>
							<div class="col-md-4">
								<label class="control-label">Price :</label>
								<p>Rp <?php echo number_format($e->harga_event,0,',','.') ?></p>
								<input type="hidden" name="harga" value="<?php echo $e->harga_event ?>">
								<label class="control-label">Status :</label>
								<p><?php echo $e->status_event ?></p>
								<label class="control-label">Level Partisipasi :</label>
								<p><?php echo $e->id_level ?></p>
								<input type="hidden" name="order_id" value="<?php echo $this->m_invoice->get_no_invoice() ?>">
							</div>
						</form>
					</div>
				</div>
				<?php } ?>
			</div>
		</div>
	</section>
</div>