<div class="content-wrapper">
	<section class="content-header">
		<h1>
        Daftar Tagihan
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('tagihan') ?>">Tagihan</a></li>
      </ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#event" data-toggle="tab">Tagihan Event</a></li>
						<li><a href="#level" data-toggle="tab">Tagihan Upgrade Level</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="event">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Tagihan</th>
												<th class="text-center">Pembayaran</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($tagihan as $t){ ?>
										<tbody>
											<tr>
												<td class="text-center"><?php echo $no++ ?></td>
												<td><?php echo $t->nama_event ?></td>
												<td>Rp <?php echo number_format($t->total_bayar_event,2,',','.') ?></td>
												<td class="text-center"><?php echo $t->status_participant ?></td>
												<td class="text-center">
													<a href="<?php echo site_url('invoice/'.$t->id_bayar_event) ?>" class="btn btn-info btn-sm" <?php if($t->status_participant=="Lunas"){echo "disabled";} ?>><i class="fa fa-credit-card"></i></a>
												</td>
											</tr>
										</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="level">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No</th>
												<th class="text-center">Tagihan</th>
												<th class="text-center">Pembayaran</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($level as $l){ ?>
										<tbody>
											<tr>
												<td class="text-center"><?php echo $no++ ?></td>
												<td><?php echo $l->nama_level ?></td>
												<td>Rp <?php echo number_format($l->total_bayar_level,2,',','.')  ?></td>
												<td><?php echo $l->status_pembayaran ?></td>
												<td class="text-center">
													<a href="<?php echo site_url('invoice_upgrade/'.$l->id_bayar_level) ?>" class="btn btn-info btn-xs" <?php if($l->status_pembayaran=="Lunas"){echo "disabled";} ?>><i class="fa fa-credit-card"></i></a>
												</td>
											</tr>
										</tbody>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
          <div class="modal fade" id="view" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                  <h4 class="modal-title" id="myModalLabel">Form Tambah Mentor</h4>
                </div>
                <div class="modal-body">
                 <!-- <div id="bukti_bayar_event"></div> -->
                 <input type="text" name="bukti" id="bukti_bayar_event">
                 <!-- <img src="<?php echo base_url('assets/event/bukti_bayar/') ?>" id="bukti_bayar_event"> -->
                </div>
                <div class="modal-footer">
                  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
              </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
          </div>
        <!-- /.modal -->
	</section>
</div>