<div class="content-wrapper">
	<section class="content-header"></section>
	<section class="content">
		<!-- Post -->
        <?php foreach($posting as $p){ ?>
                <div class="box">
                <div class="post">
        	        <div class="user-block">
            			<img class="img-circle img-bordered-sm" src="<?php echo base_url('assets/images/dp/'.$p->path) ?>" alt="user image">
                        <span class="username">
                          <?php echo $p->username ?>
                          <a href="#" class="pull-right btn-box-tool"><i class="fa fa-times"></i></a>
                        </span>
                    	<span class="description">Shared - <?php echo date("h:m:s", strtotime($p->tgl_timeline)) ?></span>
                  	</div>
                  <!-- /.user-block -->
                  	<p>
                    	<?php cetak($p->isi_timeline) ?>
                  	</p>
                  	<ul class="list-inline">
                    	<!-- <li><a href="#" class="link-black text-sm"><i class="fa fa-share margin-r-5"></i> Share</a></li> -->
                    	<!-- <li><a href="#" id="tombol_show" class="link-black text-sm"><i class="fa fa-thumbs-o-up margin-r-5"></i> Like</a></li> -->
                    	<li class="pull-right"><a href="#" class="link-black text-sm" id="tombol"><i class="fa fa-comments-o margin-r-5"></i> Comments
                        (<?php 
                          $this->db->from('komentar');
                          $this->db->where('id_timeline',$p->id_timeline);
                          echo $this->db->count_all_results();
                          ?>)</a></li>
                  	</ul>
                    <div id="komentar" style="display: none">
                      <div class="col-md-2">
                        <?php foreach($komentar as $k){ ?>
                          <?php if($k->id_timeline==$p->id_timeline){ ?>
                        <div class="form-group">
                          <p><img src="<?php echo base_url('assets/images/dp/'.$k->path) ?>" width="20px" height="20px"><?php echo $k->username ?></p>
                        </div>
                        <div class="form-group">
                          <p><?php echo $k->isi_komentar  ?></p>
                        </div>
                          <?php } ?>
                        <?php } ?>
                      </div>
                    </div>
                    <form action="<?php echo site_url('lini_masa/kirim_komen') ?>" method="post">
                      <input type="hidden" name="id" class="form-control" value="<?php echo $p->id_timeline ?>">
                  	   <input class="form-control input-sm" name="komen" type="text" placeholder="Type a comment">
                    </form>
                </div>
                </div>
        <?php } ?>
        <!-- /.post -->
	</section>
</div>