 <script>
   
 </script>
 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Level
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/level') ?>">Level</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
             <form action="<?php echo site_url('level/tambah') ?>" method="post">
               <input type="text" name="level" class="form-control" placeholder="Nama Level">
               <br>
               <input type="text" name="harga" class="form-control" placeholder="Harga Level">
               <br>
               <label>Status :</label><br>
               <input type="radio" name="status" class="flat-red" value="Aktif">&nbsp;Aktif
               <input type="radio" name="status" class="flat-red" value="Tidak Aktif">&nbsp;Tidak Aktif
               <br><br>
               <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-save"></i>&nbsp;Simpan</button>
             </form>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <?php if($this->session->flashdata('status') == "berhasil"){ ?>
            <div class="alert alert-success text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <?php if($this->session->flashdata('status') == "gagal"){ ?>
            <div class="alert alert-danger text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <div class="box">
            <div class="box-body"> 
              <table class="table table-bordered table-striped" id="mydata">
                <thead>
                <tr>
                  <th class="text-center">No</th> 
                  <th class="text-center">Nama Level</th>
                  <th class="text-center">Harga</th>
                  <th class="text-center">Status</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <?php $no = 1; foreach($level as $l){ ?>
                <tbody>
                  <td class="text-center"><?php cetak($no++) ?></td>
                  <td><?php cetak($l->nama_level) ?></td>
                  <td>Rp <?php cetak(number_format($l->harga_level,0,',','.')) ?></td>
                  <td class="text-center"><?php cetak($l->status_level) ?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url('level/edit/'.$l->id_level) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                    <a href="#" data-url="<?php echo site_url('level/hapus/'.$l->id_level) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                  </td>
                </tbody>
                <?php } ?>
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Modal Edit -->
      <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 id="myModalLabel">Edit</h3>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal templatemo-create-account templatemo-container-modal" role="form" action="<?php echo site_url('kabupaten/update');?>" method="post">
              <div class="form-inner">
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id_level">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" style="margin-bottom:10px;">Nama Kabupaten</label>
                    <input type="text" class="form-control" name="level" id="nama_level" placeholder="Nama Level"  required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" style="margin-bottom: 10px;">Status Level</label><br>
                    <input type="radio" name="status" class="flat-red status" value="Aktif" id="status_level">Aktif
                    <input type="radio" name="status" class="flat-red status" value="Tidak Aktif" id="status_level">Tidak Aktif
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                    <button class="btn btn-warning" type="button" data-dismiss="modal" aria-label="Close">
                      Tutup
                    </button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>  
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <script type="text/javascript">
    // Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Level",
          text: "Yakin ingin menghapus level ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FD0404",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });

    // //Format Currency
    // var tanpa_rupiah = document.getElementById('tanpa-rupiah');
    // tanpa_rupiah.addEventListener('keyup', function(e)
    // {
    //     tanpa_rupiah.value = formatRupiah(this.value);
    // });

    // /* Fungsi */
    // function formatRupiah(angka, prefix)
    // {
    //     var number_string = angka.replace(/[^,\d]/g, '').toString(),
    //         split    = number_string.split(','),
    //         sisa     = split[0].length % 3,
    //         rupiah     = split[0].substr(0, sisa),
    //         ribuan     = split[0].substr(sisa).match(/\d{3}/gi);
            
    //     if (ribuan) {
    //         separator = sisa ? '.' : '';
    //         rupiah += separator + ribuan.join('.');
    //     }
        
    //     rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    //     return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    // }

    $(document).ready(function(){
      // Format mata uang.
      $( '#uang' ).mask('000.000.000', {reverse: true});
 
    })

  </script>