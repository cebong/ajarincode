 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Kabupaten
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/kabupaten') ?>">Kabupaten</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
              <form action="<?php echo site_url('kabupaten/tambah') ?>" method="post">
                <div class="form-group">
                <select name="provinsi" class="form-control select2">
                  <option>-- Pilih Provinsi --</option>
                  <?php foreach($provinsi as $p){ ?>
                  <option value="<?php echo $p->id_provinsi ?>"><?php echo $p->nama_provinsi ?></option>
                  <?php } ?>
                </select>
                </div>
                <div class="form-group">
                <input type="text" name="kabupaten" class="form-control" placeholder="Nama Kabupaten">
                </div>
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i>&nbsp; Simpan</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
              <form method="post" action="<?php echo site_url('kabupaten/pencarian') ?>">
                <label class="control-label">Form Pencarian</label>
                <div class="input-group input-group-sm">
                  <input type="text" class="form-control" name="keyword" placeholder="Masukkan kata kunci">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                      </span>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <?php if($this->session->flashdata('status') == "gagal"){ ?>
            <div class="alert alert-danger text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <?php if($this->session->flashdata('status') == "berhasil"){ ?>
            <div class="alert alert-success text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <div class="box">
            <div class="box-body">
              <table class="table table-bordered">
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Kabupaten</th>
                  <th class="text-center">Action</th>
                </tr>
                <?php $no= $this->uri->segment(3) + 1; foreach($kabupaten as $k){ ?>
                <tr>
                  <td><?php cetak($no++) ?></td>
                  <td><?php cetak($k->nama_kabupaten) ?></td>
                  <td class="text-center">
                    <a href="<?php echo site_url('kabupaten/edit/'.$k->id_kabupaten) ?>" class="btn btn-info btn-xs"><i class="fa fa-edit"></i>&nbsp;Edit</a>
                    <a href="#" data-url="<?php echo site_url('kabupaten/hapus/'.$k->id_kabupaten) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                  </td>
                </tr>
              <?php } ?>
              </table>
              <?php echo $this->pagination->create_links() ?>
            </div>
          </div>
        </div>
      </div>
      <!-- Modal Edit -->
      <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 id="myModalLabel">Edit</h3>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal templatemo-create-account templatemo-container-modal" role="form" action="<?php echo site_url('kabupaten/update');?>" method="post">
              <div class="form-inner">
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id_kabupaten">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <select class="form-control select2" name="provinsi" id="id_provinsi">
                      <option>-- Pilih Provinsi --</option>
                      <?php $data['provinsi'] = $this->m_provinsi->list_provinsi()->result();foreach($provinsi as $p){ ?>
                      <option value="<?php cetak($p->id_provinsi) ?>"><?php cetak($p->nama_provinsi) ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" style="margin-bottom:10px;">Nama Kabupaten</label>
                    <input type="text" class="form-control" name="kabupaten" id="nama_kabupaten" placeholder="Nama Provinsi"  required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                    <button class="btn btn-warning" type="button" data-dismiss="modal" aria-label="Close">
                      Tutup
                    </button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>  
    <section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    // Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Kabupaten",
          text: "Yakin ingin menghapus kabupaten ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FD0404",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });
  </script>