 <!-- Content Wrapper. Contains page content -->
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Data Provinsi
      </h1>
      <ol class="breadcrumb">
        <li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="<?php echo site_url('admin/provinsi') ?>">Provinsi</a></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
              <form action="<?php echo site_url('provinsi/tambah') ?>" method="post">
                <input type="text" name="provinsi" class="form-control" placeholder="Nama Provinsi">
                <br>
                <button type="submit" class="btn btn-success btn-xs"><i class="fa fa-save"></i>&nbsp;Simpan</button>
              </form>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="box">
            <div class="box-body">
              <form action="<?php echo site_url('provinsi/pencarian') ?>" method="post">
                <label class="control-label">Pencarian</label>
                <div class="input-group input-group-sm">
                  <input type="text" class="form-control" name="keyword" placeholder="Masukkan kata kunci">
                      <span class="input-group-btn">
                        <button type="submit" class="btn btn-info btn-flat">Go!</button>
                      </span>
                </div>
              </form>
            </div>
          </div>
        </div>
        <div class="col-xs-12">
          <?php if($this->session->flashdata('status') == "gagal"){ ?>
            <div class="alert alert-danger text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <?php if($this->session->flashdata('status') == "berhasil"){ ?>
            <div class="alert alert-success text-center"><?php echo $this->session->flashdata('message') ?></div>
          <?php } ?>
          <div class="box">
            <div class="box-body">  
              <table class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th class="text-center">No</th>
                  <th class="text-center">Nama Provinsi</th>
                  <th class="text-center">Action</th>
                </tr>
                </thead>
                <?php $no =  $this->uri->segment(3) + 1; foreach($provinsi as $p){ ?>
                <tbody>
                  <tr>
                    <td class="text-center"><?php cetak($no++)  ?></td>
                    <td><?php cetak($p->nama_provinsi)  ?></td>
                    <td class="text-center">
                      <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#edit" onclick="edit(<?php cetak($p->id_provinsi) ?>)"><i class="fa fa-edit"></i>&nbsp;Edit</button>
                      <a href="#" data-url="<?php echo site_url('provinsi/hapus/'.$p->id_provinsi) ?>" class="btn btn-danger btn-xs confirm_delete"><i class="fa fa-trash"></i>&nbsp;Hapus</a>
                    </td>
                  </tr>
                </tbody>
                <?php } ?>
              </table>
              <?php echo $this->pagination->create_links() ?>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Modal Edit -->
      <div class="modal fade" id="edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
        <div class="modal-dialog" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h3 id="myModalLabel">Edit</h3>
              <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div class="modal-body">
              <form class="form-horizontal templatemo-create-account templatemo-container-modal" role="form" action="<?php echo site_url('provinsi/update');?>" method="post">
              <div class="form-inner">
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="hidden" name="id" id="id_provinsi">
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <label class="control-label" style="margin-bottom:10px;">Nama Provinsi</label>
                    <input type="text" class="form-control" name="provinsi" id="nama_provinsi" placeholder="Nama Provinsi"  required>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-md-12">
                    <input type="submit" value="Simpan" class="btn btn-primary">
                    <button class="btn btn-warning" type="button" data-dismiss="modal" aria-label="Close">
                      Tutup
                    </button>
                  </div>
                </div>
              </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <script type="text/javascript">
    // Hapus
    $(document).ready(function(){
      $('.confirm_delete').on('click', function(){
        
        var delete_url = $(this).attr('data-url');

        swal({
          title: "Hapus Provinsi",
          text: "Yakin ingin menghapus provinsi ini ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#FD0404",
          confirmButtonText: "Hapus !",
          cancelButtonText: "Batalkan",
          closeOnConfirm: false     
        }, function(){
          window.location.href = delete_url;
        });

        return false;
      });
    });

    //ajax script edit

      function edit(idprovinsi){
        $.ajax({
            url:"<?php echo site_url('provinsi/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idprovinsi},
            cache:false,
            success:function(result){
              $('#id_provinsi').val(result['id_provinsi']);
              $('#nama_provinsi').val(result['nama_provinsi']);
            }
        });
      }
  </script>