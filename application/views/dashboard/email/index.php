<div class="content-wrapper">
	<section class="content-header">
		<h1>
			Daftar Respon
			<small>AjarinCode</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="<?php echo site_url('dashboard') ?>"><i class="fa fa-dashboard"></i>&nbsp;Dashboard</a></li>
			<li class="active"><a href="<?php echo site_url('admin/email') ?>">E-Mail</a></li>
		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-md-12">
				<div class="nav-tabs-custom">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#belum" data-toggle="tab">Belum Direspon</a></li>
						<li><a href="#sudah" data-toggle="tab">Sudah Direspon</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane active" id="belum">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No.</th>
												<th class="text-center">Nama</th>
												<th class="text-center">E-Mail</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($email as $e){ ?>
											<?php if($e->status_contact=="Belum direspon"){ ?>
											<tbody>
												<tr>
													<th class="text-center"><?php echo $no++ ?></th>
													<th><?php cetak($e->nama_contact) ?></th>
													<th><?php cetak($e->email_contact) ?></th>
													<th class="text-center <?php if($e->status_contact=="Belum direspon"){echo "bg-red";}else{echo "bg-green";} ?>"><?php cetak($e->status_contact) ?></th>
													<th class="text-center">
														<a href="#" data-toggle="modal" data-target="#show" onclick="edit(<?php cetak($e->id_contact) ?>)" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('home/status_contact/'.$e->id_contact) ?>" class="btn btn-success btn-xs" <?php if($e->status_contact=="Direspon"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
													</th>
												</tr>
											</tbody>
											<?php } ?>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="sudah">
							<div class="box">
								<div class="box-body">
									<table class="table table-bordered">
										<thead>
											<tr>
												<th class="text-center">No.</th>
												<th class="text-center">Nama</th>
												<th class="text-center">E-Mail</th>
												<th class="text-center">Status</th>
												<th class="text-center">Action</th>
											</tr>
										</thead>
										<?php $no=1; foreach($email as $e){ ?>
											<?php if($e->status_contact=="Direspon"){ ?>
											<tbody>
												<tr>
													<th class="text-center"><?php echo $no++ ?></th>
													<th><?php cetak($e->nama_contact) ?></th>
													<th><?php cetak($e->email_contact) ?></th>
													<th class="text-center <?php if($e->status_contact=="Belum direspon"){echo "bg-red";}else{echo "bg-green";} ?>"><?php cetak($e->status_contact) ?></th>
													<th class="text-center">
														<a href="#" data-toggle="modal" data-target="#show" onclick="edit(<?php cetak($e->id_contact) ?>)" class="btn btn-info btn-xs"><i class="fa fa-eye"></i></a>
														<a href="<?php echo site_url('home/status_contact/'.$e->id_contact) ?>" class="btn btn-success btn-xs" <?php if($e->status_contact=="Direspon"){echo "disabled";} ?>><i class="fa fa-check"></i></a>
													</th>
												</tr>
											</tbody>
											<?php } ?>
										<?php } ?>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Modal -->
        <div class="modal fade" id="show">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Lihat Pesan</h4>
              </div>
              <div class="modal-body">
                <div class="form-group">
                	<input type="text" name="nama" class="form-control" id="nama_contact" readonly>
                </div>
                <div class="form-group">
                	<input type="text" name="email" class="form-control" id="email_contact" readonly>
                </div>
                <div class="form-group">
                	<textarea class="form-control" name="message" id="message_contact"></textarea>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /.modal -->
	</section>
</div>
<script>
	//ajax script edit

      function edit(idcontact){
        $.ajax({
            url:"<?php echo site_url('home/edit');?>",
            type:"post",
            dataType: 'json',
            data:{id:idcontact},
            cache:false,
            success:function(result){
              $('#nama_contact').val(result['nama_contact']);
              $('#email_contact').val(result['email_contact']);
              $('#message_contact').val(result['message_contact']);
            }
        });
      }
</script>