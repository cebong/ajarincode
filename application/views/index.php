<header id="fh5co-header" class="fh5co-cover" role="banner" style="background-image:url(<?php echo base_url() ?>assets/home/images/careerbuilder-arpost-2442.jpeg);" data-stellar-background-ratio="0.5">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="display-t">
						<div class="display-tc animate-box" data-animate-effect="fadeIn">
							<img src="<?php echo base_url('assets/images/logo.png') ?>" width="100px" height="100px">
							<h1>AjarinCode</h1>
							<h2>Belajar coding lebih mudah dimanapun dengan biaya murah</h2>
							<p><a class="btn btn-primary btn-lg btn-learn" href="<?php  echo site_url('login') ?>">Take A Course</a> <a class="btn btn-primary btn-lg popup-vimeo btn-video" href="https://vimeo.com/channels/staffpicks/93951774"><i class="icon-play"></i> Watch Video</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</header>

	<div id="fh5co-counter" class="fh5co-counters">
		<div class="container">
			<div class="row">
				<div class="col-md-3 text-center animate-box">
					<span class="fh5co-counter js-counter" data-from="0" data-to="<?php 
						$this->db->from('user');
						$this->db->like('hak_akses','Peserta');
						echo $this->db->count_all_results();
					?>" data-speed="5000" data-refresh-interval="50"></span>
					<span class="fh5co-counter-label">Peserta</span>
				</div>
				<div class="col-md-3 text-center animate-box">
					<span class="fh5co-counter js-counter" data-from="0" data-to="<?php 
						$this->db->from('materi');
						echo $this->db->count_all_results();
					?>" data-speed="5000" data-refresh-interval="50"></span>
					<span class="fh5co-counter-label">Materi</span>
				</div>
				<div class="col-md-3 text-center animate-box">
					<span class="fh5co-counter js-counter" data-from="0" data-to="<?php
						$this->db->from('user');
						$this->db->like('hak_akses','Mentor');
						echo $this->db->count_all_results();
					?>" data-speed="5000" data-refresh-interval="50"></span>
					<span class="fh5co-counter-label">Mentor</span>
				</div>
				<div class="col-md-3 text-center animate-box">
					<span class="fh5co-counter js-counter" data-from="0" data-to="<?php 
						$this->db->from('event');
						echo $this->db->count_all_results();
					?>" data-speed="5000" data-refresh-interval="50"></span>
					<span class="fh5co-counter-label">Event</span>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-explore" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Take A Course</h2>
					<p>Ayo Jadi Developer!</p>
				</div>
			</div>
		</div>		
		<div class="fh5co-explore fh5co-explore1">
			<div class="container">
				<div class="row">
					<div class="col-md-8 col-md-push-5 animate-box">
						<img class="img-responsive" src="<?php echo base_url() ?>assets/home/images/image.png" alt="work">
					</div>
					<div class="col-md-4 col-md-pull-8 animate-box">
						<div class="mt">
							<h3>Materi yang kami ajarkan meliputi :</h3>
							<ul class="list-nav">
								<li><i class="icon-check2"></i>HTML</li>
								<li><i class="icon-check2"></i>CSS</li>
								<li><i class="icon-check2"></i>Javascript</li>
								<li><i class="icon-check2"></i>PHP</li>
								<li><i class="icon-check2"></i>SQL</li>
							</ul>
							
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="fh5co-explore">
			<div class="container">
				<div class="row">
					<div class="col-md-6 animate-box">
						<div class="mt">
							<div>
								<h4><i class="icon-users"></i>Komunikatif</h4>
								<p>Sharing dengan mentor yang tersedia apabila mengalami kesulitan dalam belajar</p>
							</div>
							<div>
								<h4><i class="icon-credit-card"></i>Biaya Murah</h4>
								<p>Cukup Bayar 1 kali di setiap levelnya</p>
							</div>
						</div>
					</div>
					<div class="col-md-6 animate-box">
						<div class="mt">
							<div>
								<h4><i class="icon-book"></i>Materi Mudah Dipahami</h4>
								<p>Materi telah kami susun secara ringkas berdasarkan tingkat kesulitan</p>
							</div>
							<div>
								<h4><i class="icon-cloud"></i>Belajar Dimanapun</h4>
								<p>Tidak perlu khawatir, anda dapat belajar dimanapun dan kapanpun yang anda mau, waktu kami anda yang tentukan</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="fh5co-steps">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Start A Course</h2>
					<p>Tata Cara Belajar di AjarinCode.com</p>
				</div>
			</div>

			<div class="row bs-wizard animate-box" style="border-bottom:0;">
                
				<div class="col-xs-3 bs-wizard-step complete">
					<div class="text-center bs-wizard-stepnum"><h4>Step 1</h4></div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center"><p>Buat Akun</p></div>
				</div>

				<div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
					<div class="text-center bs-wizard-stepnum"><h4>Step 2</h4></div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center"><p>Akses Materi</p></div>
				</div>

				<div class="col-xs-3 bs-wizard-step complete"><!-- complete -->
					<div class="text-center bs-wizard-stepnum"><h4>Step 3</h4></div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center"><p>Upgrade Level</p></div>
				</div>

				<div class="col-xs-3 bs-wizard-step active"><!-- active -->
					<div class="text-center bs-wizard-stepnum"><h4>Step 4</h4></div>
					<div class="progress"><div class="progress-bar"></div></div>
					<a href="#" class="bs-wizard-dot"></a>
					<div class="bs-wizard-info text-center"><p>Start Making Money</p></div>
				</div>
			</div>

		</div>
	</div>

	<!-- <div id="fh5co-testimonial" class="fh5co-bg-section">
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-6 col-md-offset-3 text-center fh5co-heading">
					<h2>Testimonials</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="row animate-box">
						<div class="owl-carousel owl-carousel-fullwidth">
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="<?php echo base_url() ?>assets/home/images/person_1.jpg" alt="user">
									</figure>
									<span>Jean Doe, via <a href="#" class="twitter">Twitter</a></span>
									<blockquote>
										<p>&ldquo;Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="<?php echo base_url() ?>assets/home/images/person_2.jpg" alt="user">
									</figure>
									<span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
									<blockquote>
										<p>&ldquo;Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
									</blockquote>
								</div>
							</div>
							<div class="item">
								<div class="testimony-slide active text-center">
									<figure>
										<img src="<?php echo base_url() ?>assets/home/images/person_3.jpg" alt="user">
									</figure>
									<span>John Doe, via <a href="#" class="twitter">Twitter</a></span>
									<blockquote>
										<p>&ldquo;Far from the countries Vokalia and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove right at the coast of the Semantics, a large language ocean.&rdquo;</p>
									</blockquote>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div> -->

	


	<div id="fh5co-started" style="background-image:url(<?php echo base_url() ?>assets/home/images/img_bg_2.jpg);">
		<div class="overlay"></div>
		<div class="container">
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center fh5co-heading">
					<h2>Lets Get Started</h2>
					<p>Ayo Mulai jadi Developer dari sekarang</p>
				</div>
			</div>
			<div class="row animate-box">
				<div class="col-md-8 col-md-offset-2 text-center">
					<p><a href="<?php echo site_url('login') ?>" class="btn btn-default btn-lg">Start Course</a></p>
				</div>
			</div>
		</div>
	</div>