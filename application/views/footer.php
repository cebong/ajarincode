<footer id="fh5co-footer" role="contentinfo">
		<div class="container">
			<div class="row row-pb-md">
				<div class="col-md-3 fh5co-widget">
					<h4>About Learning</h4>
					<p>AjarinCode merupakan media pembelajaran bahasa pemrograman berbasis web</p>
					<p><i class="icon-phone"></i>&nbsp;+6285792071316</p>
					<p><i class="icon-mail"></i>&nbsp;support@ajarincode.com</p>
				</div>
			</div>

			<div class="row copyright">
				<div class="col-md-12 text-center">
					<p>
						<small class="block">&copy; 2018. All Rights Reserved.</small> 
					</p>
					<p>
						<ul class="fh5co-social-icons">
							<li><a href="https://twitter.com/AjarincodeCom" target="_blank"><i class="icon-twitter"></i></a></li>
							<li><a href="https://www.facebook.com/ajarincode/" target="_blank"><i class="icon-facebook"></i></a></li>
							<li><a href="https://www.instagram.com/ajarincode/" target="_blank"><i class="icon-instagram"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCn4QBJdz3wFZ8D1muzd3z2Q?view_as=subscriber" target="_blank"><i class="icon-youtube"></i></a></li>
						</ul>
					</p>
				</div>
			</div>

		</div>
	</footer>
	</div>

	<div class="gototop js-top">
		<a href="#" class="js-gotop"><i class="icon-arrow-up"></i></a>
	</div>
	
	<!-- jQuery -->
	<script src="<?php echo base_url() ?>assets/home/js/jquery.min.js"></script>
	<!-- jQuery Easing -->
	<script src="<?php echo base_Url() ?>assets/home/js/jquery.easing.1.3.js"></script>
	<!-- Bootstrap -->
	<script src="<?php echo base_url() ?>assets/home/js/bootstrap.min.js"></script>
	<!-- Waypoints -->
	<script src="<?php echo base_url() ?>assets/home/js/jquery.waypoints.min.js"></script>
	<!-- Stellar Parallax -->
	<script src="<?php echo base_url() ?>assets/home/js/jquery.stellar.min.js"></script>
	<!-- Carousel -->
	<script src="<?php echo base_url() ?>assets/home/js/owl.carousel.min.js"></script>
	<!-- countTo -->
	<script src="<?php echo base_url() ?>assets/home/js/jquery.countTo.js"></script>
	<!-- Magnific Popup -->
	<script src="<?php echo base_url() ?>assets/home/js/jquery.magnific-popup.min.js"></script>
	<script src="<?php echo base_url() ?>assets/home/js/magnific-popup-options.js"></script>
	<!-- Main -->
	<script src="<?php echo base_url() ?>assets/home/js/main.js"></script>

	<!--Kontrol Menu-->
	<script type="text/javascript">
	  $(function() {
	    $('#nav a[href~="' + location.href + '"]').parents('li').addClass('active');
	  });
	</script>

	</body>
</html>