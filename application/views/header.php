<!DOCTYPE HTML>
<html>
    <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>AjarinCode</title>
    <link rel="icon" type="image/png" href="<?php echo base_url() ?>assets/images/icon.ico">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Free HTML5 Website Template by freehtml5.co" />
    <meta name="keywords" content="free website templates, free html5, free template, free bootstrap, free website template, html5, css3, mobile first, responsive" />
    <meta name="author" content="AjarinCode" />

    <!-- 
    //////////////////////////////////////////////////////

    FREE HTML5 TEMPLATE 
    DESIGNED & DEVELOPED by FreeHTML5.co
        
    Website:        http://freehtml5.co/
    Email:          info@freehtml5.co
    Twitter:        http://twitter.com/fh5co
    Facebook:       https://www.facebook.com/fh5co

    //////////////////////////////////////////////////////
     -->

    <!-- Facebook and Twitter integration -->
    <meta property="og:title" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:url" content=""/>
    <meta property="og:site_name" content=""/>
    <meta property="og:description" content=""/>
    <meta name="twitter:title" content="" />
    <meta name="twitter:image" content="" />
    <meta name="twitter:url" content="" />
    <meta name="twitter:card" content="" />

    <link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,700,800" rel="stylesheet">
    
    <!-- Animate.css -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/animate.css">
    <!-- Icomoon Icon Fonts-->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/icomoon.css">
    <!-- Bootstrap  -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/bootstrap.css">

    <!-- Magnific Popup -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/magnific-popup.css">

    <!-- Owl Carousel  -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/owl.theme.default.min.css">

    <!-- Theme style  -->
    <link rel="stylesheet" href="<?php echo base_url() ?>assets/home/css/style.css">

    <!-- Modernizr JS -->
    <script src="<?php echo base_url() ?>assets/home/js/modernizr-2.6.2.min.js"></script>
    <!-- FOR IE9 below -->
    <!--[if lt IE 9]>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <?php
    //proses
    $filecounter=("counter.txt");
    $kunjungan=file($filecounter);
    $kunjungan[0]++;
    $file=fopen($filecounter,"w");
    fputs($file,"$kunjungan[0]");
    fclose($file);
    ?>

    </head>
    <body>
        
    <div class="fh5co-loader"></div>
    
    <div id="page">
    <nav class="fh5co-nav" role="navigation">
        <div class="top">
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 text-right">
                        <p class="num">Call: +6285792071316</p>
                        <ul class="fh5co-social">
                            <li><a href="https://twitter.com/AjarincodeCom" target="_blank"><i class="icon-twitter"></i></a></li>
                            <li><a href="https://www.facebook.com/ajarincode/" target="_blank"><i class="icon-facebook"></i></a></li>
                            <li><a href="https://www.instagram.com/ajarincode/" target="_blank"><i class="icon-instagram"></i></a></li>
                            <li><a href="https://www.youtube.com/channel/UCn4QBJdz3wFZ8D1muzd3z2Q?view_as=subscriber" target="_blank"><i class="icon-youtube"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="top-menu">
            <div class="container">
                <div class="row">
                    <div class="col-xs-1">
                        <div id="fh5co-logo"><a href="<?php echo site_url('home') ?>">AjarinCode<span>.</span></a></div>
                    </div>
                    <div class="col-xs-11 text-right menu-1">
                        <ul id="nav">
                            <li><a href="<?php echo site_url('home') ?>">Home</a></li>
                            <li><a href="<?php echo site_url('about') ?>">About</a></li>
                            <li><a href="<?php echo site_url('pricing') ?>">Pricing</a></li>
                            <li><a href="<?php echo site_url('contact') ?>">Contact</a></li>
                            <li class="btn-cta"><a href="<?php echo site_url('login') ?>"><span>Login</span></a></li>
                            <li class="btn-cta"><a href="<?php echo site_url('register') ?>"><span>Register</span></a></li>
                        </ul>
                    </div>
                </div>
                
            </div>
        </div>
    </nav>