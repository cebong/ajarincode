<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Event extends CI_Controller {
	
		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function tambah(){
			$jenis = $this->input->post('jenis');
			$event = $this->input->post('event');
			$mulai = $this->input->post('mulai');
			$akhir = $this->input->post('akhir');
			$deskripsi = $this->input->post('deskripsi');
			$harga = $this->input->post('harga');
			$status = $this->input->post('status');
			$level = $this->input->post('level');
			$partisipan = implode(", ", $level);
			$gambar = $_FILES['path']['name'];
			$config['upload_path'] = './assets/event/image';
			$config['allowed_types'] = 'jpg|png';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $event;
			$config['file_ext_tolower'] = TRUE;
			$this->load->library('upload',$config);
			$this->upload->do_upload('path');

			$data = array(
				'id_jenis' => $jenis,
				'id_level' => $partisipan,
				'nama_event' => $event,
				'tgl_mulai' => $mulai,
				'tgl_akhir' => $akhir,
				'deskripsi_event' => $deskripsi,
				'harga_event' => $harga,
				'path_event' => $this->upload->data('file_name'),
				'status_event' => $status,
			);

			$this->m_event->create($data,'event');
			redirect('admin/kelola_event');
		}

		function edit($id){
			$where = array('id_event' => $id);
			$data = array(
				'event' => $this->m_event->get($where,'event')->result(),
				'jenis' => $this->m_jenis->list_jenis()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/event/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$jenis = $this->input->post('jenis');
			$event = $this->input->post('event');
			$mulai = $this->input->post('mulai');
			$akhir = $this->input->post('akhir');
			$deskripsi = $this->input->post('deskripsi');
			$harga = $this->input->post('harga');
			$status = $this->input->post('status');
			$gambar = $_FILES['path']['name'];
			$config['upload_path'] = './assets/event/image';
			$config['allowed_types'] = 'jpg|png';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $event;
			$config['file_ext_tolower'] = TRUE;
			$this->load->library('upload');
			$this->upload->do_upload('path');

			$where = array('id_event' => $id);
			$data = array(
				'nama_event' => $event,
				'id_jenis' => $jenis,
				'tgl_mulai' => $mulai,
				'tgl_akhir' => $akhir,
				'deskripsi_event' => $deskripsi,
				'harga_event' => $harga,
				'path_event' => $this->upload->data('file_name'),
				'status_event' => $status,
			);

			$this->m_event->replace($where,$data,'event');
			redirect('admin/kelola_event');
		}

		function hapus($id){
			$where = array('id_event' => $id);
			$this->m_event->trash($where,'event');
			redirect('admin/kelola_event');
		}

		function partisipasi(){
			$event = $this->input->post('id');
			$user = $this->session->userdata('id');

			$data = array(
				'id_user' => $user,
				'id_event' => $event,
				'status_participant' => "Belum Lunas",
			);
			$id_participant = $this->m_event->daftar_event($data,'participant');
			$nominal = $this->input->post('harga');
			$order_id = $this->input->post('order_id');
			$sub = substr($nominal,-3);
			$sub2 = substr($nominal,-2);
			$sub3 = substr($nominal,-1);
 
			$total =  random_string('numeric', 3);
			$total2 =  random_string('numeric', 2);
			$total3 =  random_string('numeric', 1);
 
				if($sub==0){
					$hasil =  $nominal + $total; 
					$pembayaran = array(
						'id_bayar_event' => $order_id,
						'id_participant' => $id_participant,
						'kode_unik_event' => $total,
						'total_bayar_event' => $hasil,
					);
					$this->m_event->pembayaran($pembayaran,'pembayaran_event');
					redirect('invoice/'.$order_id);
				} else if($sub2 == 0){
					$hasil = $nominal + $total2; 
					$no = substr($hasil,-3);
					$pembayaran = array(
						'id_bayar_event' => $this->m_invoice->get_no_invoice(),
						'id_participant' => $id_participant,
						'kode_unik_event' => $no,
						'total_bayar_event' => $hasil,
					);
					$this->m_event->pembayaran($pembayaran,'pembayaran_event');
					redirect('invoice/'.$order_id);
				} else if($sub3 == 0){
					$hasil = $nominal + $total3; 
					$no = substr($hasil,-3);
					$pembayaran = array(
						'id_bayar_event' => $this->m_invoice->get_no_invoice(),
						'id_participant' => $id_participant,
						'kode_unik_event' => $no,
						'total_bayar_event' => $hasil,
					);
					$this->m_event->pembayaran($pembayaran,'pembayaran_event');
					redirect('invoice/'.$order_id);
				}else{
					$pembayaran = array(
						'id_bayar_event' => $this->m_invoice->get_no_invoice(),
						'id_participant' => $id_participant,
						'kode_unik_event' => $sub,
						'total_bayar_event' => $nominal,
					);
					$this->m_event->pembayaran($pembayaran,'pembayaran_event');
					redirect('invoice/'.$order_id);
				}
		}

		function view_pembayaran($id){
			$where = array('id_bayar_event' => $id);
			$data['pembayaran'] = $this->m_event->look_payment($where,'pembayaran_event')->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/pembayaran/event/view',$data);
			$this->load->view('dashboard/footer');
		}
	
	}
	
	/* End of file Event.php */
	/* Location: ./application/controllers/Event.php */
?>