<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Ask extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata('logged_in') != TRUE){
				redirect('login');
			}
		}
	
		public function index()
		{
			$data['kodeunik'] = $this->m_ask->code_otomatis();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/ask_code/tanya',$data);
			$this->load->view('dashboard/footer');
		}

		function tambah(){
			$id = $this->input->post('id');
			$tanya = $this->input->post('tanya');
			$code = $this->input->post('code');
			$kupon = $this->input->post('kupon');

			$data = array(
				'id_ask' => $kupon,
				'id_user' => $id,
				'asking' => $tanya,
				'code' => $code,
			);

			// var_dump($data);
			$this->m_ask->create($data,'ask_code');
			redirect('dashboard/ask_code');
		}

		function edit($id){
			$where = array('id_ask' => $id);
			$data = array('ask' => $this->m_ask->get($where,'ask_code')->result());

			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/ask_code/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function tanya_kembali(){
			$id = $this->input->post('id');
			$tanya = $this->input->post('tanya');
			$code = $this->input->post('code');

			$where = array('id_ask' => $id);
			$data = array(
				'asking' => $tanya,
				'code' => $code,
			);

			// var_dump($where);
			// var_dump($data);
			$this->m_ask->replace_tanya($where,$data,'ask_code');
			redirect('dashboard/ask_code');
		}

		function ceklist($id){
			$where = array('id_ask' => $id);
			$data = array('status_ask' => "Terjawab");

			$this->m_ask->update_status($where,$data,'ask_code');
			redirect('dashboard/ask_code');
		}

		function jawab($id){
			$where = array('id_ask' => $id);
			$data = array(
				'ask' => $this->m_ask->answer($where,'ask_code')->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/ask_code/kelola_ask/jawab',$data);
			$this->load->view('dashboard/footer');
		}

		function kirim_jawaban(){
			$id = $this->input->post('id');
			$jawaban = $this->input->post('jawaban');
			$respon = $this->input->post('responder');

			$where = array('id_ask' => $id);
			$data = array(
				'respon' => $jawaban,
				'id_respon' => $respon,
			);

			$this->m_ask->send_respon($where,$data,'ask_code');
			redirect('admin/show_ask');

		}
	
	}
	
	/* End of file Ask.php */
	/* Location: ./application/controllers/Ask.php */
?>