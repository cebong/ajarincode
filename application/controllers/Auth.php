<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Auth extends CI_Controller {
    
        function login(){
            $username = $this->input->post('username');
            $password = password_hash($this->input->post('password'), PASSWORD_DEFAULT);

            $where = array(
                'username' => $username,
                'password' => password_verify($password),
            );

            $cek = $this->m_auth->cek_login($where,'user');
            if($cek->num_rows() > 0){
                $row = $cek->row();
                $session = array(
                    'logged_in' => TRUE,
                    'id' => $row->id_user,
                    'nama_user' => $row->nama_user,
                    'jk' => $row->jk,
                    'tgl_lahir' => $row->tgl_lahir,
                    'no_user' => $row->no_user,
                    'asal' => $row->nama_kabupaten,
                    'username' => $username,
                    'email' => $row->email,
                    'path' => $row->path,
                    'bio' => $row->bio,
                    'id_level' => $row->id_level,
                    'level' => $row->nama_level,
                    'status' => $row->status_user,
                    'gabung' => $row->tgl_join,
                    'hak_akses' => $row->hak_akses,
                    'confirm' => $row->confirm,
                );
                $this->session->set_userdata($session);

                if($this->session->userdata('confirm') != 1){
                    $eror = array(
                        'status' => "failed",
                        'message' => "Silahkan lakukan aktivasi akun melalui email yang didaftarkan",
                    );
                    $this->session->set_flashdata($eror);
                    redirect('login');
                }
                else{
                    redirect('dashboard');
                }
            }
            else{
                $eror = array(
                    'status' => "failed",
                    'message' => "Maaf username dan password yang anda masukkan tidak sesuai silahkan ulangi kembali",
                );
                $this->session->set_flashdata($eror);
                redirect('login');
            }
        }

        function logout(){
            $this->session->sess_destroy();
            redirect('login');
        }

        function register(){
            $nama = $this->input->post('nama');
            $jk = $this->input->post('jk');
            $tgl = $this->input->post('tanggal');
            $nomer = $this->input->post('nomer');
            $asal = $this->input->post('kabupaten');
            $email = $this->input->post('email');
            $username = $this->input->post('username');
            $password = $this->input->post('password');
            $gabung = $this->input->post('gabung');
            //$captcha = $this->input->post('captcha');
            $recaptcha = $this->input->post('g-recaptcha-response');
            $response = $this->recaptcha->verifyResponse($recaptcha);

            $cek_data = $this->m_auth->cek_data($email,$username,'user');

            if($response == true){
                if($cek_data->num_rows() > 0){
                  $row = $cek_data->row();
                  if($email == $row->email){
                    $eror = array(
                        'status' => "failed",
                        'message' => "Maaf email sudah terdaftar, silahkan gunakan email yang berbeda",
                    );
                    $this->session->set_flashdata($eror);
                    redirect('register');
                  }
                  elseif($username == $row->username){
                    $eror = array(
                        'status' => "failed",
                        'message' => "Maaf Username sudah terdaftar silahkan gunakan username yang berbeda",
                    );
                    $this->session->set_flashdata($eror);
                    redirect('register');
                  }
                  else{
                    if($jk == "L"){
                        $data = array(
                            'nama_user' => $nama,
                            'jk' => $jk,
                            'tgl_lahir' => $tgl,
                            'no_user' => $nomer,
                            'id_kabupaten' => $asal,
                            'username' => $username,
                            'password' => password_hash($password, PASSWORD_DEFAULT),
                            'email' => $email,
                            'path' => "default_men.png",
                            'bio' => "Hello saya sudah bergabung di AjarinCode",
                            'id_level' => 1,
                            'status_user' => "Aktif",
                            'hak_akses' => "Peserta",
                            'tgl_join' => $gabung,
                            'confirm' => 0,
                        );

                        $id = $this->m_auth->daftar($data,'user');
                        redirect('login');
                        $encrypted_id = md5($id);
                        require_once(APPPATH.'libraries/PHPMailer/PHPMailerAutoload.php');
                        $mail = new PHPMailer;
         
                        $mail->isSMTP();
                        $mail->Host = 'mail.ajarincode.com'; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
                        $mail->SMTPAuth = false;
                        $mail->SMPTSecure = false;
                        $mail->Username = 'support@ajarincode.com'; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
                        $mail->Password = 'MasanemasusasE'; //masukan kata sandi
                        $mail->Port = 587; //port tidak usah di ubah, biarkan 587
                         
                        $mail->setFrom('support@ajarincode.com', 'AjarinCode'); //email pengirim
                        $mail->addAddress($email, 'penerima'); //email penerima
                        $mail->isHTML(true);
                         
                                ///atur pesan email disini
                        $mail->Subject = 'Verifikasi Akun';
                        $mail->AddEmbeddedImage('https://drive.google.com/open?id=12TLXJk86b-H7GPjXFUi2LC3P8HuSNHdH','header','email_header.png');
                        $mail->Body    = '<header><img src="cid:header"></header><br>
                        Terimakasih sudah melakukan pendaftaran, selanjutnya lakukan konfirmasi melalui link berikut ini '.site_url("auth/verification/$encrypted_id");
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                         
                        if(!$mail->send()) {
                          echo 'Message could not be sent.';
                          echo 'Mailer Error: ' . $mail->ErrorInfo;
                        } else {
                            $success = array(
                                'status' => "berhasil",
                                'message' => "Selamat Pendaftaran telah berhasil, silahkan cek email untuk proses aktivasi akun",
                            );
                            $this->session->set_flashdata($success);
                            redirect('login');
                        }
                    }
                    else{
                        $data = array(
                            'nama_user' => $nama,
                            'jk' => $jk,
                            'tgl_lahir' => $tgl,
                            'no_user' => $nomer,
                            'id_kabupaten' => $asal,
                            'username' => $username,
                            'password' => password_hash($password, PASSWORD_DEFAULT),
                            'email' => $email,
                            'path' => "default_women.png",
                            'bio' => "Hello saya sudah bergabung di AjarinCode",
                            'id_level' => 1,
                            'status_user' => "Aktif",
                            'hak_akses' => "Peserta",
                            'tgl_join' => $gabung,
                            'confirm' => 0,
                        );

                        $id = $this->m_auth->daftar($data,'user');
                        redirect('login');
                        $encrypted_id = md5($id);
                        require_once(APPPATH.'libraries/PHPMailer/PHPMailerAutoload.php');
                        $mail = new PHPMailer;
         
                        $mail->isSMTP();
                        $mail->Host = 'mail.ajarincode.com'; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
                        $mail->SMTPAuth = false;
                        $mail->SMPTSecure = false;
                        $mail->Username = 'support@ajarincode.com'; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
                        $mail->Password = 'MasanemasusasE'; //masukan kata sandi
                        $mail->Port = 587; //port tidak usah di ubah, biarkan 587
                         
                        $mail->setFrom('support@ajarincode.com', 'AjarinCode'); //email pengirim
                        $mail->addAddress($email, 'penerima'); //email penerima
                        $mail->isHTML(true);
                         
                                ///atur pesan email disini
                        $mail->Subject = 'Verifikasi Akun';
                        $mail->AddEmbeddedImage('https://drive.google.com/open?id=12TLXJk86b-H7GPjXFUi2LC3P8HuSNHdH','header','email_header.png');
                        $mail->Body    = '<header><img src="cid:header"></header><br>
                        Terimakasih sudah melakukan pendaftaran, selanjutnya lakukan konfirmasi melalui link berikut ini '.site_url("auth/verification/$encrypted_id");
                        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                         
                        if(!$mail->send()) {
                          echo 'Message could not be sent.';
                          echo 'Mailer Error: ' . $mail->ErrorInfo;
                        } else {
                            $success = array(
                                'status' => "berhasil",
                                'message' => "Selamat Pendaftaran telah berhasil, silahkan cek email untuk proses aktivasi akun",
                            );
                            $this->session->set_flashdata($success);
                            redirect('login');
                        }
                    }
                  } 
                }
            }
            else{
                $eror = array(
                    'status' => "failed",
                    'message' => "Maaf Captcha yang anda masukkan tidak sesuai, silahkan ulangi kembali"
                );
                $this->session->set_flashdata($eror);
                redirect('register');
            }
        }

        function verification($key){
            $where = array('md5(id_user)' => $key);
            $data = array('confirm' => 1);
            $this->m_auth->changeActive($where,$data,'user');
            $success = array(
                'status' => "change",
                'message' => "Selamat akun anda sudah aktif, silahkan login",
            );
            $this->session->set_flashdata($success);
            redirect('login');
        }

        function request_new_password(){
            $email = $this->input->post('email');

            $where = array('email' => $email);

            $cek = $this->m_auth->cek_email($where,'user');
            if($cek->num_rows() > 0){
                $row = $cek->row();
                $data = $row->id_user;

                $encrypted_id = md5($data);
                require_once(APPPATH.'libraries/PHPMailer/PHPMailerAutoload.php');
                $mail = new PHPMailer;
 
                $mail->isSMTP();
                $mail->Host = 'mail.ajarincode.com'; //nama "domain" ganti sesuai nama domain anda. misal domain anda satuan.com maka bentuk host mailnya adalah mail.satuan.com
                $mail->SMTPAuth = false;
                $mail->SMPTSecure = false;
                $mail->Username = 'support@ajarincode.com'; //email dari domain anda, untuk cara pembuatan email akan di bahas di bawah
                $mail->Password = 'MasanemasusasE'; //masukan kata sandi
                $mail->Port = 587; //port tidak usah di ubah, biarkan 587
                 
                $mail->setFrom('support@ajarincode.com', 'AjarinCode'); //email pengirim
                $mail->addAddress($email, 'penerima'); //email penerima
                $mail->isHTML(true);
                 
                        ///atur pesan email disini
                $mail->Subject = 'Lupa Password';
                $mail->Body    = 'Silahkan buat password baru melalui link berikut<br> '.site_url("home/change_password/$encrypted_id");
                $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';
                 
                if(!$mail->send()) {
                  echo 'Message could not be sent.';
                  echo 'Mailer Error: ' . $mail->ErrorInfo;
                } else {
                    $success = array(
                        'status' => "berhasil",
                        'message' => "Permintaan lupa Password berhasil, silahkan cek email",
                    );
                    $this->session->set_flashdata($success);
                    redirect('login');
                }
            }
            else{
                $eror = array(
                    'status' => "not found",
                    'message' => "Maaf email yang anda maksud tidak terdaftar",
                );
                $this->session->set_flashdata($eror);
                redirect('login');
            }
        }

        function update_password(){
            $id = $this->input->post('id');
            $password = $this->input->post('password');
            $where = array('id_user' => $id);
            $data = array('password' => password_hash($password,PASSWORD_DEFAULT));
            $this->m_auth->changePassword($where,$data,'user');
            $success = array(
                'status' => "change",
                'message' => "Password sudah berhasil diganti",
            );
            $this->session->set_flashdata($success);
            redirect('login');
        }
    
    }
    
    /* End of file Auth.php */
    
?>