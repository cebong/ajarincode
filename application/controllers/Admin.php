<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Admin extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
    
        function provinsi(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_provinsi->jumlah_provinsi();
            $config['base_url'] = base_url('admin/provinsi/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data['provinsi'] = $this->m_provinsi->data($config['per_page'],$from)->result();
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/provinsi/index', $data);
            $this->load->view('dashboard/footer');
        }

        function kabupaten(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_kabupaten->jumlah_kabupaten();
            $config['base_url'] = base_url('admin/kabupaten/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'kabupaten' => $this->m_kabupaten->data($config['per_page'],$from)->result(),
                'provinsi' => $this->m_provinsi->list_provinsi()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/kabupaten/index', $data);
            $this->load->view('dashboard/footer');
        }

        function level(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_level->jumlah_level();
            $config['base_url'] = base_url('admin/level/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'level' => $this->m_level->data($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/level/index',$data);
            $this->load->view('dashboard/footer');
        }

        function kategori(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_kategori->jumlah_kategori();
            $config['base_url'] = base_url('admin/kategori/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'kategori' => $this->m_kategori->data($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/kategori/index',$data);
            $this->load->view('dashboard/footer');
        }

        function jenis_event(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_jenis->jumlah_jenis();
            $config['base_url'] = base_url('admin/jenis_event/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'jenis' => $this->m_jenis->data($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/event/master/jenis/index',$data);
            $this->load->view('dashboard/footer');
        }

        function kelola_event(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_event->jumlah_event();
            $config['base_url'] = base_url('admin/kelola_event/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'event' => $this->m_event->data($config['per_page'],$from)->result(),
                'jenis' => $this->m_jenis->list_jenis()->result(),
                'level' => $this->m_level->list_level()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/event/index',$data);
            $this->load->view('dashboard/footer');
        }

        function rekening(){
            $data['rekening'] = $this->m_rekening->list_rekening()->result();
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/pembayaran/rekening/index',$data);
            $this->load->view('dashboard/footer');
        }

        function partisipasi(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_partisipasi->jumlah_partisipasi();
            $config['base_url'] = base_url('admin/partisipasi/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'partisipasi' => $this->m_partisipasi->data($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/event/partisipasi/index',$data);
            $this->load->view('dashboard/footer'); 
        }

        function materi(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_materi->jumlah_materi();
            $config['base_url'] = base_url('admin/materi/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'materi' => $this->m_materi->data($config['per_page'],$from)->result(),
                'level' => $this->m_level->list_level()->result(),
                'kategori' => $this->m_kategori->list_kategori()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/materi/kelola_materi/index',$data);
            $this->load->view('dashboard/footer');
        }

        function soal(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_soal->jumlah_soal();
            $config['base_url'] = base_url('admin/soal/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'soal' => $this->m_soal->data($config['per_page'],$from)->result(),
                'level' => $this->m_level->list_level()->result(),
                'kategori' => $this->m_kategori->list_kategori()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/quis/soal/index',$data);
            $this->load->view('dashboard/footer');
        }

        function peserta(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_user->jumlah_user();
            $config['base_url'] = base_url('admin/user/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'user' => $this->m_user->data_peserta($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/user/peserta/index', $data);
            $this->load->view('dashboard/footer');
        }

        function mentor(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_user->jumlah_mentor();
            $config['base_url'] = base_url('admin/mentor/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'user' => $this->m_user->data_mentor($config['per_page'],$from)->result(),
                'provinsi' => $this->m_provinsi->list_provinsi()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/user/mentor/index', $data);
            $this->load->view('dashboard/footer');
        }

        function laporan(){
            $data = array(
                'level' => $this->m_level->list_level()->result(),
                'kabupaten' => $this->m_kabupaten->list_kabupaten()->result(),
                'jenis' => $this->m_jenis->list_jenis()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/laporan',$data);
            $this->load->view('dashboard/footer');
        }

        function rekap_event(){
        	$this->load->library('pagination');
        	$jumlah_data = $this->m_event->jumlah_bayar_event();
        	$config['base_url'] = base_url('admin/rekap_event/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';

            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array('pembayaran_event' => $this->m_event->list_pembayaran_event($config['per_page'],$from)->result());
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/pembayaran/event/index',$data);
            $this->load->view('dashboard/footer');
        }

        function rekap_level(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_level->jumlah_bayar_level();
            $config['base_url'] = base_url('admin/rekap_level/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';

            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array('pembayaran_level' => $this->m_level->list_pembayaran_level($config['per_page'],$from)->result());
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/pembayaran/level/index',$data);
            $this->load->view('dashboard/footer');
        }

        function show_ask(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_ask->jumlah_ask();
            $config['base_url'] = base_url('admin/show_ask/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'ask' => $this->m_ask->list_ask($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/ask_code/kelola_ask/index', $data);
            $this->load->view('dashboard/footer');
        }

         function email(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_email->jumlah_email();
            $config['base_url'] = base_url('admin/email/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'email' => $this->m_email->list_email($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/email/index', $data);
            $this->load->view('dashboard/footer');
        }

        function report(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_report->jumlah_bug();
            $config['base_url'] = base_url('admin/report/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'report' => $this->m_report->data_bug($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/report/kelola_report/index', $data);
            $this->load->view('dashboard/footer');
        }

        function rekap_tes(){
            $this->load->library('pagination');
            $jumlah_data = $this->m_hasil->jumlah_hasil();
            $config['base_url'] = base_url('admin/rekap_tes/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data = array(
                'rekap' => $this->m_hasil->data_hasil($config['per_page'],$from)->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/quis/rekap/index', $data);
            $this->load->view('dashboard/footer');
        }
    
    }
    
    /* End of file Admin.php */
    
?>