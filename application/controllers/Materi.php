<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Materi extends CI_Controller {
	
		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		public function create()
		{
			$data = array(
				'kategori' => $this->m_kategori->list_kategori()->result(),
				'level' => $this->m_level->list_level()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/materi/kelola_materi/create',$data);
			$this->load->view('dashboard/footer');
		}



		function tambah(){
			$judul = $this->input->post('judul');
			$level = $this->input->post('level');
			$kategori = $this->input->post('kategori');
			$deskripsi = $this->input->post('deskripsi');
			$url = $this->input->post('url');
			$isi = $this->input->post('isi');

			$data = array(
				'id_level' => $level,
				'id_kategori' => $kategori,
				'judul_materi' => $judul,
				'deskripsi_materi' => $deskripsi,
				'isi_materi' => $isi,
				'link_video' => $url,
				'slug_materi' => slug($judul),
			) ;

			$this->m_materi->create($data,'materi');
			redirect('admin/materi');
		}

		function edit($id){
			$where = array('id_materi' => $id);
			$data = array(
				'materi' => $this->m_materi->get($where,'materi')->result(),
				'level' => $this->m_level->list_level()->result(),
				'kategori' => $this->m_kategori->list_kategori()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/materi/kelola_materi/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$judul = $this->input->post('judul');
			$level = $this->input->post('level');
			$kategori = $this->input->post('kategori');
			$deskripsi = $this->input->post('deskripsi');
			$video = $this->input->post('video');
			$isi = $this->input->post('isi');
			$id = $this->input->post('id');

			$where = array('id_materi' => $id);
			$data = array(
				'judul_materi' => $judul,
				'id_level' => $level,
				'id_kategori' => $kategori,
				'deskripsi_materi' => $deskripsi,
				'isi_materi' => $isi,
				'link_video' => $video,
				'slug_materi' => slug($judul),
			);

			$this->m_materi->replace($where,$data,'materi');
			redirect('admin/materi');
		}

		function hapus($id){
			$where = array('id_materi' => $id);
			$this->m_materi->trash($where,'materi');
			redirect('admin/materi');
		}

		function tampil_materi($id){
			$where = array('materi.id_level' => $id);
			$data = array(
				'materi' => $this->m_materi->show_materi_level($where,'materi')->result(),
				'kategori' => $this->m_kategori->list_kategori()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/materi/list_materi',$data);
			$this->load->view('dashboard/footer');
		}

		function baca_materi($id){
			$where = array('id_materi' => $id);
			$data = array('materi' => $this->m_materi->read($where,'materi')->result());
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/materi/baca_materi',$data);
			$this->load->view('dashboard/footer');
		}
	
	}
	
	/* End of file Materi.php */
	/* Location: ./application/controllers/Materi.php */
?>