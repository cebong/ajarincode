<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Soal extends CI_Controller {

		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function tambah(){
			$level = $this->input->post('level');
			$soal = $this->input->post('soal');
			$opsiA = $this->input->post('opsiA');
			$opsiB = $this->input->post('opsiB');
			$opsiC = $this->input->post('opsiC');
			$opsiD = $this->input->post('opsiD');
			$kunci = $this->input->post('kunci');
			$kategori = $this->input->post('kategori');

			$pertanyaan = array(
				'id_level' => $level,
				'pertanyaan' => $soal,
				'kunci' => $kunci,
				'id_kategori' => $kategori,
			);
			$id_soal = $this->m_soal->create_soal($pertanyaan,'soal');
			$jawaban = array(
				'opsiA' => $opsiA,
				'opsiB' => $opsiB,
				'opsiC' => $opsiC,
				'opsiD' => $opsiD,
				'id_soal' => $id_soal,
			);
			$this->m_soal->create_jawaban($jawaban,'opsi');
			redirect('admin/soal');
		}

		function edit($id){
			$where = array('id_soal' => $id);
			$data = array(
				'level' => $this->m_level->list_level()->result(),
				'kategori' => $this->m_kategori->list_kategori()->result(),
				'soal' => $this->m_soal->get_soal($where,'soal')->result(),
				'jawaban' => $this->m_soal->get_jawaban($where,'opsi')->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/quis/soal/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$level = $this->input->post('level');
			$soal = $this->input->post('soal');
			$kunci = $this->input->post('kunci');
			$opsiA = $this->input->post('opsiA');
			$opsiB = $this->input->post('opsiB');
			$opsiC = $this->input->post('opsiC');
			$opsiD = $this->input->post('opsiD');
			$kategori = $this->input->post('kategori');

			$where = array('id_soal' => $id);
			$pertanyaan = array(
				'id_level' => $level,
				'pertanyaan' => $soal,
				'kunci' => $kunci,
				'id_kategori' => $kategori,
			);
			$id_soal = $this->m_soal->replace_soal($where,$pertanyaan,'soal');
			$jawaban = array(
				'opsiA' => $opsiA,
				'opsiB' => $opsiB,
				'opsiC' => $opsiC,
				'opsiD' => $opsiD,
			);
			$this->m_soal->replace_jawaban($where,$jawaban,'opsi');
			redirect('admin/soal');
		}

		function hapus($id){
			$where = array('id_soal' => $id);
			$this->m_soal->trash_soal($where,'soal');
			$this->m_soal->trash_jawaban($where,'opsi');
			redirect('admin/soal');
		}

		function tes($id){
			$where = array('id_kategori' => $id);
			
	        $data['soal'] = $this->m_soal->tampil_soal($where,'soal')->result();

	        $i = count($data['soal']);
	        
	        // print_r($i);
	       
		    while(--$i){
		       	do{
		       		$j = mt_rand(0,$i);
		       		// print_r('<br>$i : '.$i);
		       		// print_r('$j : '.$j);
			       	if($i != $j){
			            $tmp = $data['soal'][$j];
			            $data['soal'][$j] = $data['soal'][$i];
			            $data['soal'][$i] = $tmp;
			        }
		       	}while($i==$j);
		    }

			$tampil['soal']=array();
		    for ($t=0; $t <8 ; $t++) { 
		    	$tampil['soal'][$t] = $data['soal'][$t]; //print_r($tampil['soal'][$j]);
		    }



	        $this->load->view('dashboard/sidebar');
	        $this->load->view('dashboard/tes',$tampil);
	        $this->load->view('dashboard/footer');
	        return $tampil;
        }

        function kirim_jawaban(){
        	$user = $this->session->userdata('id');
        	$kategori = $this->input->post('kategori');
        	$jawaban = $this->input->post('jawaban[]');
        	$id_soal = $this->input->post('id_soal[]');
        	$id_level = $this->input->post('level');


        	$jumlah_benar = 0;
        	
        	for($i=0;$i<5;$i++){
        		$id = $id_soal[$i];
        		
	        	$where = array('soal.id_soal' => $id);
	        	$data['soal'] = $this->m_soal->tampil_soal($where,'soal')->result();
	        	foreach($data['soal'] as $k){
	        	
        		if($k->kunci == $jawaban[$id]){
	        			++$jumlah_benar;
        			}
        		}
        		
        	}
        	$hasil = $jumlah_benar * 20;

        	// $data = array(
        	// 	'id_user' => $user,
        	// 	'id_kategori' => $kategori,
        	// 	'id_level' => $id_level,
        	// 	'nilai' => $hasil,
        	// );

        	$cek = $this->m_hasil->cek_user($user,$id_level,'hasil_tes');

        	if($cek->num_rows() > 0){
				$where = array('id_user' => $user);
				$data = array(
					'nilai' => $hasil,
				);

				$this->m_soal->update_nilai($where,$data,'hasil_tes');
				redirect('dashboard/tes');        		
        	}
        	else{
        		$data = array(
	        		'id_user' => $user,
	        		'id_kategori' => $kategori,
	        		'id_level' => $id_level,
	        		'nilai' => $hasil,
	        	);

	        	$this->m_soal->simpan_nilai_tes($data,'hasil_tes');
	        	redirect('dashboard/tes');
        	}
        	
        	// $this->m_soal->simpan_nilai_tes($data,'hasil_tes');
        	// redirect('dashboard/tes');

        }
	}
	
	/* End of file Soal.php */
	/* Location: ./application/controllers/Soal.php */
?>