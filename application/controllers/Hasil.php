<?php  
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Hasil extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			$this->load->model('m_tagihan');
		}

		// function CheckKey($query)
		// {
		//    $ketemu=mysql_query($query);
		//    if(mysql_num_rows($ketemu)>0) 
		//    {
		//      return true;
		//    }
		//    else
		//    {
		//      return false;
		//    }
		// }
	
		function upgrade_level($id){
			
			$nilai_min = 70;
			$cek_nilai = $this->m_hasil->cek_nilai()->num_rows();
			// var_dump($cek_nilai);
			if($cek_nilai > 0){
				$eror = array(
					'status' => "gagal",
					'message' => "Maaf nilai anda masih ada yang belum memenuhi syarat",
				);
				$this->session->set_flashdata($eror);
				redirect('tes');
			}
			else{
				
				$id = $this->session->userdata('id');
				$cek_lunas = $this->m_tagihan->cek_lunas($id,'pembayaran_level')->result_array();
				
				// var_dump($cek_lunas);
				if($cek_lunas['0']['status_pembayaran'] == "Belum Lunas"){
					$eror = array(
						'status' => 'gagal',
						'message' => "Maaf anda tidak bisa melakukan upgrade level, selesaikan pembayaran sebelumnya",
					);
					$this->session->set_flashdata($eror);
					redirect('tes');
				}
				else{
					$level = $this->session->userdata('id_level')+1;
				
					$where = array('id_user' => $id);
					$data = array('id_level' => $level);
					// var_dump($level);
					$this->m_user->up_level($where,$data,'user');
					$this->session->set_userdata($data);

					// Penanganan pembayaran level
					$data_level = $this->m_level->cek_level($level)->result_array();

					$nominal = $data_level['0']['harga_level'];
					// var_dump($nominal);
					$order_id = $this->m_invoice->get_no_invoice_level();
					$sub = substr($nominal,-3);
					$sub2 = substr($nominal,-2);
					$sub3 = substr($nominal,-1);
		 
					$total =  random_string('numeric', 3);
					$total2 =  random_string('numeric', 2);
					$total3 =  random_string('numeric', 1);
		 
						if($sub==0){
							$hasil =  $nominal + $total; 
							$pembayaran = array(
								'id_bayar_level' => $order_id,
								'id_user' => $this->session->userdata('id'),
								'kode_unik_level' => $total,
								'total_bayar_level' => $hasil,
							);
							$this->m_hasil->pembayaran($pembayaran,'pembayaran_level');
							redirect('invoice_upgrade/'.$order_id);
						} else if($sub2 == 0){
							$hasil = $nominal + $total2; 
							$no = substr($hasil,-3);
							$pembayaran = array(
								'id_bayar_level' => $order_id,
								'id_user' => $this->session->userdata('id'),
								'kode_unik_level' => $total,
								'total_bayar_level' => $hasil,
							);
							$this->m_hasil->pembayaran($pembayaran,'pembayaran_level');
							redirect('invoice_upgrade/'.$order_id);
						} else if($sub3 == 0){
							$hasil = $nominal + $total3; 
							$no = substr($hasil,-3);
							$pembayaran = array(
								'id_bayar_level' => $order_id,
								'id_user' => $this->session->userdata('id'),
								'kode_unik_level' => $total,
								'total_bayar_level' => $hasil,
							);
							$this->m_hasil->pembayaran($pembayaran,'pembayaran_level');
							redirect('invoice_upgrade/'.$order_id);
						}else{
							$pembayaran = array(
								'id_bayar_level' => $order_id,
								'id_user' => $this->session->userdata('id'),
								'kode_unik_level' => $total,
								'total_bayar_level' => $nominal,
							);
							$this->m_hasil->pembayaran($pembayaran,'pembayaran_level');
							redirect('invoice_upgrade/'.$order_id);
						}
				}
			}
		}
	
	}
	
	/* End of file Hasil.php */
	/* Location: ./application/controllers/Hasil.php */
?>