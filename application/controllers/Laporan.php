<?php
Class Laporan extends CI_Controller{
    
    function __construct() {
        parent::__construct();
        if($this->session->userdata('logged_in') != TRUE){
                redirect('login');
            }
        $this->load->library('pdf');
        $this->load->model('m_laporan');
    }

    function cetak_per_level(){
        $where = array('user.id_level' => $this->input->post('level'));

        $pdf = new FPDF('p','mm','A4');
        $pdf->SetTitle('Laporan Peserta per Level');
        
        $pdf->AliasNbPages();
        $pdf->AddPage();
        //$pdf->Image('assets/images/logo.png',10,10,50,50);
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Image('assets/images/logo.png',20,10,20,20);
        $pdf->Cell(190,7,'Daftar Peserta AjarinCode.com',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFillColor(17,56,256);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,7,'',0,0);
        $pdf->Cell(50,6,'Nama',1,0,'C',TRUE);
        $pdf->Cell(15,6,'JK',1,0,'C',TRUE);
        $pdf->Cell(27,6,'Asal',1,0,'C',TRUE);
        $pdf->Cell(50,6,'Email',1,0,'C',TRUE);
        $pdf->Cell(25,6,'Level',1,1,'C',TRUE);
        $pdf->SetFont('Arial','',10);
        
        $peserta = $this->m_laporan->list_peserta_level($where,'user')->result();
        foreach ($peserta as $row){
            $pdf->Cell(10,7,'',0,0);
            $pdf->Cell(50,6,$row->nama_user,1,0);
            $pdf->Cell(15,6,$row->jk,1,0,'C');
            $pdf->Cell(27,6,$row->nama_kabupaten,1,0);
            $pdf->Cell(50,6,$row->email,1,0);
            $pdf->Cell(25,6,$row->nama_level,1,1);
        }

        // Go to 1.5 cm from bottom
        $pdf->SetY(15);
        $pdf->SetFont('Arial','I',8);
        $pdf->Cell(0,10,'Page '.$pdf->PageNo().' of {nb} '.' '.' | '.' '.' AjarinCode.com ©2018',0,0,'C');

        $pdf->Output();
    }
    
     function cetak_per_kabupaten(){
        $where = array('user.id_kabupaten' => $this->input->post('kabupaten'));


        $pdf = new FPDF('p','mm','A4');
        $pdf->SetTitle('Laporan Peserta per Kabupaten');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
         // mencetak string 
        $pdf->Image('assets/images/logo.png',20,10,20,20);
        $pdf->Cell(190,7,'Daftar Peserta AjarinCode.com',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFillColor(17,56,256);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,7,'',0,0);
        $pdf->Cell(50,6,'Nama',1,0,'C',TRUE);
        $pdf->Cell(15,6,'JK',1,0,'C',TRUE);
        $pdf->Cell(27,6,'Asal',1,0,'C',TRUE);
        $pdf->Cell(50,6,'Email',1,0,'C',TRUE);
        $pdf->Cell(25,6,'Level',1,1,'C',TRUE);
        $pdf->SetFont('Arial','',10);
        
        $peserta = $this->m_laporan->list_peserta_kabupaten($where,'user')->result();
        foreach ($peserta as $row){
            $pdf->Cell(10,7,'',0,0);
            $pdf->Cell(50,6,$row->nama_user,1,0);
            $pdf->Cell(15,6,$row->jk,1,0,'C');
            $pdf->Cell(27,6,$row->nama_kabupaten,1,0);
            $pdf->Cell(50,6,$row->email,1,0);
            $pdf->Cell(25,6,$row->nama_level,1,1);
        }
        $pdf->Output();
    }
    
    function cetak_mentor(){
        $pdf = new FPDF('p','mm','A4');
        $pdf->SetTitle('Laporan Daftar Mentor');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
        // mencetak string 
        $pdf->Image('assets/images/logo.png',20,10,20,20);
        $pdf->Cell(190,7,'Daftar Mentor AjarinCode.com',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFillColor(17,56,256);
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(10,7,'',0,0);
        $pdf->Cell(50,6,'Nama',1,0,'C',TRUE);
        $pdf->Cell(15,6,'JK',1,0,'C',TRUE);
        $pdf->Cell(27,6,'Asal',1,0,'C',TRUE);
        $pdf->Cell(50,6,'Email',1,0,'C',TRUE);
        $pdf->Cell(25,6,'Username',1,1,'C',TRUE);
        $pdf->SetFont('Arial','',10);
        $peserta = $this->m_laporan->list_mentor()->result();
        foreach ($peserta as $row){
            $pdf->Cell(10,7,'',0,0);
            $pdf->Cell(50,6,$row->nama_user,1,0);
            $pdf->Cell(15,6,$row->jk,1,0,'C');
            $pdf->Cell(27,6,$row->nama_kabupaten,1,0);
            $pdf->Cell(50,6,$row->email,1,0);
            $pdf->Cell(25,6,$row->username,1,1); 
        }
        $pdf->Output();
    }

    function cetak_event_jenis(){
        $where = array('event.id_jenis' => $this->input->post('jenis'));
        $pdf = new FPDF('p','mm','A4');
        $pdf->SetTitle('Laporan Event');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',16);
         // mencetak string 
        $pdf->Image('assets/images/logo.png',20,10,20,20);
        $pdf->Cell(190,7,'Daftar Event AjarinCode.com',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);
        $pdf->Cell(10,7,'',0,1);

        $pdf->SetFont('Arial','B',10);
        $pdf->SetFillColor(17,56,256);
        $pdf->Cell(7,7,'',0,0);
        $pdf->Cell(50,6,'Nama Event',1,0,'C',TRUE);
        $pdf->Cell(65,6,'Pelaksanaan',1,0,'C',TRUE);
        $pdf->Cell(20,6,'Harga',1,0,'C',TRUE);
        $pdf->Cell(40,6,'Level Partisipasi',1,1,'C',TRUE);
        
        $pdf->SetFont('Arial','',10);
        $peserta = $this->m_laporan->list_event_jenis($where,'event')->result();
        foreach ($peserta as $row){
            $pdf->Cell(7,7,'',0,0);
            $pdf->Cell(50,6,$row->nama_event,1,0);
            $pdf->Cell(65,6,date("d/F/Y", strtotime($row->tgl_mulai))." - ".date("d/F/Y", strtotime($row->tgl_akhir)),1,0,'C');
            $pdf->Cell(20,6,'Rp '.number_format($row->harga_event,0,',','.'),1,0);
            $pdf->MultiCell(40,6,$row->id_level,1,1);
        }
        $pdf->Output();
    }

    //echo date("d/F/Y", strtotime($this->session->userdata('gabung')))

}