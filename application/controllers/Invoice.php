<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Invoice extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata('logged_in') != TRUE){
				redirect('login');
			}
			$this->load->model('m_invoice');
		}
	
		public function index($order_id)
		{
			$where = array('id_bayar_event' => $order_id);
			$data['invoice'] = $this->m_invoice->show_invoice($where)->result_array();
			//var_dump($data);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/invoice',$data);
			$this->load->view('dashboard/footer');
		}

		function invoice_level($order_id){
			$where = array('id_bayar_level' => $order_id);
			$data['invoice'] = $this->m_invoice->show_level($where)->result_array();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/invoice_l',$data);
			$this->load->view('dashboard/footer');
		}
	
	}
	
	/* End of file Invoice.php */
	/* Location: ./application/controllers/Invoice.php */
?>