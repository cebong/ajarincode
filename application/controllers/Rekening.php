<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Rekening extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		function tambah(){
			$nomer = $this->input->post('nomer');
			$nama = $this->input->post('nama');
			$inisial = $this->input->post('inisial');
			$pemilik = $this->input->post('pemilik');
			$img = $_FILES['path']['name'];
			$config['upload_path'] = './assets/images/payment_icon/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $inisial;
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload',$config);
			$this->upload->do_upload('path');

			$data = array(
				'path_rek' => $this->upload->data('file_name'),
				'no_rek' => $nomer,
				'nama_rek' => $nama,
				'inisial_rek' => $inisial,
				'pemilik_rek' => $pemilik,
			);

			$this->m_rekening->create($data,'rekening');
			redirect('admin/rekening');
		}
	
	}
	
	/* End of file Rekening.php */
	/* Location: ./application/controllers/Rekening.php */
?>