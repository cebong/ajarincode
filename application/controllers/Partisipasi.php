<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Partisipasi extends CI_Controller {
	
		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function update_status($id){
			$where = array('id_participant' => $id);
			$data = array(
				'status_participant' => "Lunas",
			);
			$this->m_partisipasi->replace_status($where,$data,'participant');
			redirect('admin/rekap_event');
		}
	
	}
	
	/* End of file Partisipasi.php */
	/* Location: ./application/controllers/Partisipasi.php */
?>