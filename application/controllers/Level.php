<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Level extends CI_Controller {
	
		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }

		function tambah(){
			$level = $this->input->post('level');
			$status = $this->input->post('status');
			$harga = $this->input->post('harga');

			$data = array(
				'nama_level' => $level,
				'status_level' => $status,
				'harga_level' => $harga,
			);

			// var_dump($data);
			$data = $this->m_level->create($data,'level');
			if($data >= 1){
				$sukses = array(
					'status' => "gagal",
					'message' => "Level gagal ditambahkan",
				);
				$this->session->set_flashdata($sukses);
				redirect('admin/level');
			}
			else{
				$eror = array(
					'status' => "berhasil",
					'message' => "Level berhasil ditambahkan",
				);
				$this->session->set_flashdata($eror);
				redirect('admin/level');
			}
		}

		function edit($id){
			$where = array('id_level' => $id);
			$data['level'] = $this->m_level->get($where,'level')->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/level/edit', $data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$level = $this->input->post('level');
			$status = $this->input->post('status');
			$harga = $this->input->post('harga');

			$where = array('id_level' => $id);
			$data = array(
				'nama_level' => $level,
				'status_level' => $status,
				'harga_level' => $harga,
			);

			if($this->m_level->replace($where,$data,'level') >= 1){
				$eror = array(
					'status' => "gagal",
					'message' => "Level gagal di update",
				);
				$this->session->set_flashdata($eror);
				redirect('admin/level');
			}
			else{
				$sukses = array(
					'status' => "berhasil",
					'message' => "Level berhasil diupdate",
				);
				$this->session->set_flashdata($sukses);
				redirect('admin/level');
			}
		}
	
		function hapus($id){
			$where = array('id_level' => $id);
			$this->m_level->trash($where,'level');
			redirect('admin/level');
		}

		function view_pembayaran($id){
			$where = array('id_bayar_level' => $id);
			$data['pembayaran'] = $this->m_level->look_payment($where,'pembayaran_level')->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/pembayaran/level/view',$data);
			$this->load->view('dashboard/footer');
		}

		function update_status($id){
			$where = array('id_bayar_level' => $id);
			$data = array(
				'status_pembayaran' => "Lunas",
			);
			$this->m_level->replace_status($where,$data,'pembayaran_level');
			redirect('admin/rekap_level');
		}
	
	}
	
	/* End of file Level.php */
	/* Location: ./application/controllers/Level.php */
?>