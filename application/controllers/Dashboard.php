<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Dashboard extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
    
        public function index()
        {
          $data = array(
            'user' => $this->m_user->list_user()->result(),
          );
           $this->load->view('dashboard/sidebar');
           $this->load->view('dashboard/index',$data);
           $this->load->view('dashboard/footer'); 
        }

        function profil(){
          $data = array(
            'provinsi' => $this->m_provinsi->list_provinsi()->result(),
            'posting' => $this->m_profil->post_user()->result(),
          );
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/profil',$data);
          $this->load->view('dashboard/footer');
        }

        function editor_front(){
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/front');
          $this->load->view('dashboard/footer');
        }

        function css_unifier(){
          $this->load->view('dashboard/css_unminifier');
        }

        function cheat(){
          $this->load->view('dashboard/cheat_sheets_emmet');
        }

        function materi(){
          $data['materi'] = $this->m_materi->list_materi()->result();
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/materi/index', $data);
          $this->load->view('dashboard/footer');
        }

        function lini_masa(){
          $data = array(
            'posting' => $this->m_lini_masa->show_lini()->result(),
            'komentar' => $this->m_lini_masa->show_komen()->result(),
          );
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/lini_masa', $data);
          $this->load->view('dashboard/footer',$data);
        }

        function event(){
          $data['event'] = $this->m_event->list_event()->result();
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/event',$data);
          $this->load->view('dashboard/footer');
        }

        function tes(){
          // $data['hasil_tes'] = $this->m_hasil->cek_nilai()->result();
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/menu_tes');
          $this->load->view('dashboard/footer');
        } 

        function ask_code(){
          $data = array(
            'ask' => $this->m_ask->list_ask_user()->result(),
          );
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/ask_code/index',$data);
          $this->load->view('dashboard/footer');
        }

        function friend(){
          $data = array(
            'friend' => $this->m_user->list_user()->result(),
          );
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/list_friend',$data);
          $this->load->view('dashboard/footer');
        }

        function report(){
          $data['kodeunik'] = $this->m_report->code_otomatis();
          $this->load->view('dashboard/sidebar');
          $this->load->view('dashboard/report/index',$data);
          $this->load->view('dashboard/footer');
        } 
    }
    
    /* End of file Dashboard.php */
    
?>