<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Lini_masa extends CI_Controller {
	
		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function kirim_komen(){
			$timeline = $this->input->post('id');
			$user = $this->session->userdata('id');
			$isi = $this->input->post('komen');

			$data = array(
				'id_timeline' => $timeline,
				'id_user' => $user,
				'isi_komentar' => $isi,
			);

			$this->m_lini_masa->send_coment($data,'komentar');
			redirect('lini_masa');
		}
	}
	
	/* End of file Lini_masa.php */
	/* Location: ./application/controllers/Lini_masa.php */
?>