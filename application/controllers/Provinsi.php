<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Provinsi extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
    
        function tambah(){
            $data['nama_provinsi'] = $this->input->post('provinsi');
            if($this->m_provinsi->create($data,'provinsi') >= 1){
                $gagal = array(
                    'status' => "gagal",
                    'message' => "Provinsi gagal ditambahkan",
                );
                $this->session->set_flashdata($gagal);
                redirect('admin/provinsi');
            }
            else{
                $sukses = array(
                    'status' => "berhasil",
                    'message' => "Provinsi berhasil ditambahkan",
                );
                $this->session->set_flashdata($sukses);
                redirect('admin/provinsi');
            }
        }

        public function edit()
        {
           $id = $this->input->post('id');
           $result = $this->m_provinsi->get($id);

           echo json_encode($result);
        }

        function update(){
            $id = $this->input->post('id');
            $provinsi = $this->input->post('provinsi');

            $where = array('id_provinsi' => $id);
            $data = array('nama_provinsi' => $provinsi);

            if($this->m_provinsi->replace($where,$data,'provinsi') >= 1){
                $gagal = array(
                    'status' => "gagal",
                    'message' => "Provinsi gagal di update",
                );
                $this->session->set_flashdata($gagal);
                redirect('admin/provinsi');
            }
            else{
                $berhasil = array(
                    'status' => "berhasil",
                    'message' => "Provinsi berhasil diupdate",
                );
                $this->session->set_flashdata($berhasil);
                redirect('admin/provinsi');
            }
        }

        function hapus($id){
            $where = array('id_provinsi' => $id);
            $this->m_provinsi->trash($where,'provinsi');
            redirect('admin/provinsi');
        }

        function pencarian(){
            $keyword = $this->input->post('keyword');
            $this->load->library('pagination');
            $jumlah_data = $this->m_provinsi->jumlah_provinsi_dicari($keyword);
            $config['base_url'] = base_url('provinsi/pencarian/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data['provinsi'] = $this->m_provinsi->search($keyword,$config['per_page'],$from)->result();
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/provinsi/index',$data);
            $this->load->view('dashboard/footer');
        }
    
    }
    
    /* End of file Provinsi.php */
    
?>