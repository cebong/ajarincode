<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Report extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		function create_report(){
			$id = $this->input->post('id');
			$user = $this->input->post('user');
			$jenis = $this->input->post('jenis');
			$isi = $this->input->post('isi');
			$img = $_FILES['img']['name'];
			$config['upload_path'] = './report/';
			$config['allowed_types'] = 'jpg|png|gif|jpeg';
			$config['file_name'] = $id;
			$config['file_ext_tolower'] = TRUE;
			$config['overwrite'] = TRUE;
			$config['max_size'] = 2048;

			$this->load->library('upload',$config);
			$this->upload->do_upload('img');

			if($img==null){
				$data = array(
					'id_report' => $id,
					'id_user' => $user,
					'jenis_report' => $jenis,
					'isi_report' => $isi,
					'status_report' => "Menunggu",
				);

				$this->m_report->send_report($data,'report');
				redirect('dashboard/report');
			}
			else{
				$data = array(
					'id_report' => $id,
					'id_user' => $user,
					'jenis_report' => $jenis,
					'isi_report' => $isi,
					'path_report' => $this->upload->data('file_name'),
					'status_report' => "Menunggu",	
				);

				

				$this->m_report->send_report($data,'report');
				redirect('dashboard/report');				
			}	
		}

		function view_report($id){
			$where = array('id_report' => $id);
			$data['report'] = $this->m_report->baca_report($where,'report')->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/report/kelola_report/lihat_report',$data);
			$this->load->view('dashboard/footer');
		}

		function proses_report($id){
			$where = array('id_report' => $id);
			$data = array('status_report' => "Diproses");

			$this->m_report->kerja_report($where,$data,'report');
			redirect('admin/report');
		}

		function finish_report($id){
			$where = array('id_report' => $id);
			$data = array('status_report' => "Clear");

			$this->m_report->cleared_report($where,$data,'report');
			redirect('admin/report');
		}

		function hapus_report($id){
			$where = array('id_report' => $id);
			$this->m_report->trash_report($where,'report');
			redirect('admin/report');
		}
	
	}
	
	/* End of file Report.php */
	/* Location: ./application/controllers/Report.php */
?>