<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Tagihan extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
			if($this->session->userdata('logged_in') != TRUE){

				redirect('login');
			}
			$this->load->model('m_tagihan');
		}
	
		public function index()
		{
			$data = array(
				'tagihan' => $this->m_tagihan->list_tagihan()->result(),
				'level' => $this->m_tagihan->tagihan_level()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/tagihan',$data);
			$this->load->view('dashboard/footer');
		}

		function upload_bukti_tagihan(){
		   	$id = $this->input->post('id');
           	$result = $this->m_tagihan->show_tagihan($id);

           	echo json_encode($result);
		}

		function upload_bukti_level(){
		   	$id = $this->input->post('id');
           	$result = $this->m_tagihan->show_tagihan_level($id);

           	echo json_encode($result);
		}

		function update_tagihan(){
			$order_id = $this->input->post('id');
			$participant = $this->input->post('participant');
			$img = $_FILES['gambar']['name'];
			$config['upload_path'] = './assets/event/bukti_bayar/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $order_id." ".$this->session->userdata('username');
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload',$config);
			$this->upload->do_upload('gambar');

			$gbr = $this->upload->data();

			$config['img_library'] = 'gd2';
			$config['source_image'] = './assets/event/bukti_bayar/'.$gbr['file_name'];
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = FALSE;
			$config['quality'] = '50%';
			$config['width'] = 512;
			$config['height'] = 512;
			$config['new_image'] = './assets/event/bukti_bayar/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			if(!$this->image_lib->resize()){
				echo $this->image_lib->display_errors();
			}

			$where_bayar = array('id_bayar_event' => $order_id);
			$data_bayar = array('bukti_bayar_event' => $this->upload->data('file_name'));
			$this->m_tagihan->replace_tagihan($where_bayar,$data_bayar,'pembayaran_event');
			redirect('tagihan');
		}

		function update_tagihan_level(){
			$order_id = $this->input->post('id');
			$img = $_FILES['gambar']['name'];
			$config['upload_path'] = './assets/bukti_level/';
			$config['allowed_types'] = 'jpg|png|jpeg';
			$config['overwrite'] = TRUE;
			$config['file_name'] = $order_id." ".$this->session->userdata('username');
			$config['file_ext_tolower'] = TRUE;
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload',$config);
			$this->upload->do_upload('gambar');

			$gbr = $this->upload->data();

			$config['img_library'] = 'gd2';
			$config['source_image'] = './assets/bukti_level/'.$gbr['file_name'];
			$config['create_thumb'] = FALSE;
			$config['maintain_ratio'] = FALSE;
			$config['quality'] = '50%';
			$config['width'] = 512;
			$config['height'] = 512;
			$config['new_image'] = './assets/bukti_level/'.$gbr['file_name'];
			$this->load->library('image_lib', $config);
			$this->image_lib->resize();
			$this->image_lib->clear();

			if(!$this->image_lib->resize()){
				echo $this->image_lib->display_errors();
			}

			$where_bayar = array('id_bayar_level' => $order_id);
			$data_bayar = array('bukti_bayar_level' => $this->upload->data('file_name'));
			$this->m_tagihan->replace_tagihan_level($where_bayar,$data_bayar,'pembayaran_level');
			redirect('tagihan');
		}
	
	}
	
	/* End of file Tagihan.php */
	/* Location: ./application/controllers/Tagihan.php */
?>