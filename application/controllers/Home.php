<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Home extends CI_Controller {
    
        public function index()
        {
            $this->load->view('header');
            $this->load->view('index');
            $this->load->view('footer');
        }

        function about(){
            $this->load->view('header');
            $this->load->view('about');
            $this->load->view('footer');
        }

        function pricing(){
            $this->load->view('header');
            $this->load->view('price');
            $this->load->view('footer');
        }

        function contact(){
            $this->load->view('header');
            $this->load->View('contact');
            $this->load->view('footer');
        }

        function kirim_email(){
            $nama = $this->input->post('nama');
            $email = $this->input->post('email');
            $message = $this->input->post('message');

            $data = array(
                'nama_contact' => $nama,
                'email_contact' => $email,
                'message_contact' => $message,
                'status_contact' => "Belum direspon",
            );

            $this->m_auth->contact_us($data,'contact');
            $success = array(
                'status' => "Berhasil",
                'message' => "Terimakasih atas perhatian anda, silahkan tunggu email balasan dari kami",
            );
            $this->session->set_flashdata($success);
            redirect('contact');

        }

        public function edit()
        {
           $id = $this->input->post('id');
           $result = $this->m_email->get($id);

           echo json_encode($result);
        }

        function status_contact($id){
            $where = array('id_contact' => $id);
            $data = array('status_contact' => "Direspon");

            $this->m_email->update_status($where,$data,'contact');
            redirect('admin/email');
        }

        function login(){
            $this->load->view('login');
        }

        // function create_captcha(){
        //     $this->load->helper('captcha');
        //     $options = array(
        //         'img_path' => './captcha/',
        //         'img_url' => base_url('captcha'),
        //         'img_width' => 300,
        //         'img_height' => 60,
        //         'expiration' => 7200,
        //     );

        //     $cap = create_captcha($options);
        //     $image = $cap['image'];

        //     $this->session->set_userdata('captchaword',$cap['word']);

        //     return $image;
        // }

        function register(){
            $data = array(
                'provinsi' => $this->m_provinsi->list_provinsi()->result(),
                //'img' => $this->create_captcha(),
                'captcha' => $this->recaptcha->getWidget(),
                'script_captcha' => $this->recaptcha->getScriptTag(),
            );
            $this->load->view('register',$data);
        }

        function get_kabupaten(){
            $id = $this->input->post('id');
            $data = $this->m_auth->get_subkategori($id);
            echo json_encode($data);
        }

        function lupa_password(){
            $this->load->view('forget');
        }

        function change_password($key){
            $where = array('md5(id_user)' => $key);
            $data = array('user' => $this->m_auth->data($where,'user')->row());
            $this->load->view('ganti',$data);
        }
    
    }
    
    /* End of file Home.php */
    
?>