<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Kategori extends CI_Controller {

		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function tambah(){
			$kategori = $this->input->post('kategori');
			$status = $this->input->post('status');

			$data = array(
				'nama_kategori' => $kategori,
				'status_kategori' => $status,
			);

			if($this->m_kategori->create($data,'kategori') >= 1){
				$gagal = array(
					'status' => "gagal",
					'message' => "Kategori gagal ditambahkan",
				);
				$this->session->set_flashdata($gagal);
				redirect('admin/kategori');
			}
			else{
				$sukses = array(
					'status' => "berhasil",
					'message' => "Kategori berhasil ditambahkan",
				);
				$this->session->set_flashdata($sukses);
				redirect('admin/kategori');
			}
		}

		function edit($id){
			$where = array('id_kategori' => $id);
			$data['kategori'] = $this->m_kategori->get($where,'kategori')->result();
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/master/kategori/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update(){
			$id = $this->input->post('id');
			$kategori = $this->input->post('kategori');
			$status = $this->input->post('status');

			$where = array('id_kategori' => $id);
			$data = array(
				'nama_kategori' => $kategori,
				'status_kategori' => $status,
			);

			if($this->m_kategori->replace($where,$data,'kategori') >= 1){
				$gagal = array(
					'status' => "gagal",
					'message' => "Kategori gagal diupdate",
				);
				$this->session->set_flashdata($gagal);
				redirect('admin/kategori');
			}
			else{
				$sukses = array(
					'status' => "berhasil",
					'message' => "Kategori berhasil diupdate",
				);
				$this->session->set_flashdata($sukses);
				redirect('admin/kategori');
			}
		}

		function hapus($id){
			$where = array('id_kategori' => $id);
			$this->m_kategori->trash($where,'kategori');
			redirect('admin/kategori');
		}
	
	}
	
	/* End of file Kategori.php */
	/* Location: ./application/controllers/Kategori.php */
?>