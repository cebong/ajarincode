<?php
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class User extends CI_Controller {

		public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
	
		function tambah_mentor(){
			$nama = $this->input->post('nama');
			$jk = $this->input->post('jk');
			$tgl = $this->input->post('tanggal');
			$nomer = $this->input->post('nomer');
			$asal = $this->input->post('kabupaten');
			$username = $this->input->post('username');
			$password = $this->input->post('password');
			$email = $this->input->post('email');

			$data = array(
				'nama_user' => $nama,
				'jk' => $jk,
				'tgl_lahir' => $tgl,
				'no_user' => $nomer,
				'id_kabupaten' => $asal,
				'username' => $username,
				'password' => md5($password),
				'email' => $email,
				'path' => "default.png",
				'bio' => "Hi, I'm Join AjarinCode Now",
				'id_level' => 1,
				'status_user' => "Aktif",
				'hak_akses' => "Mentor",
			);

			$this->m_user->create_mentor($data,'user');
			redirect('admin/mentor');
		}

		function edit_mentor($id){
			$where = array('id_user' => $id);
			$data = array(
				'user' => $this->m_user->get_mentor($where,'user')->result(),
				'provinsi' => $this->m_provinsi->list_provinsi()->result(),
				'level' => $this->m_level->list_level()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/user/mentor/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function edit_peserta($id){
			$where = array('id_user' => $id);
			$data = array(
				'user' => $this->m_user->get_peserta($where,'user')->result(),
				'level' => $this->m_level->list_level()->result(),
			);
			$this->load->view('dashboard/sidebar');
			$this->load->view('dashboard/user/peserta/edit',$data);
			$this->load->view('dashboard/footer');
		}

		function update_peserta(){
			$id = $this->input->post('id');
			$level = $this->input->post('level');
			$akses = $this->input->post('hak');

			$where = array('id_user' => $id);
			$data = array(
				'id_level' => $level,
				'hak_akses' => $akses,
			);
			$this->m_user->replace_peserta($where,$data,'user');
			redirect('admin/peserta');
		}

		function hapus_peserta($id){
			$where = array('id_user' => $id);
			$this->m_user->trash_peserta($where,'user');
			redirect('admin/peserta');
		}

		function pencarian_peserta(){
            $keyword = $this->input->post('keyword');
            $this->load->library('pagination');
            $jumlah_data = $this->m_user->jumlah_peserta_dicari($keyword);
            $config['base_url'] = base_url('user/pencarian/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data['user'] = $this->m_user->search_peserta($keyword,$config['per_page'],$from)->result();
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/user/peserta/index',$data);
            $this->load->view('dashboard/footer');
        }
	
	}
	
	/* End of file User.php */
	/* Location: ./application/controllers/User.php */
?>