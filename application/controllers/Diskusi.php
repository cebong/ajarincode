<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class Diskusi extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();
		}
	
		function tambah(){
			$username = $this->input->post('username');
			$isi = $this->input->post('diskusi');

			$data = array(
				'id_user' => $username,
				'isi_diskusi' => $isi,
			);

			$result = $this->m_diskusi->create($data,'diskusi');
			echo json_encode($result);
		}

		function get_chat(){
			$result = $this->m_diskusi->show_diskusi();
			echo json_encode($result);
		}
	
	}
	
	/* End of file Diskusi.php */
	/* Location: ./application/controllers/Diskusi.php */
?>