<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class Kabupaten extends CI_Controller {

        public function __construct()
        {
            parent::__construct();
            if($this->session->userdata('logged_in') != TRUE){
                redirect('login');  
            }
        }
    
        function tambah(){
        	$provinsi = $this->input->post('provinsi');
        	$kabupaten = $this->input->post('kabupaten');

        	$data = array(
        		'id_provinsi' => $provinsi,
        		'nama_kabupaten' => $kabupaten,
        	);

            if($this->m_kabupaten->create($data,'kabupaten') >= 1){
                $gagal = array(
                    'status' => "gagal",
                    'message' => "Kabupaten gagal ditambahkan",
                );
                $this->session->set_flashdata($gagal);
                redirect('admin/kabupaten');
            }
            else{
                $sukses = array(
                    'status' => "berhasil",
                    'message' => "Kabupaten berhasil ditambahkan",
                );
                $this->session->set_flashdata($sukses);
                redirect('admin/kabupaten');
            }
        }

        function edit($id){
            $where = array('id_kabupaten' => $id);
            $data = array(
                'kabupaten' => $this->m_kabupaten->get($where,'kabupaten')->result(),
                'provinsi' => $this->m_provinsi->list_provinsi()->result(),
            );
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/kabupaten/edit', $data);
            $this->load->view('dashboard/footer');
        }

        function update(){
        	$id = $this->input->post('id');
        	$provinsi = $this->input->post('provinsi');
        	$kabupaten = $this->input->post('kabupaten');

        	$where = array('id_kabupaten' => $id);
        	$data = array(
        		'id_provinsi' => $provinsi,
        		'nama_kabupaten' => $kabupaten,
        	);

            if($this->m_kabupaten->replace($where,$data,'kabupaten') >= 1){
                $gagal = array(
                    'status' => "gagal",
                    'message' => "Kabupaten gagal diupdate",
                );
                $this->session->set_flashdata($gagal);
                redirect('admin/kabupaten');
            }
            else{
                $sukses = array(
                    'status' => "berhasil",
                    'message' => "Kabupaten berhasil diupdate",
                );
                $this->session->set_flashdata($sukses);
                redirect('admin/kabupaten');
            }
        }

        function hapus($id){
        	$where = array('id_kabupaten' => $id);
        	$this->m_kabupaten->trash($where,'kabupaten');
        	redirect('admin/kabupaten');
        }

        function pencarian(){
            $keyword = $this->input->post('keyword');
            $this->load->library('pagination');
            $jumlah_data = $this->m_kabupaten->jumlah_kabupaten_dicari($keyword);
            $config['base_url'] = base_url('kabupaten/pencarian/');
            $config['total_rows'] = $jumlah_data;
            $config['per_page'] = 10;
            $config['first_link'] = 'First';
            $config['last_link'] = 'Last';
            $config['next_link'] = 'Next';
            $config['prev_link'] = 'Prev';
            $config['full_tag_open']    = '<div class="pagging text-center"><nav><ul class="pagination justify-content-center">';
            $config['full_tag_close']   = '</ul></nav></div>';
            $config['num_tag_open']     = '<li class="page-item"><span class="page-link">';
            $config['num_tag_close']    = '</span></li>';
            $config['cur_tag_open']     = '<li class="page-item active"><span class="page-link">';
            $config['cur_tag_close']    = '<span class="sr-only">(current)</span></span></li>';
            $config['next_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['next_tagl_close']  = '<span aria-hidden="true">&raquo;</span></span></li>';
            $config['prev_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['prev_tagl_close']  = '</span>Next</li>';
            $config['first_tag_open']   = '<li class="page-item"><span class="page-link">';
            $config['first_tagl_close'] = '</span></li>';
            $config['last_tag_open']    = '<li class="page-item"><span class="page-link">';
            $config['last_tagl_close']  = '</span></li>';
            
            $from = $this->uri->segment(3);
            $this->pagination->initialize($config);
            $data['kabupaten'] = $this->m_kabupaten->search($keyword,$config['per_page'],$from)->result();
            $this->load->view('dashboard/sidebar');
            $this->load->view('dashboard/master/kabupaten/index',$data);
            $this->load->view('dashboard/footer');
        }
    
    }
    
    /* End of file Kabupaten.php */
    
?>