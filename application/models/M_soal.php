<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_soal extends CI_Model {
	
		function list_soal(){
			$this->db->select('level.nama_level,soal.id_soal,soal.pertanyaan,soal.kunci,kategori.nama_kategori');
			$this->db->join('level','level.id_level=soal.id_level');
			$this->db->join('kategori','kategori.id_kategori=soal.id_kategori');
			return $this->db->get('soal');
		}

		public function jumlah_soal(){
            return $this->db->get('soal')->num_rows();
        }

        public function data($number,$offset){
        	$this->db->select('level.nama_level,soal.id_soal,soal.pertanyaan,soal.kunci,kategori.nama_kategori');
			$this->db->join('level','level.id_level=soal.id_level');
			$this->db->join('kategori','kategori.id_kategori=soal.id_kategori');
            return $this->db->get('soal',$number,$offset);
        }

		function create_soal($pertanyaan,$table){
			$this->db->insert($table,$pertanyaan);
			$id = $this->db->insert_id();
			return (isset($id)) ? $id : FALSE;
		}

		function create_jawaban($jawaban,$table){
			$this->db->insert($table,$jawaban);
		}

		function get_soal($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function get_jawaban($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace_soal($where,$pertanyaan,$table){
			$this->db->where($where);
			$this->db->update($table,$pertanyaan);
		}

		function replace_jawaban($where,$jawaban,$table){
			$this->db->where($where);
			$this->db->update($table,$jawaban);
		}

		function trash_soal($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}

		function trash_jawaban($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}

		function tampil_soal($where,$table){
			$this->db->select('soal.*,opsi.*');
			$this->db->join('level','level.id_level=soal.id_level');
			$this->db->join('opsi','opsi.id_soal=soal.id_soal');
			$this->db->where('nama_level', $this->session->userdata('level'));
			$this->db->where($where);
			return $this->db->get($table);
		}

		function tampil_jawaban($table){
			$this->db->select('opsi.*,soal.*');
			$this->db->from($table);
			$this->db->join('soal','soal.id_soal=opsi.id_soal');
			$this->db->where();
			return $this->db->get();
		}

		function simpan_nilai_tes($data,$table){
			$this->db->insert($table,$data);
		}

		function update_nilai($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
	
	}
	
	/* End of file M_soal.php */
	/* Location: ./application/models/M_soal.php */
?>