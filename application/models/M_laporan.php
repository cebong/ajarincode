<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_laporan extends CI_Model {
	
		function list_peserta_level($where,$table){
			$this->db->select('user.*,level.*,kabupaten.*');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
			$this->db->where('hak_akses','Peserta');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function list_peserta_kabupaten($where,$table){
			$this->db->select('user.*,level.*,kabupaten.*');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
			$this->db->where('hak_akses','Peserta');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function list_mentor(){
			$this->db->select('user.*,level.*,kabupaten.*');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
			$this->db->where('hak_akses','Mentor');
			return $this->db->get('user');
		}

		function list_event_jenis($where,$table){
			$this->db->select('event.*,jenis_event.*');
			$this->db->join('jenis_event','jenis_event.id_jenis=event.id_jenis');
			$this->db->where($where);
			return $this->db->get($table);
		}
	
	}
	
	/* End of file M_laporan.php */
	/* Location: ./application/models/M_laporan.php */
?>