<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_provinsi extends CI_Model {

        public function jumlah_provinsi(){
            return $this->db->get('provinsi')->num_rows();
        }

        public function data($number,$offset){
            return $this->db->get('provinsi',$number,$offset);
        }
    
        function list_provinsi(){
            return $this->db->get('provinsi');
        }

        function create($data,$table){
            $this->db->insert($table,$data);
        }

        public function get($id){
          $this->db->select('*');
          $this->db->from('provinsi');
          $this->db->where('id_provinsi', $id);
          return $this->db->get()->row_array();
        }

        function replace($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        public function jumlah_provinsi_dicari($keyword){
            $this->db->like('nama_provinsi',$keyword);
            return $this->db->get('provinsi')->num_rows();
        }

        function search($keyword,$number,$offset){
            $this->db->like('nama_provinsi',$keyword);
            return $this->db->get('provinsi',$number,$offset);
        }
    
    }
    
    /* End of file M_provinsi.php */
    
?>