<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_rekening extends CI_Model {
	
		function list_rekening(){
			return $this->db->get('rekening');
		}

		function create($data,$table){
			$this->db->insert($table,$data);
		}
	
	}
	
	/* End of file M_rekening.php */
	/* Location: ./application/models/M_rekening.php */
?>