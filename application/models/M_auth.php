<?php 
    
    defined('BASEPATH') OR exit('No direct script access allowed');
    
    class M_auth extends CI_Model {
    
        function cek_login($where,$table){
            $this->db->select('user.id_user,user.nama_user,user.jk,user.tgl_lahir,user.no_user,kabupaten.nama_kabupaten,user.username,user.password,user.email,user.path,user.bio,level.nama_level,user.status_user,user.tgl_join,user.hak_akses,user.confirm,level.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->where($where);
            return $this->db->get($table);
        }

        function cek_data($email,$username,$table){
            $this->db->where('email',$email);
            $this->db->or_where('username',$username);
            return $this->db->get($table);
        }   

        function data($where,$table){
            $this->db->where($where);
            return $this->db->get($table);
        }

        function get_subkategori($id){
            $hasil=$this->db->query("SELECT * FROM kabupaten WHERE id_provinsi='$id'");
            return $hasil->result();
        } 

        function daftar($data,$table){
            $this->db->insert($table,$data);
            $id = $this->db->insert_id();
            return (isset($id)) ? $id : FALSE;
        }

        function changeActive($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        function cek_email($where,$table){
            $this->db->where($where);
            return $this->db->get($table);
        }

        function changePassword($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        function contact_us($data,$table){
            $this->db->insert($table,$data);
        }
    
    }
    
    /* End of file M_auth.php */
    
?>