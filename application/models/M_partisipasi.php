<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_partisipasi extends CI_Model {
	
		function list_partisipasi(){
			$this->db->select('participant.id_participant,user.username,event.nama_event,participant.status_participant');
			$this->db->join('user','user.id_user=participant.id_user');
			$this->db->join('event','event.id_event=participant.id_event');
			return $this->db->get('participant');
		}

		public function jumlah_partisipasi(){
            return $this->db->get('participant')->num_rows();
        }

        public function data($number,$offset){
        	$this->db->select('participant.id_participant,user.username,event.nama_event,participant.status_participant');
			$this->db->join('user','user.id_user=participant.id_user');
			$this->db->join('event','event.id_event=participant.id_event');
            return $this->db->get('participant',$number,$offset);
        }

        function replace_status($where,$data,$table){
        	$this->db->where($where);
        	$this->db->update($table,$data);
        }
	
	}
	
	/* End of file M_partisipasi.php */
	/* Location: ./application/models/M_partisipasi.php */
?>