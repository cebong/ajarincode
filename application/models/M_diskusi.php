<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_diskusi extends CI_Model {
	
		function show_diskusi(){
			$this->db->select('diskusi.*,user.*');
			$this->db->join('user','user.id_user=diskusi.id_user');
			return $this->db->get('diskusi');
		}

		function create($data,$table){
			$this->db->insert($table,$data);
		}
	
	}
	
	/* End of file M_diskusi.php */
	/* Location: ./application/models/M_diskusi.php */
?>