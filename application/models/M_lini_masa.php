<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_lini_masa extends CI_Model {
	
		function show_lini(){
			$this->db->select('timeline.id_timeline,user.username,user.path,timeline.isi_timeline,timeline.tgl_timeline');
			$this->db->join('user','user.id_user=timeline.id_user');
			return $this->db->get('timeline');
		}

		function show_komen(){
			$this->db->select('timeline.*,komentar.*,user.*');
			$this->db->join('timeline','timeline.id_timeline=komentar.id_timeline');
			$this->db->join('user','user.id_user=komentar.id_user');
			return $this->db->get('komentar');
		}

		function send_coment($data,$table){
			$this->db->insert($table,$data);
		}
	
	}
	
	/* End of file M_lini_masa.php */
	/* Location: ./application/models/M_lini_masa.php */
?>