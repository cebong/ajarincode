<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_user extends CI_Model {

        public function list_user(){
            $this->db->select('user.*,level.*,kabupaten.*');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->not_like('hak_akses','Admin');
            $this->db->not_like('id_user',$this->session->userdata('id'));
            return $this->db->get('user');
        }

        public function list_peserta(){
            $this->db->select('user.*,level.*,kabupaten.*');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->not_like('hak_akses','Admin');
            $this->db->not_like('id_user',$this->session->userdata('id'));
            return $this->db->get('user');
        }
	
		function jumlah_user(){
			$this->db->like('hak_akses','Peserta');
			return $this->db->get('user')->num_rows();
		}

		public function data_peserta($number,$offset){
			$this->db->select('user.id_user,user.username,user.email,user.jk,level.nama_level,kabupaten.nama_kabupaten,user.confirm,user.password');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
			$this->db->like('hak_akses','Peserta');
            return $this->db->get('user',$number,$offset);
        }

        public function jumlah_peserta_dicari($keyword){
            $this->db->like('username',$keyword);
            $this->db->like('hak_akses','Peserta');
            return $this->db->get('user')->num_rows();
        }

        function search_peserta($keyword,$number,$offset){
            $this->db->select('user.id_user,user.username,user.email,user.jk,level.nama_level,kabupaten.nama_kabupaten,user.confirm');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->like('hak_akses','Peserta');
            $this->db->like('username',$keyword);
            return $this->db->get('user',$number,$offset);
        }

        function get_peserta($where,$table){
            $this->db->select('user.*,level.*,kabupaten.*');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->where($where);
            return $this->db->get($table);
        }

        function replace_peserta($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }

        function trash_peserta($where,$table){
            $this->db->where($where);
            $this->db->delete($table);
        }

        function list_mentor(){
            $this->db->select('user.id_user,user.nama_user,user.email,user.jk,level.nama_level,kabupaten.nama_kabupaten');
            $this->db->join('level','level.id_level=user.id_level');
            $this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
            $this->db->like('hak_akses','Mentor');
            return $this->db->get('user');
        }

        function jumlah_mentor(){
        	$this->db->like('hak_akses','Mentor');
        	return $this->db->get('user')->num_rows();
        }

        function data_mentor($number,$offset){
        	$this->db->select('user.id_user,user.username,user.email,user.jk,level.nama_level,kabupaten.nama_kabupaten');
        	$this->db->join('level','level.id_level=user.id_level');
        	$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
        	$this->db->like('hak_akses','Mentor');
        	return $this->db->get('user',$number,$offset);
        }

        function create_mentor($data,$table){
        	$this->db->insert($table,$data);
        }

        function get_mentor($where,$table){
        	$this->db->select('user.id_user,user.nama_user,user.jk,user.tgl_lahir,user.no_user,kabupaten.nama_kabupaten,user.username,user.email,level.nama_level,user.status_user,user.id_level');
        	$this->db->join('level','level.id_level=user.id_level');
        	$this->db->join('kabupaten','kabupaten.id_kabupaten=user.id_kabupaten');
        	$this->db->where($where);
        	return $this->db->get($table);
        }

        function up_level($where,$data,$table){
            $this->db->where($where);
            $this->db->update($table,$data);
        }
	
	}
	
	/* End of file M_user.php */
	/* Location: ./application/models/M_user.php */
?>