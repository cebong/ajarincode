<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_event extends CI_Model {
	
		function list_event(){
			$this->db->select('event.id_event,jenis_event.nama_jenis,event.nama_event,event.tgl_mulai,event.tgl_akhir,event.deskripsi_event,event.harga_event,event.status_event,event.path_event,event.id_level');
			$this->db->join('jenis_event','jenis_event.id_jenis=event.id_jenis');
			return $this->db->get('event');
		}

		public function jumlah_event(){
            return $this->db->get('event')->num_rows();
        }

        public function data($number,$offset){
        	$this->db->select('event.id_event,jenis_event.nama_jenis,event.nama_event,event.tgl_mulai,event.tgl_akhir,event.deskripsi_event,event.harga_event,event.status_event,event.path_event,event.id_level');
			$this->db->join('jenis_event','jenis_event.id_jenis=event.id_jenis');
            return $this->db->get('event',$number,$offset);
        }

		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function update($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$row = $this->db->get($table)->row();
			unlink("./assets/event/image/".$row->path_event);
			$this->db->delete($table,$where);
		}

		function daftar_event($data,$table){
			$this->db->insert($table,$data);
			$id = $this->db->insert_id();
			return (isset($id)) ? $id : FALSE;
		}

		function pembayaran($pembayaran,$table){
			$this->db->insert($table,$pembayaran);
		}

		function jumlah_bayar_event(){
			return $this->db->get('pembayaran_event')->num_rows();
		}

		function list_pembayaran_event($number,$offset){
			$this->db->select('pembayaran_event.*,participant.*,user.*,event.*')
			->join('participant','participant.id_participant=pembayaran_event.id_participant')
			->join('user','user.id_user=participant.id_user')
			->join('event','event.id_event=participant.id_event');
			return $this->db->get('pembayaran_event',$number,$offset);
		}

		function look_payment($where,$table){
			$this->db->select('pembayaran_event.*,user.*,participant.*,event.*');
			$this->db->join('participant','participant.id_participant=pembayaran_event.id_participant');
			$this->db->join('event','event.id_event=participant.id_event');
			$this->db->join('user','user.id_user=participant.id_user');
			$this->db->where($where);
			return $this->db->get($table);
		}
	
	}
	
	/* End of file M_event.php */
	/* Location: ./application/models/M_event.php */
?>