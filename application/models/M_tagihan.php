<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_tagihan extends CI_Model {
	
		function list_tagihan(){
			$this->db->select('pe.*, p.*, e.*')
	    	->from('pembayaran_event as pe')
	    	->join('participant as p','p.id_participant=pe.id_participant')
	    	->join('event as e','e.id_event=p.id_event');
	    	$this->db->where('id_user',$this->session->userdata('id'));
	    	return $this->db->get();
		}

		function tagihan_level(){
			$this->db->select('pembayaran_level.*,user.*,level.*');
			$this->db->join('user','user.id_user=pembayaran_level.id_user');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->where('pembayaran_level.id_user',$this->session->userdata('id'));
			return $this->db->get('pembayaran_level');
		}

		function show_tagihan($id){
			$this->db->select('pe.*, p.*, e.*')
	    	->from('pembayaran_event as pe')
	    	->join('participant as p','p.id_participant=pe.id_participant')
	    	->join('event as e','e.id_event=p.id_event');
	    	$this->db->where('id_bayar_event',$id);
	    	return $this->db->get()->row_array();
		}

		function show_tagihan_level($id){
			$this->db->select('pe.*, p.*, e.*')
	    	->from('pembayaran_level as pe')
	    	->join('user as p','p.id_user=pe.id_user')
	    	->join('level as e','e.id_level=p.id_level');
	    	$this->db->where('id_bayar_level',$id);
	    	return $this->db->get()->row_array();
		}

		function replace_tagihan($where_bayar,$data_bayar,$table){
			$this->db->where($where_bayar);
			$this->db->update($table,$data_bayar);
		}

		function replace_tagihan_level($where_bayar,$data_bayar,$table){
			$this->db->where($where_bayar);
			$this->db->update($table,$data_bayar);
		}

		function cek_lunas($id,$table){
			$this->db->where('pembayaran_level.id_user',$id);
			$this->db->where('status_pembayaran','Belum Lunas');
			return $this->db->get($table);
		}
	
	}
	
	/* End of file M_tagihan.php */
	/* Location: ./application/models/M_tagihan.php */
?>