<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_invoice extends CI_Model {
	
		function get_no_invoice(){
	        $q = $this->db->query("SELECT MAX(RIGHT(id_bayar_event,4)) AS kd_max FROM pembayaran_event WHERE DATE(tgl_bayar_event)=CURDATE()");
	        $kd = "";
	        if($q->num_rows()>0){
	            foreach($q->result() as $k){
	                $tmp = ((int)$k->kd_max)+1;
	                $kd = sprintf("%04s", $tmp);
	            }
	        }else{
	            $kd = "0001";
	        }
	        date_default_timezone_set('Asia/Jakarta');
	        return date('dmy').$kd;
	    }

	    function get_no_invoice_level(){
	        $q = $this->db->query("SELECT MAX(RIGHT(id_bayar_level,4)) AS kd_max FROM pembayaran_level WHERE DATE(tgl_bayar_level)=CURDATE()");
	        $kd = "";
	        if($q->num_rows()>0){
	            foreach($q->result() as $k){
	                $tmp = ((int)$k->kd_max)+1;
	                $kd = sprintf("%04s", $tmp);
	            }
	        }else{
	            $kd = "0001";
	        }
	        date_default_timezone_set('Asia/Jakarta');
	        return date('dmy').$kd;
	    }

	    function show_invoice($where){
	    	$this->db->select('pe.*, p.*, e.*')
	    	->from('pembayaran_event as pe')
	    	->join('participant as p','p.id_participant=pe.id_participant')
	    	->join('event as e','e.id_event=p.id_event');
	    	$this->db->where($where);
	    	return $this->db->get();
	    }

	    function show_level($where){
	    	$this->db->select('pembayaran_level.*,user.*,level.*');
	    	$this->db->from('pembayaran_level');
	    	$this->db->join('user','user.id_user=pembayaran_level.id_user');
	    	$this->db->join('level','level.id_level=user.id_level');
	    	$this->db->where($where);
	    	return $this->db->get();
	    }
	
	}
	
	/* End of file M_invoice.php */
	/* Location: ./application/models/M_invoice.php */
?>