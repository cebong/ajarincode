<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_hasil extends CI_Model {
	
		public function jumlah_hasil(){
            return $this->db->get('hasil_tes')->num_rows();
        }

        public function data_hasil($number,$offset){
        	$this->db->select('hasil_tes.*,user.*,kategori.*,level.*');
        	$this->db->join('user','user.id_user=hasil_tes.id_user');
        	$this->db->join('kategori','kategori.id_kategori=hasil_tes.id_kategori');
        	$this->db->join('level','level.id_level=hasil_tes.id_level');
            return $this->db->get('hasil_tes',$number,$offset);
        }

        function cek_user($user,$id_level,$table){
        	$this->db->where('id_user',$user);
        	$this->db->where('id_level',$id_level);
        	return $this->db->get($table);
        }

        function cek_nilai(){
            $this->db->select('nilai');
            // $this->db->select_min('nilai');
            $this->db->where('id_user',$this->session->userdata('id'));
            $this->db->where('id_level',$this->session->userdata('id_level'));
            $this->db->where('nilai <',70);
            return $this->db->get('hasil_tes');
        }

        function pembayaran($pembayaran,$table){
            $this->db->insert($table,$pembayaran);
        }
	
	}
	
	/* End of file M_hasil.php */
	/* Location: ./application/models/M_hasil.php */
?>