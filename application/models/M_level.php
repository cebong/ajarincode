<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_level extends CI_Model {
	
		function list_level(){
			return $this->db->get('level');
		}

		public function jumlah_level(){
            return $this->db->get('level')->num_rows();
        }

        public function data($number,$offset){
            return $this->db->get('level',$number,$offset);
        }

		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}

		function cek_level($level){
			// $this->db->select('harga_level');
			$this->db->where('id_level',$level);
			return $this->db->get('level');
		}

		function jumlah_bayar_level(){
			return $this->db->get('pembayaran_level')->num_rows();
		}

		function list_pembayaran_level($number,$offset){
			$this->db->select('pembayaran_level.*,level.*,user.*');
			$this->db->join('user','user.id_user=pembayaran_level.id_user');
			$this->db->join('level','level.id_level=user.id_level');
			return $this->db->get('pembayaran_level',$number,$offset);
		}

		function look_payment($where,$table){
			$this->db->select('pembayaran_level.*,user.*,level.*');
			$this->db->join('user','user.id_user=pembayaran_level.id_user');
			$this->db->join('level','level.id_level=user.id_level');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace_status($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
	
	}
	
	/* End of file M_level.php */
	/* Location: ./application/models/M_level.php */
?>