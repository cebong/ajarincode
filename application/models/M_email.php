<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_email extends CI_Model {
	
		public function jumlah_email(){
            return $this->db->get('contact')->num_rows();
        }

        public function list_email($number,$offset){
            return $this->db->get('contact',$number,$offset);
        }

        public function get($id){
        	$this->db->where('id_contact',$id);
        	return $this->db->get('contact')->row_array();
        }

        function update_status($where,$data,$table){
        	$this->db->where($where);
        	$this->db->update($table,$data);
        }
	
	}
	
	/* End of file M_email.php */
	/* Location: ./application/models/M_email.php */
?>