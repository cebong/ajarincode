<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_ask extends CI_Model {

		function code_otomatis(){
            $this->db->select('Right(ask_code.id_ask,3) as kode ',false);
            $this->db->order_by('id_ask', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('ask_code');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "ACAC-".$kodemax;
            return $kodejadi;

        }
	
		function jumlah_ask(){
			return $this->db->get('ask_code')->num_rows();
		}

		function list_ask($number,$offset){
			$this->db->select('ask_code.*,user.*');
			$this->db->join('user','user.id_user=ask_code.id_user');
			$this->db->like('status_ask','Pending');
			return $this->db->get('ask_code',$number,$offset);
		}

		function list_ask_user(){
			$this->db->where('id_user',$this->session->userdata('id'));
			return $this->db->get('ask_code');
		}

		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->select('ask_code.*,user.*');
			$this->db->join('user','user.id_user=ask_code.id_respon');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace_tanya($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function update_status($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function answer($where,$table){
			$this->db->select('ask_code.*,user.*');
			$this->db->join('user','user.id_user=ask_code.id_user');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function send_respon($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}
	
	}
	
	/* End of file M_ask.php */
	/* Location: ./application/models/M_ask.php */
?>