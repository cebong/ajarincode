<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_report extends CI_Model {
	
		function code_otomatis(){
            $this->db->select('Right(report.id_report,3) as kode ',false);
            $this->db->order_by('id_report', 'desc');
            $this->db->limit(1);
            $query = $this->db->get('report');
            if($query->num_rows()<>0){
                $data = $query->row();
                $kode = intval($data->kode)+1;
            }else{
                $kode = 1;

            }
            $kodemax = str_pad($kode,3,"0",STR_PAD_LEFT);
            $kodejadi  = "ACR-".$kodemax;
            return $kodejadi;
        }

        function jumlah_bug(){
			return $this->db->get('report')->num_rows();
		}

		function data_bug($number,$offset){
			$this->db->select('report.*,user.*');
			$this->db->join('user','user.id_user=report.id_user');
			return $this->db->get('report',$number,$offset);
		}

        function send_report($data,$table){
        	$this->db->insert($table,$data);
        }

        function baca_report($where,$table){
        	$this->db->select('report.*,user.*');
        	$this->db->join('user','user.id_user=report.id_user');
        	$this->db->where($where);
        	return $this->db->get($table);
        }

        function kerja_report($where,$data,$table){
        	$this->db->where($where);
        	$this->db->update($table,$data);
        }

        function cleared_report($where,$data,$table){
        	$this->db->where($where);
        	$this->db->update($table,$data);
        }

        function trash_report($where,$table){
        	$this->db->where($where);
        	$this->db->delete($table);
        }
	
	}
	
	/* End of file M_report.php */
	/* Location: ./application/models/M_report.php */
?>