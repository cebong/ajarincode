<?php 
	if ( ! defined('BASEPATH')) exit('No direct script access allowed');
	
	class M_materi extends CI_Model {
	
		function list_materi(){
			$this->db->select('materi.id_materi,level.nama_level,kategori.nama_kategori,materi.judul_materi,materi.link_video');
			$this->db->join('level','level.id_level=materi.id_level');
			$this->db->join('kategori','kategori.id_kategori=materi.id_kategori');
			return $this->db->get('materi');
		}

		public function jumlah_materi(){
            return $this->db->get('materi')->num_rows();
        }

        public function data($number,$offset){
        	$this->db->select('materi.id_materi,level.nama_level,kategori.nama_kategori,materi.judul_materi,materi.link_video');
			$this->db->join('level','level.id_level=materi.id_level');
			$this->db->join('kategori','kategori.id_kategori=materi.id_kategori');
            return $this->db->get('materi',$number,$offset);
        }

		function create($data,$table){
			$this->db->insert($table,$data);
		}

		function get($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}

		function replace($where,$data,$table){
			$this->db->where($where);
			$this->db->update($table,$data);
		}

		function trash($where,$table){
			$this->db->where($where);
			$this->db->delete($table);
		}

		function show_materi_level($where,$table){
			$this->db->select('materi.*,level.*,kategori.*');
			$this->db->join('level','level.id_level=materi.id_level');
			$this->db->join('kategori','kategori.id_kategori=materi.id_kategori');
			$this->db->where($where);
			return $this->db->get($table);
		}

		function read($where,$table){
			$this->db->where($where);
			return $this->db->get($table);
		}
	
	}
	
	/* End of file M_materi.php */
	/* Location: ./application/models/M_materi.php */
?>